var timeChartDps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDps',
					type: 'spline',
					zoomType: 'x',
				},
				credits : {
					enabled : false
				},
				subtitle: {
					text: document.ontouchstart === undefined ?
						'Click and drag in the plot area to zoom in' :
						'Drag your finger over the plot to zoom in'
				},
				title : {
					text : 'DPS over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'DPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'DPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : [{
					id: 'dataseries',
					data: dpsOverTimeData,
					marker: {
						radius: 1,
					},
					showInLegend: false
				}
				],
			});
		});

var timeChartTps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartTps',
					zoomType: 'x',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				subtitle: {
					text: document.ontouchstart === undefined ?
						'Click and drag in the plot area to zoom in' :
						'Drag your finger over the plot to zoom in'
				},
				title : {
					text : 'TPS over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'TPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'TPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : [{
					id: 'dataseries',
					data: tpsOverTimeData,
					marker: {
						radius: 1,
					},
					showInLegend: false
				}
				],
			});
		});

var timeChartHps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartHps',
					zoomType: 'x',
					type: 'spline'
				},
				subtitle: {
					text: document.ontouchstart === undefined ?
						'Click and drag in the plot area to zoom in' :
						'Drag your finger over the plot to zoom in'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'HPS over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'HPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'HPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : [{
					id: 'dataseries',
					data: hpsOverTimeData,
					marker: {
						radius: 1,
					},
					showInLegend: false
				}],
			});
		});

var perPlayerChartDps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'perPlayerChartDps',
					type: 'spline',
					zoomType: 'x',
				},
				credits : {
					enabled : false
				},
				subtitle: {
					text: document.ontouchstart === undefined ?
						'Click the legend to select displayed players' :
						'Touch the legend to select displayed players'
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					x: -10,
					y: 100,
					borderWidth: 0
				},
				title : {
					text : 'DPS over time per player'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'DPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'DPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				plotOptions: {
					series: {
						marker: {
							enabled: false,
							states: {
								hover: {
									enabled: true,
									radius: 5
								}
							}
						},
					}
				},
				series : dpsPerPlayerData,
			});
		});

var perPlayerChartTps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'perPlayerChartTps',
					zoomType: 'x',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				subtitle: {
					text: document.ontouchstart === undefined ?
						'Click the legend to select displayed players' :
						'Touch the legend to select displayed players'
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					x: -10,
					y: 100,
					borderWidth: 0
				},
				title : {
					text : 'TPS over time per player'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'TPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'TPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : tpsPerPlayerData,
			});
		});

var perPlayerChartHps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'perPlayerChartHps',
					zoomType: 'x',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				subtitle: {
					text: document.ontouchstart === undefined ?
						'Click the legend to select displayed players' :
						'Touch the legend to select displayed players'
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					x: -10,
					y: 100,
					borderWidth: 0
				},
				title : {
					text : 'HPS over time per player'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'HPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'HPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				plotOptions: {
					series: {
						lineWidth: 1,
						marker: {
							enabled: false,
							states: {
								hover: {
									enabled: true,
									radius: 5
								}
							}
						},
						shadow: false,
						states: {
							hover: {
								lineWidth: 1
							}
						}
					}
				},
				series : hpsPerPlayerData,
			});
		});