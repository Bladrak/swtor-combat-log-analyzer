var chartDamages;
$(document).ready(
		function() {
			chartDamages = new Highcharts.Chart({
				chart : {
					renderTo : 'chartDamages',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages repartition per capacity'
				},
				tooltip : {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Damages Per Capacity',
					data : damagesChartData,
				} ]
			});
		});

var chartDamagesTank;
$(document).ready(
		function() {
			chartDamagesTank = new Highcharts.Chart({
				chart : {
					renderTo : 'chartDamagesTank',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false,
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages repartition per capacity'
				},
				tooltip : {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Damages Per Capacity',
					data : damagesChartData,
				} ]
			});
		});

var chartHeal;
$(document).ready(
		function() {
			chartHeal = new Highcharts.Chart({
				chart : {
					renderTo : 'chartHeal',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Heal repartition per capacity'
				},
				tooltip: {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Heal Per Capacity',
					data : healChartData,
				} ]
			});
		});

var timeChartDamages;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDamages',
					type: 'scatter'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages per type over time'
				},
				xAxis : {
					type : 'datetime',
				},
				yAxis : {
					title : {
						text : 'Damages'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Damages: ' + this.y;
					}
				},

				series : damagesOverTimeData
			});
		});

var timeChartDamagesTank;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDamagesTank',
					type: 'scatter'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages per type over time'
				},
				xAxis : {
					type : 'datetime',
				},
				yAxis : {
					title : {
						text : 'Damages'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Damages: ' + this.y;
					}
				},
				series : damagesOverTimeData
			});
		});

var timeChartHeals;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartHeals',
					type: 'scatter'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Heals per type over time'
				},
				xAxis : {
					type : 'datetime',
				},
				yAxis : {
					title : {
						text : 'Heals'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Heal: ' + this.y;
					}
				},
				series : healsOverTimeData
			});
		});