var timeChartDamages;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDamages',
					type: 'scatter'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages per type over time'
				},
				xAxis : {
					type : 'datetime',
				},
				yAxis : {
					title : {
						text : 'Damages'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Damages: ' + this.y;
					}
				},

				series : damagesOverTimeData
			});
		});

var timeChartDamagesTank;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDamagesTank',
					type: 'scatter'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages per type over time'
				},
				xAxis : {
					type : 'datetime',
				},
				yAxis : {
					title : {
						text : 'Damages'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Damages: ' + this.y;
					}
				},
				series : damagesOverTimeData
			});
		});

//var timeChartHeals;
//$(document).ready(
//		function() {
//			chart = new Highcharts.Chart({
//				chart : {
//					renderTo : 'timeChartHeals',
//					type: 'scatter'
//				},
//				credits : {
//					enabled : false
//				},
//				title : {
//					text : 'Heals per type over time'
//				},
//				xAxis : {
//					type : 'datetime',
//				},
//				yAxis : {
//					title : {
//						text : 'Heals'
//					}
//				},
//				tooltip : {
//					formatter : function() {
//						return '<b>' + this.series.name + '</b><br/>'
//								+ Highcharts.dateFormat('%H:%M:%S', this.x)
//								+ '<br/>Heal: ' + this.y;
//					}
//				},
//				series : healsOverTimeData
//			});
//		});