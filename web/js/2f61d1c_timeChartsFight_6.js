var timeChartDamages;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDamages',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages per type over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'Damages'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Damages: ' + this.y;
					}
				},

				series : damagesOverTimeData
			});
		});

var timeChartDamagesTank;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDamagesTank',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages per type over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'Damages'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%H:%M:%S', this.x)
								+ '<br/>Damages: ' + this.y;
					}
				},
				series : damagesOverTimeData
			});
		});

var timeChartHeals;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartHeals',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Heals over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'Heals'
					}
				},
				legend : {
					enabled: false,
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'Heal: ' + this.y;
					}
				},
				series : [{
					data: healsOverTimeData,
					marker: {
						radius: 1,
					}
				}],
			});
		});

var timeChartDps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartDps',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'DPS over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'DPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'DPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : [{
					id: 'dataseries',
					data: dpsOverTimeData,
					marker: {
						radius: 1,
					},
					showInLegend: false
				}
				],
			});
		});

var timeChartTps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartTps',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'TPS over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'TPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'TPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : [{
					id: 'dataseries',
					data: tpsOverTimeData,
					marker: {
						radius: 1,
					},
					showInLegend: false
				}
				],
			});
		});

var timeChartHps;
$(document).ready(
		function() {
			chart = new Highcharts.Chart({
				chart : {
					renderTo : 'timeChartHps',
					type: 'spline'
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'HPS over time'
				},
				xAxis : {
					type : 'datetime',
					plotLines: deathsOverTimeData,
				},
				yAxis : {
					min: 0,
					title : {
						text : 'HPS'
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
								+ 'HPS: ' + Highcharts.numberFormat(this.y, 2, '.');
					}
				},
				series : [{
					id: 'dataseries',
					data: hpsOverTimeData,
					marker: {
						radius: 1,
					},
					showInLegend: false
				}],
			});
		});