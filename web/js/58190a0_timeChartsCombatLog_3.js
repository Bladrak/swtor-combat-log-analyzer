function timeChart(renderTo, title, valueTitle, seriesStr) {
	var series = eval(seriesStr);
	new Highcharts.Chart({
		chart : {
			renderTo : renderTo,
			type: 'scatter'
		},
		credits : {
			enabled : false
		},
		title : {
			text : title
		},
		xAxis : {
			type : 'datetime',
		},
		yAxis : {
			title : {
				text : valueTitle
			}
		},
		tooltip : {
			formatter : function() {
				return '<b>' + this.series.name + '</b><br/>'
						+ Highcharts.dateFormat('%H:%M:%S', this.x)
						+ '<br/>Damages: ' + this.y;
			}
		},

		series : series
	});
}

