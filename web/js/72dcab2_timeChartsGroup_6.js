function timeChartPsGroup(renderTo, title, dataTitle, dataStr, deathData) {
	var data = eval(dataStr);
	new Highcharts.StockChart({
		chart : {
			renderTo : renderTo,
			type: 'spline',
			zoomType: 'x',
		},
		credits : {
			enabled : false
		},
		subtitle: {
			text: document.ontouchstart === undefined ?
				'Click and drag in the plot area to zoom in' :
				'Drag your finger over the plot to zoom in'
		},
		title : {
			text : title
		},
		rangeSelector: {
	        buttons: [{
	            type: 'second',
	            count: 30,
	            text: '30s'
	        }, {
	            type: 'minute',
	            count: 5,
	            text: '5m'
	        }, {
	            type: 'minute',
	            count: 30,
	            text: '30m'
	        }, {
	            type: 'minute',
	            count: 60,
	            text: '1h'
	        }, {
	            type: 'minute',
	            count: 120,
	            text: '2h'
	        }, {
	            type: 'all',
	            text: 'All'
	        }],
	        selected: 1
	    },
		xAxis : {
			type : 'datetime',
			plotLines: deathData,
		},
		yAxis : {
			min: 0,
			title : {
				text : dataTitle
			}
		},
		tooltip : {
			formatter : function() {
				return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
						+ dataTitle + ': ' + Highcharts.numberFormat(this.y, 2, '.');
			}
		},
		series : [{
			id: 'dataseries',
			data: data,
			marker: {
				radius: 1,
			},
			showInLegend: false
		}
		],
	});
}

function timeChartPerPlayer(renderTo, title, dataTitle, dataStr, deathData) {
	var data = eval(dataStr);
	new Highcharts.StockChart({
		chart : {
			renderTo : renderTo,
			type: 'spline',
			zoomType: 'x',
		},
		credits : {
			enabled : false
		},
		subtitle: {
			text: document.ontouchstart === undefined ?
				'Click the legend to select displayed players' :
				'Touch the legend to select displayed players'
		},
		legend: {
			enabled: true,
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -10,
			y: 100,
			borderWidth: 0
		},
		title : {
			text : title
		},
		rangeSelector: {
	        buttons: [{
	            type: 'second',
	            count: 30,
	            text: '30s'
	        }, {
	            type: 'minute',
	            count: 5,
	            text: '5m'
	        }, {
	            type: 'minute',
	            count: 30,
	            text: '30m'
	        }, {
	            type: 'minute',
	            count: 60,
	            text: '1h'
	        }, {
	            type: 'minute',
	            count: 120,
	            text: '2h'
	        }, {
	            type: 'all',
	            text: 'All'
	        }],
	        selected: 1
	    },
		xAxis : {
			type : 'datetime',
			plotLines: deathData,
		},
		yAxis : {
			min: 0,
			title : {
				text : dataTitle
			}
		},
		tooltip : {
			formatter : function() {
				return '<b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '</b><br/>'
						+ dataTitle + ': ' + Highcharts.numberFormat(this.y, 2, '.');
			}
		},
		plotOptions: {
			series: {
				lineWidth: 1,
				turboThreshold: 10,
				marker: {
					enabled: false,
					states: {
						hover: {
							enabled: true,
							radius: 5
						}
					}
				},
				shadow: false,
				states: {
					hover: {
						lineWidth: 1
					}
				}
			}
		},
		series : data,
	});
}
