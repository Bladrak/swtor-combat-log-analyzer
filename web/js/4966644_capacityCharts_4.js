var chartDamages;
$(document).ready(
		function() {
			chartDamages = new Highcharts.Chart({
				chart : {
					renderTo : 'chartDamages',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages repartition per capacity'
				},
				tooltip : {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Damages Per Capacity',
					data : damagesChartData,
				} ]
			});
		});

var chartDamagesTank;
$(document).ready(
		function() {
			chartDamagesTank = new Highcharts.Chart({
				chart : {
					renderTo : 'chartDamagesTank',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false,
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Damages repartition per capacity'
				},
				tooltip : {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Damages Per Capacity',
					data : damagesChartData,
				} ]
			});
		});

var chartHeal;
$(document).ready(
		function() {
			chartHeal = new Highcharts.Chart({
				chart : {
					renderTo : 'chartHeal',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Heal repartition per capacity'
				},
				tooltip: {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Heal Per Capacity',
					data : healChartData,
				} ]
			});
		});

var chartHealSource;
$(document).ready(
		function() {
			chartHeal = new Highcharts.Chart({
				chart : {
					renderTo : 'chartHealSource',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Heals received per source'
				},
				tooltip: {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Heals Received Per Source',
					data : healSourceData,
				} ]
			});
		});

var chartHealTarget;
$(document).ready(
		function() {
			chartHeal = new Highcharts.Chart({
				chart : {
					renderTo : 'chartHealTarget',
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false
				},
				credits : {
					enabled : false
				},
				title : {
					text : 'Heals per target'
				},
				tooltip: {
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(this.y, 2,
										",", ".");
					}
				},
				plotOptions : {
					pie : {
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : true,
							color : '#000000',
							connectorColor : '#000000',
							formatter : function() {
								return '<b>'
										+ this.point.name
										+ '</b>: '
										+ Highcharts.numberFormat(
												this.percentage, 2, ",", ".")
										+ ' %';
							}
						}
					}
				},
				series : [ {
					type : 'pie',
					name : 'Heals Per Target',
					data : healTargetData,
				} ]
			});
		});
