function pieChart(renderTo, title, seriesName, dataStr) {
	var data = eval(dataStr);
	new Highcharts.Chart({
		chart : {
			renderTo : renderTo,
			plotBackgroundColor : null,
			plotBorderWidth : null,
			plotShadow : false
		},
		credits : {
			enabled : false
		},
		title : {
			text : title
		},
		tooltip : {
			formatter : function() {
				return '<b>'
						+ this.point.name
						+ '</b>: '
						+ Highcharts.numberFormat(this.y, 2,
								",", ".");
			}
		},
		plotOptions : {
			pie : {
				allowPointSelect : true,
				cursor : 'pointer',
				dataLabels : {
					enabled : true,
					color : '#000000',
					connectorColor : '#000000',
					formatter : function() {
						return '<b>'
								+ this.point.name
								+ '</b>: '
								+ Highcharts.numberFormat(
										this.percentage, 2, ",", ".")
								+ ' %';
					}
				}
			}
		},
		series : [ {
			type : 'pie',
			name : seriesName,
			data : data,
		} ]
	});
}
