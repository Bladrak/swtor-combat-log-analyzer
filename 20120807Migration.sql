-- phpMyAdmin SQL Dump
-- version 3.4.10.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Dim 08 Juillet 2012 à 11:26
-- Version du serveur: 5.1.50
-- Version de PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `torcombatlog`
--

-- --------------------------------------------------------

--
-- Structure de la table `loganalyzer_ext_translations`
--

CREATE TABLE IF NOT EXISTS `loganalyzer_ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) NOT NULL,
  `object_class` varchar(255) NOT NULL,
  `field` varchar(32) NOT NULL,
  `foreign_key` varchar(64) NOT NULL,
  `content` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`foreign_key`,`field`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

--
-- Contenu de la table `loganalyzer_ext_translations`
--

INSERT INTO `loganalyzer_ext_translations` (`id`, `locale`, `object_class`, `field`, `foreign_key`, `content`) VALUES
(1, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '1', 'Sith Warrior'),
(2, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '1', 'Guerrier Sith'),
(3, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '1', 'Sith-Krieger'),
(4, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '2', 'Bounty Hunter'),
(5, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '2', 'Chasseur de primes'),
(6, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '2', 'Kopfgeldjäger'),
(7, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '3', 'Sith Inquisitor'),
(8, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '3', 'Inquisiteur Sith'),
(9, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '3', 'Sith-Inquisitor'),
(10, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '4', 'Imperial Agent'),
(11, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '4', 'Agent Impérial'),
(12, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '4', 'Imperialer-Agent'),
(13, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '5', 'Jedi Knight'),
(14, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '5', 'Guerrier Jedi'),
(15, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '5', 'Jedi-Ritter'),
(16, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '6', 'Trooper'),
(17, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '6', 'Soldat'),
(18, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '6', 'Soldat'),
(19, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '7', 'Jedi Consular'),
(20, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '7', 'Jedi Consulaire'),
(21, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '7', 'Botschafter'),
(22, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '8', 'Smuggler'),
(23, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '8', 'Contrebandier'),
(24, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '8', 'Schmuggler'),
(25, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '9', 'Juggernaut'),
(26, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '9', 'Ravageur'),
(27, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '9', 'Juggernaut'),
(28, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '10', 'Marauder'),
(29, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '10', 'Maraudeur'),
(30, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '10', 'Marodeur'),
(31, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '11', 'Powertech'),
(32, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '11', 'Spécialiste'),
(33, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '11', 'PowerTech'),
(34, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '12', 'Mercenary'),
(35, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '12', 'Mercenaire'),
(36, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '12', 'Söldner'),
(37, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '13', 'Assassin'),
(38, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '13', 'Assassin'),
(39, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '13', 'Attentäter'),
(40, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '14', 'Sorcerer'),
(41, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '14', 'Sorcier'),
(42, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '14', 'Hexer'),
(43, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '15', 'Operative'),
(44, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '15', 'Agent Secret'),
(45, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '15', 'Saboteur'),
(46, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '16', 'Sniper'),
(47, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '16', 'Tireur d''élite'),
(48, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '16', 'Sharfschütze'),
(49, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '17', 'Guardian'),
(50, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '17', 'Gardien'),
(51, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '17', 'Hüter'),
(52, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '18', 'Sentinel'),
(53, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '18', 'Sentinelle'),
(54, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '18', 'Wächter'),
(55, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '19', 'Vanguard'),
(56, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '19', 'Avant-garde'),
(57, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '19', 'Frontkämpfer'),
(58, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '20', 'Commando'),
(59, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '20', 'Commando'),
(60, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '20', 'Kommando'),
(61, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '21', 'Sage'),
(62, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '21', 'Érudit'),
(63, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '21', 'Gelehrter'),
(64, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '22', 'Shadow'),
(65, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '22', 'Ombre'),
(66, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '22', 'Schatten'),
(67, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '23', 'Gunslinger'),
(68, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '23', 'Franc-tireur'),
(69, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '23', 'Revolverheld'),
(70, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '24', 'Scoundrel'),
(71, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '24', 'Malfrat'),
(72, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '24', 'Schurke'),
(73, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '25', 'Sith Warrior'),
(74, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '25', 'Guerrier Sith'),
(75, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '25', 'Sith-Krieger'),
(76, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '26', 'Bounty Hunter'),
(77, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '26', 'Chasseur de primes'),
(78, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '26', 'Kopfgeldjäger'),
(79, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '27', 'Sith Inquisitor'),
(80, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '27', 'Inquisiteur Sith'),
(81, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '27', 'Sith-Inquisitor'),
(82, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '28', 'Imperial Agent'),
(83, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '28', 'Agent Impérial'),
(84, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '28', 'Imperialer-Agent'),
(85, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '29', 'Jedi Knight'),
(86, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '29', 'Guerrier Jedi'),
(87, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '29', 'Jedi-Ritter'),
(88, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '30', 'Trooper'),
(89, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '30', 'Soldat'),
(90, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '30', 'Soldat'),
(91, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '31', 'Jedi Consular'),
(92, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '31', 'Jedi Consulaire'),
(93, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '31', 'Botschafter'),
(94, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '32', 'Smuggler'),
(95, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '32', 'Contrebandier'),
(96, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '32', 'Schmuggler'),
(97, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '33', 'Juggernaut'),
(98, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '33', 'Ravageur'),
(99, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '33', 'Juggernaut'),
(100, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '34', 'Marauder'),
(101, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '34', 'Maraudeur'),
(102, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '34', 'Marodeur'),
(103, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '35', 'Powertech'),
(104, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '35', 'Spécialiste'),
(105, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '35', 'PowerTech'),
(106, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '36', 'Mercenary'),
(107, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '36', 'Mercenaire'),
(108, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '36', 'Söldner'),
(109, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '37', 'Assassin'),
(110, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '37', 'Assassin'),
(111, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '37', 'Attentäter'),
(112, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '38', 'Sorcerer'),
(113, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '38', 'Sorcier'),
(114, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '38', 'Hexer'),
(115, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '39', 'Operative'),
(116, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '39', 'Agent Secret'),
(117, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '39', 'Saboteur'),
(118, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '40', 'Sniper'),
(119, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '40', 'Tireur d''élite'),
(120, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '40', 'Sharfschütze'),
(121, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '41', 'Guardian'),
(122, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '41', 'Gardien'),
(123, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '41', 'Hüter'),
(124, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '42', 'Sentinel'),
(125, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '42', 'Sentinelle'),
(126, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '42', 'Wächter'),
(127, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '43', 'Vanguard'),
(128, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '43', 'Avant-garde'),
(129, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '43', 'Frontkämpfer'),
(130, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '44', 'Commando'),
(131, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '44', 'Commando'),
(132, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '44', 'Kommando'),
(133, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '45', 'Sage'),
(134, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '45', 'Érudit'),
(135, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '45', 'Gelehrter'),
(136, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '46', 'Shadow'),
(137, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '46', 'Ombre'),
(138, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '46', 'Schatten'),
(139, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '47', 'Gunslinger'),
(140, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '47', 'Franc-tireur'),
(141, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '47', 'Revolverheld'),
(142, 'en', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '48', 'Scoundrel'),
(143, 'fr', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '48', 'Malfrat'),
(144, 'de', 'LogAnalyzer\\Bundle\\TorUtilsBundle\\Entity\\TorClass', 'name', '48', 'Schurke');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
