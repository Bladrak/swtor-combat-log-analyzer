#!/bin/bash
Go="--go"
Path="/var/www/logAnalyzer2"
# if [ $1 = $Go ]; then
# 	echo 'executing rsync -avz --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ ftp.hbriand.com:/homez.395/hbriand/logAnalyzer'
# 	rsync -avz --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ ftp.hbriand.com:/homez.395/hbriand/logAnalyzer
# else
# 	echo 'executing rsync -avz --dry-run --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ ftp.hbriand.com:/homez.395/hbriand/logAnalyzer'
# 	rsync -avz --dry-run --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ ftp.hbriand.com:/homez.395/hbriand/logAnalyzer
# fi
if [ $1 = $Go ]; then
	echo 'executing rsync -avz --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ root@37.59.126.86:'$Path
	rsync -avz --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ root@37.59.126.86:$Path
	echo 'ssh root@37.59.126.86 "cd "$Path"; ./app/console cache:clear --env=prod; chown -R root:root ./; chmod -R 777 app/cache; chmod -R 777 app/logs;"'
	ssh root@37.59.126.86 "cd $Path; ./app/console cache:clear --env=prod; chown -R root:root ./; chmod -R 777 app/cache; chmod -R 777 app/logs;" 
else
	echo 'executing rsync -avz --dry-run --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ root@37.59.126.86:'$Path
	rsync -avz --dry-run --exclude-from="app/config/rsync_exclude.txt" -e ssh ./ root@37.59.126.86:$Path
fi
