#!/bin/bash

#bundles[0]=LogAnalyzerCombatLogBundle
#bundles[1]=LogAnalyzerGlobalBundle
#bundles[2]=LogAnalyzerRankingBundle
bundles[3]=LogAnalyzerStatsBundle
#bundles[4]=LogAnalyzerUserBundle
#bundles[5]=LogAnalyzerTorUtilsBundle

languages[0]=en
languages[1]=fr
languages[2]=de

for bundle in ${bundles[@]} 
do
	:
	for lang in ${languages[@]} 
	do
		:
		php app/console bcc:trans:update $lang $bundle --force
	done
done