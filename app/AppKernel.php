<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
       		new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
       		new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),

        		// Added custom bundles

        	new FOS\UserBundle\FOSUserBundle(),
       		new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            //new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle(),
       		new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
        	new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
        	new BCC\ExtraToolsBundle\BCCExtraToolsBundle(),
       		new JMS\TranslationBundle\JMSTranslationBundle(),

        		// App bundles

			new LogAnalyzer\Bundle\CombatLogBundle\LogAnalyzerCombatLogBundle(),
			new LogAnalyzer\Bundle\UserBundle\LogAnalyzerUserBundle(),
            new LogAnalyzer\Bundle\GlobalBundle\LogAnalyzerGlobalBundle(),
            new LogAnalyzer\Bundle\StatsBundle\LogAnalyzerStatsBundle(),
            new LogAnalyzer\Bundle\TorUtilsBundle\LogAnalyzerTorUtilsBundle(),
            new LogAnalyzer\Bundle\RankingBundle\LogAnalyzerRankingBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
