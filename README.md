SWTOR Log Analyzer
========================

Welcome the SWTOR Log Analyzer project ! 

How to use this repository
========================

You can browse the source code, fork it and take a part in the application development.

If you're a beta tester, you can post and view bugs in the Issues section.

Project technologies
========================

* The application is built using the Symfony2 PHP framework (www.symfony.com).
* The website frontend is generated through the Bootstrap by Twitter CSS framework.
* The Javascript used is performed by JQuery and the libs datatables and Highcharts.
