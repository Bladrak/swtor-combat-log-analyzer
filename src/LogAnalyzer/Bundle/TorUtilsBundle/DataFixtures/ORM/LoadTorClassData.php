<?php

namespace LogAnalyzer\Bundle\TorUtilsBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;

use LogAnalyzer\Bundle\TorUtilsBundle\Entity\TorClass;

use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadUserData implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		$transMgr = $manager->getRepository('StofDoctrineExtensionsBundle:Translation');
		
		//Basic class
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Sith Warrior");
		$class->setImageName("Icon-class-sithwarrior");
		$transMgr->translate($class, 'name', 'fr', 'Guerrier Sith')
				->translate($class, 'name', 'de', 'Sith-Krieger');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Bounty Hunter");
		$class->setImageName("Icon-class-bountyhunter");
		$transMgr->translate($class, 'name', 'fr', 'Chasseur de primes')
		->translate($class, 'name', 'de', 'Kopfgeldjäger');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Sith Inquisitor");
		$class->setImageName("Icon-class-sithinquisitor");
		$transMgr->translate($class, 'name', 'fr', 'Inquisiteur Sith')
		->translate($class, 'name', 'de', 'Sith-Inquisitor');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Imperial Agent");
		$class->setImageName("Icon-class-imperialagent");
		$transMgr->translate($class, 'name', 'fr', 'Agent Impérial')
		->translate($class, 'name', 'de', 'Imperialer-Agent');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Jedi Knight");
		$class->setImageName("Icon-class-jediknight");
		$transMgr->translate($class, 'name', 'fr', 'Guerrier Jedi')
		->translate($class, 'name', 'de', 'Jedi-Ritter');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Trooper");
		$class->setImageName("Icon-class-trooper");
		$transMgr->translate($class, 'name', 'fr', 'Soldat')
		->translate($class, 'name', 'de', 'Soldat');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Jedi Consular");
		$class->setImageName("Icon-class-sithwarrior");
		$transMgr->translate($class, 'name', 'fr', 'Jedi Consulaire')
		->translate($class, 'name', 'de', 'Botschafter');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(false);
		$class->setTranslatableLocale('en');
		$class->setName("Smuggler");
		$class->setImageName("Icon-class-smuggler");
		$transMgr->translate($class, 'name', 'fr', 'Contrebandier')
		->translate($class, 'name', 'de', 'Schmuggler');
		$manager->persist($class);
		
		
		// Advanced class
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Juggernaut");
		$class->setImageName("Icon-class-sithwarrior");
		$transMgr->translate($class, 'name', 'fr', 'Ravageur')
		->translate($class, 'name', 'de', 'Juggernaut');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Marauder");
		$class->setImageName("Icon-class-sithwarrior");
		$transMgr->translate($class, 'name', 'fr', 'Maraudeur')
		->translate($class, 'name', 'de', 'Marodeur');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Powertech");
		$class->setImageName("Icon-class-bountyhunter");
		$transMgr->translate($class, 'name', 'fr', 'Spécialiste')
		->translate($class, 'name', 'de', 'PowerTech');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Mercenary");
		$class->setImageName("Icon-class-bountyhunter");
		$transMgr->translate($class, 'name', 'fr', 'Mercenaire')
		->translate($class, 'name', 'de', 'Söldner');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Assassin");
		$class->setImageName("Icon-class-sithinquisitor");
		$transMgr->translate($class, 'name', 'fr', 'Assassin')
		->translate($class, 'name', 'de', 'Attentäter');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Sorcerer");
		$class->setImageName("Icon-class-sithinquisitor");
		$transMgr->translate($class, 'name', 'fr', 'Sorcier')
		->translate($class, 'name', 'de', 'Hexer');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Operative");
		$class->setImageName("Icon-class-imperialagent");
		$transMgr->translate($class, 'name', 'fr', 'Agent Secret')
		->translate($class, 'name', 'de', 'Saboteur');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_EMPIRE);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Sniper");
		$class->setImageName("Icon-class-imperialagent");
		$transMgr->translate($class, 'name', 'fr', 'Tireur d\'élite')
		->translate($class, 'name', 'de', 'Sharfschütze');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Guardian");
		$class->setImageName("Icon-class-jediknight");
		$transMgr->translate($class, 'name', 'fr', 'Gardien')
		->translate($class, 'name', 'de', 'Hüter');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Sentinel");
		$class->setImageName("Icon-class-jediknight");
		$transMgr->translate($class, 'name', 'fr', 'Sentinelle')
		->translate($class, 'name', 'de', 'Wächter');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Vanguard");
		$class->setImageName("Icon-class-trooper");
		$transMgr->translate($class, 'name', 'fr', 'Avant-garde')
		->translate($class, 'name', 'de', 'Frontkämpfer');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Commando");
		$class->setImageName("Icon-class-trooper");
		$transMgr->translate($class, 'name', 'fr', 'Commando')
		->translate($class, 'name', 'de', 'Kommando');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Sage");
		$class->setImageName("Icon-class-jediconsular");
		$transMgr->translate($class, 'name', 'fr', 'Érudit')
		->translate($class, 'name', 'de', 'Gelehrter');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Shadow");
		$class->setImageName("Icon-class-jediconsular");
		$transMgr->translate($class, 'name', 'fr', 'Ombre')
		->translate($class, 'name', 'de', 'Schatten');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Gunslinger");
		$class->setImageName("Icon-class-smuggler");
		$transMgr->translate($class, 'name', 'fr', 'Franc-tireur')
		->translate($class, 'name', 'de', 'Revolverheld');
		$manager->persist($class);
		
		$class = new TorClass();
		$class->setFaction(TorClass::FACTION_REPUBLIC);
		$class->setIsAdvanced(true);
		$class->setTranslatableLocale('en');
		$class->setName("Scoundrel");
		$class->setImageName("Icon-class-smuggler");
		$transMgr->translate($class, 'name', 'fr', 'Malfrat')
		->translate($class, 'name', 'de', 'Schurke');
		$manager->persist($class);
		
		$manager->flush();
	}
}