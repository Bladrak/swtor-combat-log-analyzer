<?php

namespace LogAnalyzer\Bundle\TorUtilsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ServerRepositoryTest extends WebTestCase
{

	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $_em;

	private $conn;

	protected function setUp() {
		$kernel = static::createKernel();
		$kernel->boot();
		$this->_em = $kernel->getContainer()
		->get('doctrine.orm.entity_manager');
		$this->conn = $kernel->getContainer()->get('database_connection');
	}

	public function testServerList()
	{
		$list = $this->_em->getRepository('LogAnalyzerTorUtilsBundle:Server')->updateServerList();
		var_dump($list);
	}
}