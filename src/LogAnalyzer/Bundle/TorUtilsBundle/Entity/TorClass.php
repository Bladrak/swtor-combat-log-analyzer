<?php

namespace LogAnalyzer\Bundle\TorUtilsBundle\Entity;

use Gedmo\Translatable\Translatable;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * LogAnalyzer\Bundle\TorUtilsBundle\Entity\TorClass
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\TorUtilsBundle\Entity\TorClassRepository", readOnly=true)
 */
class TorClass implements Translatable
{
	const FACTION_EMPIRE = 0;
	const FACTION_REPUBLIC = 1;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var smallint $faction
     *
     * @ORM\Column(name="faction", type="smallint")
     */
    private $faction;

    /**
     * @var string $name
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $imageName
     *
     * @ORM\Column(name="imageName", type="string", length=255)
     */
    private $imageName;

    /**
     * @var boolean $isAdvanced
     *
     * @ORM\Column(name="isAdvanced", type="boolean")
     */
    private $isAdvanced;
    
    /**
     * @Gedmo\Locale
     */
	private $locale;


    /**
     *
     * @return string
     */
    public function __toString()
    {
    	$name = "";
    	try {
    		$name = $this->getName();
    	} catch (\Exception $ex) {
    		$name = $ex->getMessage();
    	}
    	return $name;
    }
    
    public function setTranslatableLocale($locale)
    {
    	$this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set faction
     *
     * @param smallint $faction
     */
    public function setFaction($faction)
    {
        $this->faction = $faction;
    }

    /**
     * Get faction
     *
     * @return smallint
     */
    public function getFaction()
    {
        return $this->faction;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isAdvanced
     *
     * @param boolean $isAdvanced
     */
    public function setIsAdvanced($isAdvanced)
    {
        $this->isAdvanced = $isAdvanced;
    }

    /**
     * Get isAdvanced
     *
     * @return boolean
     */
    public function getIsAdvanced()
    {
        return $this->isAdvanced;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName.".png";
    }
}