<?php

namespace LogAnalyzer\Bundle\TorUtilsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server
 *
 * @ORM\Table(indexes={@ORM\index(name="sort_geographicZone", columns={"geographicZone"})})
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\TorUtilsBundle\Entity\ServerRepository", readOnly=true)
 */
class Server
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string $geographicZone
     *
     * @ORM\Column(name="geographicZone", type="string", length=255)
     */
    private $geographicZone;

    /**
     * @var string $timezone
     *
     * @ORM\Column(name="timezone", type="string", length=255)
     */
    private $timezone;

    /**
     * @var string $language
     *
     * @ORM\Column(name="language", type="string", length=25)
     */
    private $language;


    public function __toString()
    {
    	return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set geographicZone
     *
     * @param string $geographicZone
     */
    public function setGeographicZone($geographicZone)
    {
        $this->geographicZone = $geographicZone;
    }

    /**
     * Get geographicZone
     *
     * @return string
     */
    public function getGeographicZone()
    {
        return $this->geographicZone;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set language
     *
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
}