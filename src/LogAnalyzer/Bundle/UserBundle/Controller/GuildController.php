<?php

namespace LogAnalyzer\Bundle\UserBundle\Controller;

use FOS\UserBundle\Model\UserInterface;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\UserBundle\Entity\Guild;
use LogAnalyzer\Bundle\UserBundle\Form\GuildType;

/**
 * Guild controller.
 *
 * @Route("/guild")
 */
class GuildController extends Controller
{
    /**
     * Lists all Guild entities.
     *
     * @Route("/", name="guild")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('LogAnalyzerUserBundle:Guild')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Sets the leader for a guild
     *
     * @Route("/{id}/leader/{charId}", name="guild_set_leader")
     * @Template()
     */
    public function setLeaderAction($id, $charId)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
        }
        $newLeader = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($charId);
        if (!$newLeader) {
        	throw $this->createNotFoundException($this->get('translator')->trans('Unable to find TorCharacter entity.'));
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        $access = false;
        if (is_object($user) && $user instanceof UserInterface) {
        	$access = $user->hasCharacter($entity->getLeader());
        }
        if ($access === false)
        	throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


        $entity->setLeader($newLeader);
        $em->persist($entity);
        $em->flush();
        $this->get('session')->setFlash('success', $this->get('translator')->trans('%name% is the new guild leader', array('%name%' => $newLeader->getName())));

    	return $this->redirect($this->generateUrl('guild_show', array('id' => $entity->getId())));

        //return array('entities' => $entities);
    }

    /**
     * Removes a member from a guild
     *
     * @Route("/{id}/remove/{charId}", name="guild_remove_character")
     * @Template()
     */
    public function removeCharacterAction($id, $charId)
    {
    	$em = $this->getDoctrine()->getEntityManager();

    	$entity = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);
    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
    	}

    	$user = $this->container->get('security.context')->getToken()->getUser();
    	$access = false;
    	if (is_object($user) && $user instanceof UserInterface) {
    		$access = $user->hasCharacter($entity->getLeader());
    	}
    	if ($access === false)
    		throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


    	$toRemove = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($charId);
    	if (!$toRemove) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find TorCharacter entity.'));
    	}

    	$entity->getMembers()->removeElement($toRemove);
    	$toRemove->setGuild(null);
        $em->persist($entity);
        $em->persist($toRemove);
        $em->flush();
    	$this->get('session')->setFlash('success', $this->get('translator')->trans('%name% is not part of the guild anymore', array('%name%' => $toRemove->getName())));

    	return $this->redirect($this->generateUrl('guild_show', array('id' => $entity->getId())));

    	//return array('entities' => $entities);
    }

    /**
     * Finds and displays a Guild entity.
     *
     * @Route("/{id}/show", name="guild_show")
     * @Template()
     */
    public function showAction($id)
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
        }

        $userIsLeader = $access = false;
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (is_object($user) && $user instanceof UserInterface) {
                foreach ($user->getCharacters() as $tC)
		        {
		        	if ($tC->getId() == $entity->getLeader()->getId()){
		        		$userIsLeader = true;
		        		$access = true;
		        		break;
		        	}
		        	foreach ($entity->getMembers() as $member)
		        	{
		        		$access = $access || $user->hasCharacter($member);
		        	}

		        }
        }

        $deleteForm = $this->createDeleteForm($id);

//         $breadcrumbs = $this->get("white_october_breadcrumbs");
//         $breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//         $breadcrumbs->addItem($entity->getName(), $this->get("router")->generate("guild_show", array("id" => $entity->getId())));


        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        	'userIsLeader' => $userIsLeader,
        	'access'		=> $access,
        	);
    }

    /**
     * Displays a form to create a new Guild entity.
     *
     * @Route("/{charid}/new", name="guild_new")
     * @Template()
     */
    public function newAction($charid)
    {
        $entity = new Guild();
        $leader = $this->getDoctrine()->getEntityManager()->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($charid);
        if ($leader->getGuild() != null) {
        	throw new AccessDeniedException($this->get('translator')->trans("You can't create a guild while you're member of one."));
        }
        $entity->setLeader($leader);
        $entity->setServer($leader->getServer());

        $form   = $this->createForm(new GuildType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Guild entity.
     *
     * @Route("/create", name="guild_create")
     * @Method("post")
     * @Template("LogAnalyzerUserBundle:Guild:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Guild();
        $request = $this->getRequest();
        $form    = $this->createForm(new GuildType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
        	$entity->addTorCharacter($entity->getLeader());
        	$entity->getLeader()->setGuild($entity);
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('guild_show', array('id' => $entity->getId())));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Guild entity.
     *
     * @Route("/{id}/edit", name="guild_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        $access = false;
        if (is_object($user) && $user instanceof UserInterface) {
        	$access = $user->hasCharacter($entity->getLeader());
        }
        if ($access === false)
        	throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


        $editForm = $this->createForm(new GuildType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Guild entity.
     *
     * @Route("/{id}/update", name="guild_update")
     * @Method("post")
     * @Template("LogAnalyzerUserBundle:Guild:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        $access = false;
        if (is_object($user) && $user instanceof UserInterface) {
        	$access = $user->hasCharacter($entity->getLeader());
        }
        if ($access === false)
        	throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


        $editForm   = $this->createForm(new GuildType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('guild_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Guild entity.
     *
     * @Route("/{id}/delete", name="guild_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);

            $user = $this->container->get('security.context')->getToken()->getUser();
            $access = false;
            if (is_object($user) && $user instanceof UserInterface) {
            	$access = $user->hasCharacter($entity->getLeader());
            }
            if ($access === false)
            	throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('guild'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
