<?php

namespace LogAnalyzer\Bundle\UserBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use FOS\UserBundle\Model\UserInterface;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\UserBundle\Entity\GuildApply;
use LogAnalyzer\Bundle\UserBundle\Form\GuildApplyType;

/**
 * GuildApply controller.
 *
 * @Route("/guildapply")
 */
class GuildApplyController extends Controller
{
    /**
     * Lists all GuildApply entities.
     *
     * @Route("/", name="guildapply")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('LogAnalyzerUserBundle:GuildApply')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Accepts an apply
     *
     * @Route("/{id}/accept", name="apply_accept")
     * @Template()
     */
    public function acceptAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$entity = $em->getRepository('LogAnalyzerUserBundle:GuildApply')->find($id);

    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find GuildApply entity.'));
    	}

    	$user = $this->container->get('security.context')->getToken()->getUser();
    	if (!is_object($user) || !$user instanceof UserInterface) {
    		throw new AccessDeniedException($this->get('translator')->trans('This user does not have access to this section.'));
    	}
    	$userIsMember = false;
    	foreach ($user->getCharacters() as $tC)
    	{
    		if ($tC->getGuild()->getId() == $entity->getApplied()->getId()){
    			$userIsMember = true;
    			break;
    		}
    	}
    	if (!$userIsMember) {
    		throw new AccessDeniedException($this->get('translator')->trans('This user does not have access to this section.'));
    	}

    	$entity->getApplyant()->setGuild($entity->getApplied());
    	$entity->getApplied()->addTorCharacter($entity->getApplyant());

    	$this->get('session')->setFlash('success', $this->get('translator')->trans('%name% has joined the guild!', array('%name%' => $entity->getApplyant()->getName())));

    	$em->remove($entity);
    	$em->flush();

    	$response = $this->forward('LogAnalyzerUserBundle:Guild:show', array(
															        'id'  => $entity->getApplied()->getId(),
	    ));

    	// further modify the response or return it directly

	    return $response;
    }

    /**
     * Declines an apply
     *
     * @Route("/{id}/decline", name="apply_decline")
     * @Template()
     */
    public function declineAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$entity = $em->getRepository('LogAnalyzerUserBundle:GuildApply')->find($id);

    	$user = $this->container->get('security.context')->getToken()->getUser();
    	$access = false;
    	if (is_object($user) && $user instanceof UserInterface) {
    		$access = $user->hasCharacter($entity->getApplied()->getLeader());
    		if ($access == false) {
      	  foreach ($entity->getApplied()->getMembers() as $member) {
        		$access = $user->hasCharacter($member);
        		if ($access)
        		  break;
      	  }
    		}
    	}
    	if ($access === false)
    		throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


    	$entity->setStatus(GuildApply::STATUS_DECLINED);
    	$entity->getApplied()->getApplies()->removeElement($entity);

    	$this->get('session')->setFlash('warning', $this->get('translator')->trans(
    															'%name%\'s apply has been declined!', 
    															array('%name%' => $entity->getApplyant()->getName())
    														));

    	$entity->getApplyant()->getApplies()->removeElement($entity);
    	$em->flush();
    	return $this->forward('LogAnalyzerUserBundle:Guild:show', array(
															        'id'  => $entity->getApplied()->getId(),
	    						));
    }

    /**
     * Finds and displays a GuildApply entity.
     *
     * @Route("/{id}/show", name="guildapply_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerUserBundle:GuildApply')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find GuildApply entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new GuildApply entity.
     *
     * @Route("/{charid}/new", name="guildapply_new")
     * @Template()
     */
    public function newAction($charid)
    {
        $entity = new GuildApply();
        $entity->setApplyant($this->getDoctrine()->getEntityManager()->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($charid));
        $entity->setApplyDate(new \DateTime());
        $entity->setStatus(GuildApply::STATUS_NEW);

        $form   = $this->createForm(new GuildApplyType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new GuildApply entity.
     *
     * @Route("/create", name="guildapply_create")
     * @Method("post")
     * @Template("LogAnalyzerUserBundle:GuildApply:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new GuildApply();
        $request = $this->getRequest();
        $formData = $request->get("GuildApply");
        $tC = $this->getDoctrine()->getEntityManager()->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($formData["applyant"]);

        $user = $this->container->get('security.context')->getToken()->getUser();
        $access = false;
        if (is_object($user) && $user instanceof UserInterface) {
        	$access = $user->hasCharacter($tC);
        }
        if ($access === false)
        	throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


        $entity->setApplyant($tC);
        $form    = $this->createForm(new GuildApplyType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', $this->get('translator')->trans('Your apply has been posted!'));
            return $this->redirect($this->generateUrl('character_show', array('id' => $entity->getApplyant()->getId())));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Deletes a GuildApply entity.
     *
     * @Route("/{id}/delete", name="guildapply_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('LogAnalyzerUserBundle:GuildApply')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Unable to find GuildApply entity.'));
            }

            $em->remove($entity);
            $em->flush();
        }

    	$response = $this->forward('LogAnalyzerUserBundle:TorCharacter:show', array(
															        'id'  => $entity->getApplyant()->getId(),
	    ));

    	// further modify the response or return it directly

	    return $response;
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
