<?php

namespace LogAnalyzer\Bundle\UserBundle\Controller;
use Symfony\Component\HttpFoundation\Response;

use LogAnalyzer\Bundle\UserBundle\Entity\GuildApply;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;
use LogAnalyzer\Bundle\UserBundle\Form\TorCharacterType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;

/**
 * TorCharacter controller.
 *
 * @Route("/character")
 */
class TorCharacterController extends Controller {
	/**
	 * Lists all TorCharacter entities.
	 *
	 * @Route("/", name="character")
	 * @Template()
	 */
	public function indexAction() {
		$em = $this->getDoctrine()->getEntityManager();

		$entities = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')
				->findAll();

		return array('entities' => $entities);
	}

	/**
	 * Returns the classes select choice by faction.
	 *
	 * @Route("/byfaction", name="character_classbyfaction")
	 */
	public function classByFactionAction() {
		$id = $this->get('request')->query->get('id');

		$em = $this->getDoctrine()->getEntityManager();

		$entities = $em->getRepository('LogAnalyzerTorUtilsBundle:TorClass')
				->advancedByFaction($id);

		$html = "";

		foreach ($entities as $torClass)
		{
			$html .= sprintf("<option value=\"%d\">%s</option>", $torClass->getId(), $torClass->getName());
		}

		return new Response($html);
	}

	/**
	 * Returns the server select choice by zone.
	 *
	 * @Route("/byzone", name="character_serverbyzone")
	 */
	public function serverByZoneAction() {
		$id = $this->get('request')->query->get('id');

		$em = $this->getDoctrine()->getEntityManager();

		$entities = $em->getRepository('LogAnalyzerTorUtilsBundle:Server')
				->findBy(array('geographicZone' => $id));

		$html = "";

		foreach ($entities as $server)
		{
			$html .= sprintf("<option value=\"%d\">%s</option>", $server->getId(), $server->getName());
		}

		return new Response($html);
	}

	/**
	 * Leaves the character's guild
	 *
	 * @Route("/{id}/leaveguild", name="character_leaveguild")
	 * @Template("LogAnalyzerUserBundle:TorCharacter:show.html.twig")
	 */
	public function leaveGuildAction($id)
	{
		$em = $this->getDoctrine()->getEntityManager();

		$entity = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')
		->find($id);

		if (!$entity) {
			throw $this
			->createNotFoundException(
					'Unable to find TorCharacter entity.');
		}
		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			$access = $user->hasCharacter($entity);
		}
		if ($access === false)
			throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));

		if ($entity->getGuild() == null) {
			throw $this->createNotFoundException($this->get('translator')->trans('Unable to find guild'));
		}
		if ($entity->getGuild()->getLeader()->getId() == $entity->getId()) {
			$this->get('session')->setFlash('error', $this->get('translator')->trans('You cannot leave the guild if you\'re the leader, you have to abdicate first'));
			return $this->showAction($entity->getId());
		}
		$entity->getGuild()->getMembers()->removeElement($entity);
		$entity->setGuild(null);
		$em->flush();
		$this->get('session')->setFlash('success', $this->get('translator')->trans('You have left the guild.'));
		return $this->showAction($entity->getId());
	}


	/**
	 * Finds and displays a TorCharacter entity.
	 *
	 * @Route("/{id}/show", name="character_show")
	 * @Template()
	 */
	public function showAction($id) {


		$em = $this->getDoctrine()->getEntityManager();

		$entity = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')
				->tcForShow($id);

		if (!$entity) {
			throw $this
					->createNotFoundException(
							$this->get('translator')->trans('Unable to find TorCharacter entity.'));
		}

		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			$access = $user->hasCharacter($entity);
		}


		$deleteForm = $this->createDeleteForm($id);
// 		$breadcrumbs = $this->get("white_october_breadcrumbs");
// 		$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
// 		$breadcrumbs->addItem($entity->getName(), $this->get("router")->generate("character_show", array("id" => $entity->getId())));

		if ($entity->getApplies() != null && count($entity->getApplies()) > 0 && $entity->getApplies()->get(0)->getStatus() == GuildApply::STATUS_DECLINED) {
			$this->get('session')->setFlash('error', $this->get('translator')->trans('Your guild apply has been declined'));
			$em->remove($entity->getApplies()->get(0));
			$entity->getApplies()->removeElement($entity->getApplies()->get(0));
			$em->flush();
		}

		return array('entity' => $entity,
				'delete_form' => $deleteForm->createView(),
					'access' => $access);
	}

	/**
	 * Displays a form to create a new TorCharacter entity.
	 *
	 * @Route("/new", name="character_new")
	 * @Template()
	 */
	public function newAction() {
		$user = $this->container->get('security.context')->getToken()
				->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			throw new AccessDeniedException(
					$this->get('translator')->trans('You do not have access to this section.'));
		}

		$entity = new TorCharacter();
		$entity->setUser($user);
		$form = $this->createForm(new TorCharacterType(), $entity);
		return $this->container->get('templating')
				->renderResponse(
						'LogAnalyzerUserBundle:TorCharacter:new.html.twig',
						array('entity' => $entity,
								'form' => $form->createView()));
	}

	/**
	 * Creates a new TorCharacter entity.
	 *
	 * @Route("/create", name="character_create")
	 * @Method("post")
	 * @Template("LogAnalyzerUserBundle:TorCharacter:new.html.twig")
	 */
	public function createAction() {
		$user = $this->container->get('security.context')->getToken()
				->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			throw new AccessDeniedException(
					$this->get('translator')->trans('You do not have access to this section.'));
		}

		$entity = new TorCharacter();
		$request = $this->getRequest();
		$form = $this->createForm(new TorCharacterType(), $entity);
		$form->bindRequest($request);

		if ($form->isValid() && $entity->getUser() == $user) {
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($entity);
			$em->flush();

			$this->get('session')->setFlash('success', $this->get('translator')->trans('Character created!'));

			return $this
					->redirect(
							$this->generateUrl('_welcome'));

		}

		return array('entity' => $entity, 'form' => $form->createView());
	}

	/**
	 * Displays a form to edit an existing TorCharacter entity.
	 *
	 * @Route("/{id}/edit", name="character_edit")
	 * @Template()
	 */
	public function editAction($id) {
		$em = $this->getDoctrine()->getEntityManager();

		$entity = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')
				->find($id);

		if (!$entity) {
			throw $this
					->createNotFoundException(
							$this->get('translator')->trans('Unable to find TorCharacter entity.'));
		}

		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			$access = $user->hasCharacter($entity);
		}
		if ($access === false)
			throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


		$editForm = $this->createForm(new TorCharacterType(), $entity);
		$deleteForm = $this->createDeleteForm($id);

		return array('entity' => $entity,
				'edit_form' => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),);
	}

	/**
	 * Edits an existing TorCharacter entity.
	 *
	 * @Route("/{id}/update", name="character_update")
	 * @Method("post")
	 * @Template("LogAnalyzerUserBundle:TorCharacter:edit.html.twig")
	 */
	public function updateAction($id) {
		$em = $this->getDoctrine()->getEntityManager();

		$entity = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')
				->find($id);

		if (!$entity) {
			throw $this
					->createNotFoundException(
							$this->get('translator')->trans('Unable to find TorCharacter entity.'));
		}

		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			$access = $user->hasCharacter($entity);
		}
		if ($access === false)
			throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


		$editForm = $this->createForm(new TorCharacterType(), $entity);
		$deleteForm = $this->createDeleteForm($id);

		$request = $this->getRequest();

		$editForm->bindRequest($request);

		if ($editForm->isValid()) {
			$em->persist($entity);
			$em->flush();

			$this->get('session')->setFlash('success', $this->get('translator')->trans('Changes saved!'));

			return $this
					->redirect(
							$this
									->generateUrl('character_edit',
											array('id' => $id)));
		}

		return array('entity' => $entity,
				'edit_form' => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),);
	}

	/**
	 * Deletes a TorCharacter entity.
	 *
	 * @Route("/{id}/delete", name="character_delete")
	 * @Method("post")
	 */
	public function deleteAction($id) {
		$form = $this->createDeleteForm($id);
		$request = $this->getRequest();

		$form->bindRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getEntityManager();
			$entity = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')
					->find($id);
			if (!$entity) {
				throw $this
						->createNotFoundException(
								$this->get('translator')->trans('Unable to find TorCharacter entity.'));
			}

			$user = $this->container->get('security.context')->getToken()->getUser();
			$access = false;
			if (is_object($user) && $user instanceof UserInterface) {
				$access = $user->hasCharacter($entity);
			}
			if ($access === false)
				throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateUrl('_welcome'));
	}

	private function createDeleteForm($id) {
		return $this->createFormBuilder(array('id' => $id))
				->add('id', 'hidden')->getForm();
	}
}
