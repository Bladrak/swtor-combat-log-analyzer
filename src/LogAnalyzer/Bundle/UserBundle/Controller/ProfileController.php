<?php
namespace LogAnalyzer\Bundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProfileController extends BaseController
{
	/**
	 * @Route("/")
	 * @Template()
	 */
	public function showAction()
	{
		return parent::showAction();
	}
}