<?php

namespace LogAnalyzer\Bundle\UserBundle\Form;

use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\Mapping\OrderBy;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GuildApplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$server = $options['data']->getApplyant()->getServer();
        $builder
            ->add('applyDate', 'date', array(
            		'attr' => array('style' => 'display:none')
            		))
            ->add('status', 'integer', array(
            		'attr' => array('style' => 'display:none'))
            		)
            ->add('applyant', 'entity', array(
        			'class' => 'LogAnalyzerUserBundle:TorCharacter',
        			'attr' => array('style' => 'display:none'),
            		))
            ->add('applied', 'entity', array(
            		'class' => 'LogAnalyzerUserBundle:Guild',
            		'label' => 'Select guild',
            		'query_builder' => function(EntityRepository $er) use ($server) {
            		return $er->createQueryBuilder('g')
            					->where('g.server = :server')
            					->setParameter('server', $server)
            					->orderBy('g.name');
            		}))
        ;
    }

    public function getName()
    {
        return 'GuildApply';
    }
}
