<?php

namespace LogAnalyzer\Bundle\UserBundle\Form;

use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;

use Symfony\Component\Form\FormEvents;

use Symfony\Component\Form\Event\DataEvent;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TorCharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$tc = $builder->getData();
    	if ($tc instanceof TorCharacter && $tc->getTorClass() != null) {
    		$faction = $tc->getTorClass()->getFaction();
    	} else {
    		$faction = 0;
    	}
    	if ($tc instanceof TorCharacter && $tc->getServer() != null) {
    		$srvGz = $tc->getServer()->getGeographicZone();
    	} else {
    		$srvGz = "--PTS--";
    	}
        $builder
            ->add('name')
            ->add('faction', 'choice', array(
            		'choices' => array(0 => 'Empire', 1 => 'Republic'),
            		'empty_value' => "Choose a faction",
            		'property_path' => false,
            		))
            ->add('torClass', 'entity', array(
            		'class' => 'LogAnalyzerTorUtilsBundle:TorClass',
            		'label' => 'Class',
            		'empty_value' => "Choose a faction first",
            		))
            ->add('serverZone', 'choice', array(
            		'label' => "Server Zone",
            		'choices' => array("--PTS--" => "--PTS--", "US" => "US", "European" => "European", "Asia Pacific" => "Asia Pacific"),
            		'empty_value' => "Choose a zone",
            		'property_path' => false,
            		))
            ->add('server', 'entity', array(
        			'class' => 'LogAnalyzerTorUtilsBundle:Server',
            		'empty_value' => "Choose a zone first",
            		))
            ->add('user', 'entity', array(
        			'class' => 'LogAnalyzerUserBundle:User',
        			'attr' => array('style' => 'display:none'),
            		))
        ;
    }

    public function getName()
    {
        return 'loganalyzer_bundle_userbundle_torcharactertype';
    }
}
