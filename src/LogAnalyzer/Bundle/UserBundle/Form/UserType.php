<?php

namespace LogAnalyzer\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class UserType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        // add your custom field
//         $builder->remove('email')
//         		->remove('plainPassword')
//         		->remove('email');
        
//         $builder->add('email')
//         		->add('confirmEmail')
//         		->add('password');
    }

    public function getName()
    {
        return 'loganalyzer_user_registration';
    }
}