<?php

namespace LogAnalyzer\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GuildType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('server', 'entity', array(
            		'class' => 'LogAnalyzerTorUtilsBundle:Server',
            		'attr' => array('style' => 'display:none'),
            		))
            ->add('leader', 'entity', array(
            		'class' => 'LogAnalyzerUserBundle:TorCharacter',
            		'attr' => array('style' => 'display:none'),
            		))
        ;
    }

    public function getName()
    {
        return 'loganalyzer_bundle_userbundle_guildtype';
    }
}
