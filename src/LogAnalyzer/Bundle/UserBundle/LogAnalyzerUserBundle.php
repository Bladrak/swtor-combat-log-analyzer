<?php

namespace LogAnalyzer\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LogAnalyzerUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
