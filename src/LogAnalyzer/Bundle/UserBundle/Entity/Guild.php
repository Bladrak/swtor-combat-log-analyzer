<?php

namespace LogAnalyzer\Bundle\UserBundle\Entity;

use LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAnalyzer\Bundle\UserBundle\Entity\Guild
 *
 * @ORM\Table(name="Guild", uniqueConstraints={@ORM\UniqueConstraint(name="idServerName", columns={"server_id", "name"})},
 * 				indexes={@ORM\index(name="sort_name", columns={"name"}),
 * 							@ORM\index(name="join_server", columns={"server_id"}),
 * 							@ORM\index(name="join_leader", columns={"leader_id"})})
 * @ORM\Entity
 */
class Guild
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server $server
     *
     * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    /**
     * @var ArrayCollection $members
     *
     * @ORM\OneToMany(targetEntity="TorCharacter", mappedBy="guild")
     */
    private $members;

    /**
     * @var TorCharacter $leader
     *
     * @ORM\OneToOne(targetEntity="TorCharacter")
     * @ORM\JoinColumn(name="leader_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $leader;

    /**
     * @var ArrayCollection $applies
     *
     * @ORM\OneToMany(targetEntity="GuildApply", mappedBy="applied")
     */
    private $applies;

    /**
     * @var ArrayCollection $logStats
     *
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\Stats", mappedBy="referencingGuild")
     */
    private $logStats;

    /**
     * @var ArrayCollection $groupStats
     *
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats", mappedBy="guild")
     */
    private $groupStats;




    public function __construct()
    {
    	$this->members = new ArrayCollection();
    	$this->logStats = new ArrayCollection();
    	$this->groupStats = new ArrayCollection();
    	$this->applies = new ArrayCollection();
    }

    public function getStatsFieldsToAggregate()
    {
    	$keysToAggregate = array("healCritRatioValue",
    			"dmgDoneCritRatioValue", "dmgTakenCritRatioValue",
    			"healCritRatioCapacity", "dmgDoneCritRatioCapacity",
    			"totalHealReceived", "totalHealDone", "totalDamageDone",
    			"totalThreat", "totalAbsorbedDamages", "totalInterrupts",
    			"totalDispells", "dmgTakenCritRatioCapacity", "beginTime",
    			"maxHealReceived", "maxDamageDealt", "maxDamageTaken",
    			"endTime");
    	return $keysToAggregate;
    }


    public function __toString()
    {
    	return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set server
     *
     * @param string $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }

    /**
     * Get server
     *
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Add members
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $members
     */
    public function addTorCharacter(\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $members)
    {
        $this->members[] = $members;
    }

    /**
     * Get members
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Add logStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats
     */
    public function addStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats)
    {
        $this->logStats[] = $logStats;
    }

    /**
     * Get logStats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLogStats()
    {
        return $this->logStats;
    }

    /**
     * Set leader
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $leader
     */
    public function setLeader(\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $leader)
    {
        $this->leader = $leader;
    }

    /**
     * Get leader
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * Add applies
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\GuildApply $applies
     */
    public function addGuildApply(\LogAnalyzer\Bundle\UserBundle\Entity\GuildApply $applies)
    {
        $this->applies[] = $applies;
    }

    /**
     * Get pending applies
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPendingApplies()
    {
    	$ret = new ArrayCollection();
    	foreach ($this->applies as $apply)
    	{
    		if ($apply->getStatus() == GuildApply::STATUS_NEW || $apply->getStatus() == GuildApply::STATUS_READ)
    			$ret->add($apply);
    	}
    	return $ret;
    }

    /**
     * Get applies
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getApplies()
    {
        return $this->applies;
    }

    /**
     * Add groupStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats $groupStats
     */
    public function addGroupStats(\LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats $groupStats)
    {
        $this->groupStats[] = $groupStats;
    }

    /**
     * Get groupStats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getGroupStats()
    {
        return $this->groupStats;
    }
}