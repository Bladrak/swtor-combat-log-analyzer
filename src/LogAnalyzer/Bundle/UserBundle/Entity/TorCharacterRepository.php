<?php

namespace LogAnalyzer\Bundle\UserBundle\Entity;
use Doctrine\ORM\NoResultException;

use Doctrine\ORM\Query;

use Doctrine\ORM\EntityRepository;
use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;

class TorCharacterRepository extends EntityRepository {

	public function tcForShow($tcId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select('tc, g, l, tcl, s, ga')
					->from('LogAnalyzerUserBundle:TorCharacter', 'tc')
					->leftJoin('tc.guild', 'g')
					->leftJoin('tc.logs', 'l')
					->leftJoin('tc.torClass', 'tcl')
					->leftJoin('tc.server', 's')
					->leftJoin('tc.applies', 'ga')
					->where('tc.id = :id')
					->setParameter('id', $tcId);
		try {
			return $queryBuilder->getQuery()->getSingleResult();
		}catch(NoResultException $ex) {
			return null;
		}
	}

}
