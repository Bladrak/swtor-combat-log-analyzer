<?php

namespace LogAnalyzer\Bundle\UserBundle\Entity;

use LogAnalyzer\Bundle\TorUtilsBundle\Entity\TorClass;
use LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog;

/**
 * LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter
 *
 * @ORM\Table(indexes={@ORM\index(name="sort_name", columns={"name"}),
 * 						@ORM\index(name="join_class", columns={"torClass_id"}),
 * 						@ORM\index(name="join_server", columns={"server_id"}),
 * 						@ORM\index(name="join_user", columns={"user_id"}),
 * 						@ORM\index(name="join_guild", columns={"guild_id"})})
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\UserBundle\Entity\TorCharacterRepository")
 */
class TorCharacter
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var LogAnalyzer\Bundle\TorUtilsBundle\Entity\TorClass $torClass
     *
     * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\TorUtilsBundle\Entity\TorClass")
     * @ORM\JoinColumn(name="torClass_id", referencedColumnName="id")
     */
    private $torClass;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server $server
     *
     * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     */
    private $server;

    /**
     * @var User $user
     *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="characters")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var Guild $guild
     *
     * @ORM\ManyToOne(targetEntity="Guild", inversedBy="members")
     * @ORM\JoinColumn(name="guild_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $guild;


    /**
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog", mappedBy="torCharacter")
     *
     * @var ArrayCollection $logs
     */
    private $logs;

    /**
     * @var ArrayCollection $logStats
     *
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\Stats", mappedBy="referencingTorCharacter")
     */
    private $logStats;

    /**
     * @var ArrayCollection $groups
     *
     * @ORM\ManyToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats", mappedBy="members")
     */
    private $groups;


    /**
     * @var Collection $applies
     *
     * @ORM\OneToMany(targetEntity="GuildApply", mappedBy="applyant")
     */
    private $applies;



    public function __construct()
    {
    	$this->logs = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->logStats = new ArrayCollection();
    	$this->groups = new ArrayCollection();
    }


    public function getStatsFieldsToAggregate()
    {
    	$keysToAggregate = array("healCritRatioValue",
    			"dmgDoneCritRatioValue", "dmgTakenCritRatioValue",
    			"healCritRatioCapacity", "dmgDoneCritRatioCapacity",
    			"totalHealReceived", "totalHealDone", "totalDamageDone",
    			"totalThreat", "totalAbsorbedDamages", "totalInterrupts",
    			"totalDispells", "dmgTakenCritRatioCapacity", "beginTime",
    			"endTime");
		$charKeys = array("healPerCapacity", "maxHealReceived",
				"maxDamageDealt", "maxDamageTaken", "damagePerCapacity",
				"deaths", "threatAvgPerCapacity",
				"damagePerCapacity", "damageAvgPerCapacity",
				"deathsCount", "gainedXp");
    	$keysToAggregate = array_merge($keysToAggregate, $charKeys);
    	return $keysToAggregate;
    }


    /**
     *
     * @return string
     */
    public function __toString()
    {
    	return $this->getName();
    }


    /**
     * Returns the number of logs imported for this character
     *
     * @return integer
     */
    public function getLogsCount()
    {
    	return $this->getLogs()->count();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set torClass
     *
     * @param string $torClass
     */
    public function setTorClass($torClass)
    {
        $this->torClass = $torClass;
    }

    /**
     * Get torClass
     *
     * @return string
     */
    public function getTorClass()
    {
        return $this->torClass;
    }

    /**
     * Set server
     *
     * @param string $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }

    /**
     * Get server
     *
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Set user
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\User $user
     */
    public function setUser(\LogAnalyzer\Bundle\UserBundle\Entity\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get name and class concatenated
     *
     * @return string
     */
    public function getNameAndClass()
    {
    	return $this->name." - ".$this->torClass->getName();
    }

    /**
     * Add logs
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $logs
     */
    public function addCombatLog(\LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $logs)
    {
        $this->logs[] = $logs;
    }

    /**
     * Get logs
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Set guild
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\Guild $guild
     */
    public function setGuild(\LogAnalyzer\Bundle\UserBundle\Entity\Guild $guild = null)
    {
        $this->guild = $guild;
    }

    /**
     * Get guild
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }

    /**
     * Add logStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats
     */
    public function addStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats)
    {
        $this->logStats[] = $logStats;
    }

    /**
     * Get logStats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLogStats()
    {
        return $this->logStats;
    }

    /**
     * Get applies
     *
     * @return ArrayCollection
     */
    public function getApplies()
    {
        return $this->applies;
    }

    /**
     * Add groups
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats $groups
     */
    public function addGroupStats(\LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats $groups)
    {
        $this->groups[] = $groups;
    }

    /**
     * Get groups
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add apply
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\GuildApply $apply
     */
    public function addGuildApply(\LogAnalyzer\Bundle\UserBundle\Entity\GuildApply $apply)
    {
        $this->applies[] = $apply;
    }
}