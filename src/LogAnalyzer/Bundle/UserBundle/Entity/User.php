<?php

namespace LogAnalyzer\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * LogAnalyzer\Bundle\UserBundle\Entity\User
 *
 * @ORM\Table(name="la_user")
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToMany(targetEntity="TorCharacter", mappedBy="user")
     */
    private $characters;


    /**
     * @var ArrayCollection $logStats
     *
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\Stats", mappedBy="referencingUser")
     */
    private $logStats;



    public function __construct()
    {
    	parent::__construct();
    	$this->characters = new ArrayCollection();
    	$this->logStats = new ArrayCollection();
    }
    
    public function getStatsFieldsToAggregate()
    {
    	$keysToAggregate = array("healCritRatioValue",
    			"dmgDoneCritRatioValue", "dmgTakenCritRatioValue",
    			"healCritRatioCapacity", "dmgDoneCritRatioCapacity",
    			"totalHealReceived", "totalHealDone", "totalDamageDone",
    			"totalThreat", "totalAbsorbedDamages", "totalInterrupts",
    			"totalDispells", "dmgTakenCritRatioCapacity", "beginTime",
    			"endTime");
    	return $keysToAggregate;
    }
    

    public function hasCharacter(TorCharacter $char)
    {
    	return $this->hasCharacterId($char->getId());
    }

    public function hasCharacterId($charId)
    {
    	foreach ($this->getCharacters() as $tC)
    	{
    		if ($tC->getId() == $charId){
    			return true;
    		}
    	}
    	return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Add characters
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $characters
     */
    public function addTorCharacter(\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $characters)
    {
        $this->characters[] = $characters;
    }

    /**
     * Get characters
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Add logStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats
     */
    public function addStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats)
    {
        $this->logStats[] = $logStats;
    }

    /**
     * Get logStats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLogStats()
    {
        return $this->logStats;
    }
}