<?php

namespace LogAnalyzer\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAnalyzer\Bundle\UserBundle\Entity\GuildApply
 *
 * @ORM\Table(indexes={@ORM\index(name="join_applyant", columns={"applyant_id"}),
 * 							@ORM\index(name="join_guild", columns={"guild_id"})})
 * @ORM\Entity
 */
class GuildApply
{
	const STATUS_NEW = 0;
	const STATUS_READ = 1;
	const STATUS_ACCEPTED = 2;
	const STATUS_DECLINED = 3;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $applyDate
     *
     * @ORM\Column(name="applyDate", type="datetime")
     */
    private $applyDate;

    /**
     * @var smallint $status
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var string $reason
     *
     * @ORM\Column(name="reason", type="string", length=500, nullable=true)
     */
    private $reason;

    /**
     * @var TorCharacter $applyant
     *
     * @ORM\ManyToOne(targetEntity="TorCharacter", inversedBy="applies")
     * @ORM\JoinColumn(name="applyant_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $applyant;

    /**
     * @var Guild $applied
     *
     * @ORM\ManyToOne(targetEntity="Guild", inversedBy="applies")
     * @ORM\JoinColumn(name="guild_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $applied;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set applyDate
     *
     * @param datetime $applyDate
     */
    public function setApplyDate($applyDate)
    {
        $this->applyDate = $applyDate;
    }

    /**
     * Get applyDate
     *
     * @return datetime
     */
    public function getApplyDate()
    {
        return $this->applyDate;
    }

    /**
     * Set status
     *
     * @param smallint $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return smallint
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reason
     *
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set applyant
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $applyant
     */
    public function setApplyant(\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $applyant)
    {
        $this->applyant = $applyant;
    }

    /**
     * Get applyant
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter
     */
    public function getApplyant()
    {
        return $this->applyant;
    }

    /**
     * Set applied
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\Guild $applied
     */
    public function setApplied(\LogAnalyzer\Bundle\UserBundle\Entity\Guild $applied)
    {
        $this->applied = $applied;
    }

    /**
     * Get applied
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\Guild
     */
    public function getApplied()
    {
        return $this->applied;
    }
}