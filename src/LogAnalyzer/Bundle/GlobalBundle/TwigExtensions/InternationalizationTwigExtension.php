<?php

namespace LogAnalyzer\Bundle\GlobalBundle\TwigExtensions;

class InternationalizationTwigExtension extends \Twig_Extension {

	public function getFilters()
	{
		return array(
				'localeNumberFormat' => new \Twig_Filter_Function(
						'\LogAnalyzer\Bundle\GlobalBundle\TwigExtensions\InternationalizationTwigExtension::numberFormatFilter'),
			);
	}

	public static function numberFormatFilter($value, $style = 'decimal', $digits = 0)
	{
		if ($value === null)
			return $value;

		$values = array(
					'default' => \NumberFormatter::DEFAULT_STYLE,
					'decimal' => \NumberFormatter::DECIMAL,
					'currency' => \NumberFormatter::CURRENCY,
					'percent' => \NumberFormatter::PERCENT,
					'scientific' => \NumberFormatter::SCIENTIFIC,
					'duration' => \NumberFormatter::DURATION,
		);

		$formatter = new \NumberFormatter(
				\Locale::getDefault(),
				$values[$style]
				);
		$formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, $digits);
		$result = $formatter->format($value);
		if ($result === false) {
			throw new \InvalidArgumentException(sprintf('The value "%s" of type %s cannot be formatted.', $value, gettype($value)));
		}

		return $result;
	}

	public function getName()
	{
		return "LA_IntlExtension";
	}

}