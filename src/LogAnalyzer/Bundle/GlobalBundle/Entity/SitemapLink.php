<?php

namespace LogAnalyzer\Bundle\GlobalBundle\Entity;

class SitemapLink
{
	/**
	 *
	 * @var string
	 */
	private $link;

	/**
	 *
	 * @var integer
	 */
	private $priority;

	/**
	 *
	 * @var \DateTime
	 */
	private $lastModDate;

	/**
	 *
	 * @var string
	 */
	private $changeFreq;
	
	/**
	 * 
	 * @var array
	 */
	private $params;

	public function __construct($route, $params = array(), $priority, \DateTime $lastModDate = null, $changeFreq = null)
	{
		$this->link = $route;
		$this->params = $params;
		$this->priority = $priority;
		$this->lastModDate = $lastModDate;
		$this->changeFreq = $changeFreq;
	}


	public function getLink()
	{
		return $this->link;
	}

	public function setLink($link)
	{
		$this->link = $link;
	}
	
	public function getParams($lang = null)
	{
		if ($lang === null)
			return $this->params;
		return array_merge(array('_locale' => $lang), $this->params);
	}
	
	public function setParams($params)
	{
		$this->params = $params;
	}
	
	public function getPriority()
	{
		return $this->priority;
	}

	public function setPriority($priority)
	{
		$this->priority = $priority;
	}

	public function getLastModDate()
	{
		return $this->lastModDate;
	}

	public function setLastModDate(\DateTime $lastModDate)
	{
		$this->lastModDate = $lastModDate;
	}

	public function getChangeFreq()
	{
		return $this->changeFreq;
	}

	public function setChangeFreq($changeFreq)
	{
		$this->changeFreq = $changeFreq;
	}
}