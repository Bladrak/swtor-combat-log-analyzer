<?php


namespace LogAnalyzer\Bundle\GlobalBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class SitemapCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName("sitemap:generate")
			->setDescription("Generates the sitemap.xml file for SEO.")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
	}
}