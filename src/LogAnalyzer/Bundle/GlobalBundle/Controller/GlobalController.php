<?php
namespace LogAnalyzer\Bundle\GlobalBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use LogAnalyzer\Bundle\GlobalBundle\Entity\SitemapLink;

use LogAnalyzer\Bundle\UserBundle\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use LogAnalyzer\Bundle\GlobalBundle\Entity\Enquiry;
use LogAnalyzer\Bundle\GlobalBundle\Form\EnquiryType;


/**
 * Global controller.
 *
 * @Route("/global")
 */
class GlobalController extends Controller
{
	/**
	 * @Route("/contact", name="contact")
	 * @Template()
	 */
	public function indexAction()
	{
		$enquiry = new Enquiry();
		$form = $this->createForm(new EnquiryType(), $enquiry);

		return array('form' => $form->createView());
	}
	
	/**
	 * @Route("/changelang/{lang}/{route}/{params}", defaults={"route"="_welcome", "params"=null}, name="change_language")
	 * @Template()
	 */
	public function changeLanguageAction($lang, $route, $params)
	{
		if ($params === null || !is_array($params))
			$params = array();
		if ($route === "_internal")
			$route = "_welcome";
		
		return $this->redirect($this->generateUrl($route, array_merge(array('_locale' => $lang), $params)));
	}

	/**
	 * @Template()
	 */
	public function sitemapIndexAction()
	{
		$links = array();
		$lastModDate = new \DateTime();

		$types = array("general", "user", "guild", "character", "combatlog");


		foreach ($types as $type)
			$links[] = new SitemapLink('sitemap', array("type" => $type), 0.0, $lastModDate);

// 		$fightCount = $this->getDoctrine()->getEntityManager()->createQuery("Select COUNT(f.id) FROM LogAnalyzerCombatLogBundle:Fight f")->getSingleScalarResult();
// 		for ($i=0; $i < $fightCount / 20000; $i++)
// 			$links[] = new SitemapLink('sitemap', array("type" => "fight".($i+1)), 0.0, $lastModDate);

		$xmlResp = new Response();
		$xmlResp->headers->set('Content-Type', 'text/xml');
		return $this->render('LogAnalyzerGlobalBundle:Global:sitemapIndex.xml.twig',
				array(
						'links' => $links,
				),
				$xmlResp
		);
	}


	/**
	 * @Route("/sitemap/{type}.xml", name="sitemap")
	 * @Template()
	 */
	public function sitemapAction($type)
	{
		$links = array();
		$lastModDate = new \DateTime();

		if ($type == "general")
		{
			$links[] = new SitemapLink('_welcome', array(), 1.0, null, null);
			$links[] = new SitemapLink('contact', array(), 0.9, null, null);
			$links[] = new SitemapLink('stats_show_sample', array(), 0.9, $lastModDate, "hourly");

			// Server rankings
			$statsColumns = array(
					"overallDps" => "DPS",
					"overallHps" => "HPS",
					"overallTps" => "TPS",
					"totalDamageDone" => "Total Damage Done",
					"totalHealDone" => "Total Heal Done",
					"totalThreat" => "Total Threat",
					"dmgDoneCritRatioCapacity" => "Damage critic percentage",
					"healCritRatioCapacity" => "Heal critic percentage",
					"deathsCount" => "Number of deaths");

			$servers = $this->getDoctrine()->getRepository('LogAnalyzerTorUtilsBundle:Server')->getServerListAsArray();

			foreach ($statsColumns as $stat => $statName)
			{
				$links[] = new SitemapLink('ranking_stat', array('stat' => $stat), 0.85, $lastModDate, "hourly");
				foreach ($servers as $serversByZone)
				{
					foreach ($serversByZone as $srvId => $server)
					{
						$links[] = new SitemapLink('ranking_stat_server', array('stat' => $stat, 'server' => $srvId), 0.8, $lastModDate, "daily");
					}
				}
			}

			unset($servers);
		} else {

			// Now dealing with guilds, users, characters, combat logs & fights stats
			$ids = $this->getDoctrine()->getRepository('LogAnalyzerUserBundle:User')->getLinks($type);
			$freqChange = array(
							"fight" => "never", 
							"user" => "daily", 
							"guild" => "hourly", 
							"character" => "daily", 
							"combatlog" => "never"
							);
			if (strpos($type, "fight") !== false)
				$type = "fight";

			foreach ($ids as $value)
			{
				if ($type != "user" && $type != "fight" && $type != "combatlog")
				{
					$links[] = new SitemapLink($type.'_show', array('id' => $value["id"]), 0.75, $lastModDate, $freqChange[$type]);
				}
				if ($type != "guild")
					$links[] = new SitemapLink('stats_show_'.$type, array('id' => $value["id"]), 0.75, $lastModDate, $freqChange[$type]);
			}
		}
		$xmlResp = new Response();
		$xmlResp->headers->set('Content-Type', 'text/xml');

		return $this->render('LogAnalyzerGlobalBundle:Global:sitemap.xml.twig',
					array(
							'links' => $links,
							),
					$xmlResp
				);
	}


	public function topMenuAction($currentRoute)
	{
		return $this->render('LogAnalyzerGlobalBundle:Global:topMenu.html.twig',
				array(
						'cRoute' => $currentRoute,
// 						'servers' => $this->getDoctrine()->getRepository('LogAnalyzerTorUtilsBundle:Server')->getServerListAsArray(),
// 						'statsColumns' => array(
// 								"overallDps" => "DPS",
// 								"overallHps" => "HPS",
// 								"overallTps" => "TPS",
// 								"totalDamageDone" => "Total Damage Done",
// 								"totalHealDone" => "Total Heal Done",
// 								"totalThreat" => "Total Threat",
// 								"dmgDoneCritRatioCapacity" => "Damage critic percentage",
// 								"healCritRatioCapacity" => "Heal critic percentage",
// 								"deathsCount" => "Number of deaths"),
				));
	}

	public function socialAction()
	{
		return $this->render('LogAnalyzerGlobalBundle:Global:social.html.twig',
				array(
				));
	}

	public function socialShareStatsAction($url)
	{
		return $this->render('LogAnalyzerGlobalBundle:Global:socialShareStats.html.twig',
				array(
						'url' => $url,
				));
	}

	public function sidebarAction($currentRoute)
	{
		$statsColumns = array(
								"overallDps" 				=> $this->get('translator')->trans("DPS"),
								"overallHps" 				=> $this->get('translator')->trans("HPS"),
								"overallTps" 				=> $this->get('translator')->trans("TPS"),
								"totalDamageDone" 			=> $this->get('translator')->trans("Total Damage Done"),
								"totalHealDone" 			=> $this->get('translator')->trans("Total Heal Done"),
								"totalThreat" 				=> $this->get('translator')->trans("Total Threat"),
								"dmgDoneCritRatioCapacity"	=> $this->get('translator')->trans("Damage critic percentage"),
								"healCritRatioCapacity"		=> $this->get('translator')->trans("Heal critic percentage"),
								"deathsCount" 				=> $this->get('translator')->trans("Number of deaths"));

		$servers = $this->getDoctrine()->getRepository('LogAnalyzerTorUtilsBundle:Server')->getServerListAsArray();

		$user= $this->container->get('security.context')->getToken()->getUser();
		if (!is_object($user) || !$user instanceof User) // User not authentified
			return $this->render('LogAnalyzerGlobalBundle:Global:sidebar.html.twig',
					array(
							'cRoute' => $currentRoute,
							'treeStatsInfos' => array(),
							'statsColumns' => $statsColumns,
							'servers' => $servers));

		// Otherwise we compute the sidebar stats datas
		$user = $this->getDoctrine()->getEntityManager()->getRepository('LogAnalyzerUserBundle:User')->getSidebarDatasForUser($user);
		$treeInfos = array();
		if (is_object($user) && $user instanceof User) {
			$treeInfos["statsurl"] = $this->get('router')->generate('stats_show_user', array('id' => $user->getId()));
			$treeInfos["showurl"] = $this->get('router')->generate('_welcome');
			$treeInfos["selected"] = ($currentRoute == $treeInfos["statsurl"]) ? 'self' : false;
			$treeInfos["children"] = array();
			foreach ($user->getCharacters() as $tc) { // TorCharacter
				$treeInfos["children"][$tc->getId()] = array();
				$treeInfos["children"][$tc->getId()]["statsurl"] = $this->get('router')->generate('stats_show_character', array('id' => $tc->getId()));
				$treeInfos["children"][$tc->getId()]["showurl"] = $this->get('router')->generate('character_show', array('id' => $tc->getId()));
				$treeInfos["children"][$tc->getId()]["id"] = $tc->getId();
				$treeInfos["children"][$tc->getId()]["desc"] = $tc->getName();
				if ($currentRoute == $treeInfos["children"][$tc->getId()]["statsurl"]) {
					$treeInfos["children"][$tc->getId()]["selected"] = "self";
					$treeInfos["selected"] = "child";
				} else {
					$treeInfos["children"][$tc->getId()]["selected"] = false;
				}
				if ($tc->getGuild() != null)
				{
					$treeInfos["children"][$tc->getId()]["guild"]["desc"] = $tc->getGuild()->getName();
					$treeInfos["children"][$tc->getId()]["guild"]["id"] = $tc->getGuild()->getId();
					$treeInfos["children"][$tc->getId()]["guild"]["statsurl"] = $this->get('router')->generate('guild_show', array('id' => $tc->getGuild()->getId()));
					$treeInfos["children"][$tc->getId()]["guild"]["showurl"] = $this->get('router')->generate('guild_show', array('id' => $tc->getGuild()->getId()));
					if ($currentRoute == $treeInfos["children"][$tc->getId()]["guild"]["statsurl"]) {
						$treeInfos["children"][$tc->getId()]["guild"]["selected"] = "self";
						$treeInfos["children"][$tc->getId()]["selected"] = "child";
						$treeInfos["selected"] = "child";
					} else {
						$treeInfos["children"][$tc->getId()]["guild"]["selected"] = false;
					}
				}
				$treeInfos["children"][$tc->getId()]["children"] = array();
				foreach ($tc->getLogs() as $cl) { // CombatLog
					$treeInfos["children"][$tc->getId()]["children"][$cl->getId()] = array();
					$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["statsurl"] = $this->get('router')->generate('stats_show_combatlog', array('id' => $cl->getId()));
					$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["id"] = $cl->getId();
//					$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["desc"] = $cl->getClImportTime()->format("F jS, Y \\a\\t H:ia");
					if ($currentRoute == $treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["statsurl"]) {
						$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["selected"] = "self";
						$treeInfos["selected"] = "child";
						$treeInfos["children"][$tc->getId()]["selected"] = "child";
					} else {
						$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["selected"] = false;
					}
// 					$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"] = array();
// 					foreach ($cl->getFights() as $fgt) { // Fights
// 						$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()] = array();
// 						$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()]["statsurl"] = $this->get('router')->generate('stats_show_fight', array('id' => $fgt->getId()));
// 						$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()]["id"] = $fgt->getId();
// 						$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()]["desc"] = "Fight versus ".$fgt->getVersusTarget();
// 						if ($currentRoute == $treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()]["statsurl"]) {
// 							$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()]["selected"] = "self";
// 							$treeInfos["selected"] = "child";
// 							$treeInfos["children"][$tc->getId()]["selected"] = "child";
// 							$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["selected"] = "child";
// 						} else {
// 							$treeInfos["children"][$tc->getId()]["children"][$cl->getId()]["children"][$fgt->getId()]["selected"] = false;
// 						}
// 					}
				}
			}
		}
		return $this->render('LogAnalyzerGlobalBundle:Global:sidebar.html.twig',
				array(
					'cRoute' => $currentRoute,
					'treeStatsInfos' => $treeInfos,
					'servers' => $servers,
					'statsColumns' => $statsColumns,
				));
	}

	/**
	 * @Route("/sendContact", name="sendContact")
	 * @Template()
	 */
	public function contactAction()
	{
		$enquiry = new Enquiry();
		$form = $this->createForm(new EnquiryType(), $enquiry);

		$request = $this->getRequest();
		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
				// Perform some action, such as sending an email

				$message = \Swift_Message::newInstance()
				->setSubject('SWTOR Log Analyzer Contact : '.$enquiry->getSubject())
				->setFrom($enquiry->getEmail())
				->setReplyTo($enquiry->getEmail())
				->setTo('h.briand@gmail.com')
				->setBody($this->renderView('LogAnalyzerGlobalBundle:Global:email.txt.twig', array('enquiry' => $enquiry)));
				$this->get('mailer')->send($message);

				$this->get('session')->setFlash('notice', $this->get('translator')->trans('Your contact enquiry was successfully sent. Thank you!'));

				// Redirect - This is important to prevent users re-posting
				// the form if they refresh the page
				return $this->redirect($this->generateUrl('contact'));
			}
		}

		return $this->render('LogAnalyzerGlobalBundle:Global:contact.html.twig', array(
				'form' => $form->createView()
		));
	}
}
