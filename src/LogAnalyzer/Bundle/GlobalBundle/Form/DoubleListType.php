<?php
namespace LogAnalyzer\Bundle\GlobalBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\FormViewInterface;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Bridge\Doctrine\Form\DataTransformer\EntitiesToArrayTransformer;

use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityChoiceList;

use Symfony\Bundle\DoctrineBundle\Registry;

use Symfony\Component\Form\FormInterface;

use Symfony\Component\Form\FormView;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;

class DoubleListType extends EntityType
{
	
	/**
	 * {@inheritdoc}
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		parent::setDefaultOptions($resolver);
	
		$resolver->replaceDefaults(array('multiple'=>true));
	}

//     protected $registry;

//     protected $choices;

//     public function __construct(Registry $registry)
//     {
//         $this->registry = $registry;
//     }

//     /**
//      * {@inheritdoc}
//      */
//     public function buildForm(FormBuilderInterface $builder, array $options)
//     {
//          $builder
//                ->prependClientTransformer(new EntitiesToArrayTransformer($options['choice_list']))
//                ;

//         $this->choices = $options['choice_list']->getChoices();

//         unset($options['choices']);

//     }


//     /**
//      * {@inheritdoc}
//      */
//     public function buildView(FormViewInterface $view, FormInterface $form, array $options)
//     {
//         $values = $view->get('value');

//         $selecteds = array_flip($values);
//         $choices_selected = $choices_unselected = array();

//         //Rebuilds choices
//         foreach ($this->choices as $key => $choice) {
//             if (isset($selecteds[$key])) {
//                 $choices_selected[$key] = $choice;
//             } else {
//                 $choices_unselected[$key] = $choice;
//             }
//         }

//         $view->set('choices_selected', $choices_selected);
//         $view->set('choices_unselected', $choices_unselected);
//     }

// 	public function getParent()
// 	{
// 	    return 'form';
// 	}
	
// 	public function setDefaultOptions(OptionsResolverInterface $resolver)
// 	{
// 	    $resolver->setDefaults(array(
// 	        'compound' => false,
// 	    ));
// 	}

//     public function getDefaultOptions()
//     {
//         $defaultOptions = array(
//             'em'                => null,
//             'class'             => null,
//             'property'          => null,
//             'query_builder'     => null,
//             'choices'           => null,
//         );

// //         $options = array_replace($defaultOptions, $options);
        
// //         if (!isset($options['choice_list'])) {
//             $defaultOptions['choice_list'] = new EntityChoiceList(
//                 $this->registry->getEntityManager($options['em']),
//                 $options['class'],
//                 $options['property'],
//                 $options['query_builder'],
//                 $options['choices']
//             );
// //         }

//         return $defaultOptions;
//     }

	public function getName()
	{
		return 'double_list';
	}
}