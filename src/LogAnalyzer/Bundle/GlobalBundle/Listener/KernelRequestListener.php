<?php

namespace LogAnalyzer\Bundle\GlobalBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use Symfony\Component\HttpKernel\HttpKernelInterface;

class KernelRequestListener {
	
	/**
	 * 
	 * @var ContainerInterface
	 */
	private $container;
	
	public function onKernelRequest(GetResponseEvent $event)
	{
		if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
			return;
		}
	
		$request = $event->getRequest();
		if ($request->getRequestFormat() !== 'html') {
			return;
		}
		
		
		
		if( ($newLocale = $this->newLocale($request)) && $newLocale !== $event->getRequest()->getLocale())
		{
			$route = $request->get('_route');
			if (stripos($route, 'sitemap') !== false)
				return;
			//$event->getRequest()->setLocale($newLocale);
			$params = $request->get('_route_params');
			$params['_locale'] = $newLocale;
			
			$redirectResponse = new RedirectResponse($this->getContainer()->get('router')->generate($route, $params));
			$redirectResponse->headers->setCookie(new Cookie('sla_locale', $newLocale, time() + 3600 * 24 * 7));
			
			$event->setResponse($redirectResponse);
		}
	}
	
	public function setContainer(ContainerInterface $container)
	{
		$this->container = $container;
	}
	
	/**
	 * @return ContainerInterface
	 */
	public function getContainer()
	{
		return $this->container;
	}
	
	private function newLocale(Request $request)
	{
		if (stripos($request->headers->get('referer'), $request->server->get('HTTP_HOST')) !== false)
			return false;
		if ($request->get('sla_locale') !== null)
			return false;
		$locale = $request->getPreferredLanguage();
		if ($locale !== null)
			$locale = substr($locale, 0, 2);
		return $locale;
	}
}
