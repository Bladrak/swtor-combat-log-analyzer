<?php

namespace LogAnalyzer\Bundle\RankingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\TorUtilsBundle\Entity\ServerRepository;

/**
 *
 * @Route("/ranking")
 */

class DefaultController extends Controller
{
    /**
     * @Route("/{stat}", name="ranking_stat")
     * @Template()
     */
    public function rankingForStatAction($stat)
    {
        return array(
        		'ranks' => $this->get('log_analyzer_ranking.ranking_service')->getRankingForColumn($stat),
        		'statColumn' => $stat,
        		'displayServer' => true);
    }

    /**
     * @Route("/{stat}/{server}", name="ranking_stat_server")
     * @Template("LogAnalyzerRankingBundle:Default:rankingForStat.html.twig")
     */
    public function rankingForStatServerAction($stat, $server)
    {
    	$ranks = $this->get('log_analyzer_ranking.ranking_service')->getRankingForColumn($stat, $server);
    	if (count($ranks) > 0)
    	{
	    	$serverName = $ranks[0]->getReferencingTorCharacter()->getServer();
    	} else {
    		$serverName = $this->getDoctrine()->getRepository("LogAnalyzerTorUtilsBundle:Server")->find($server);
    	}
        return array(
        		'ranks' => $ranks,
        		'statColumn' => $stat,
        		'server' => $serverName,
        		'displayServer' => false);
    }
}
