<?php

namespace LogAnalyzer\Bundle\RankingBundle\Service;

use LogAnalyzer\Bundle\TorUtilsBundle\Entity\Server;

use Doctrine\ORM\EntityManager;

class RankingService
{
	/**
	 *
	 * @var EntityManager
	 */
	protected $em = null;

	/**
	 * @return EntityManager
	 */
	public function getEntityManager() { return $this->em; }

	public function __construct($em)
	{
		$this->em = $em;
	}

	/**
	 * Returns the ranking for the specified stats column
	 *
	 * @param string $statsColumnName
	 * @param string $server
	 * @param integer $startOffest
	 * @param integer $limit
	 */
	public function getRankingForColumn($statsColumnName, $server = null, $startOffset = 0, $limit = 100)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('tc, partial s.{id, '.$statsColumnName.'}')
			->from('LogAnalyzerStatsBundle:Stats', 's')
			->leftJoin('s.referencingTorCharacter', 'tc')
			->where('s.referencingTorCharacter = tc')
			->orderBy('s.'.$statsColumnName, 'DESC');
		if ($limit > 0){
			$qb->setFirstResult($startOffset);
			$qb->setMaxResults($limit);
		}

		if ($server != null)
		{
			$qb->andWhere('tc.server = :srv')
				->setParameter('srv', $server);
		}
		
		return $qb->getQuery()->getResult();
	}
}