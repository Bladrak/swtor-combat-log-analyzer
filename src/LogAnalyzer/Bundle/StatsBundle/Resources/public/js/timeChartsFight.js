function timeChartDamageSpline(renderTo, title, axisTitle, seriesStr, deathsOverTimeData) {
	var series = eval(seriesStr);

	new Highcharts.StockChart({
		chart : {
			renderTo : renderTo,
			type: 'spline'
		},
		rangeSelector: {
			enabled: false,
		},
		credits : {
			enabled : false
		},
		title : {
			text : title
		},
		xAxis : {
			type : 'datetime',
			dateTimeLabelFormats: {
				second: '%H:%M:%S',
			},
			range: 2 * 60 * 1000,
			plotLines: deathsOverTimeData,
		},
		yAxis : {
			min: 0,
			title : {
				text : axisTitle
			}
		},
		tooltip: {
	    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
	    	valueDecimals: 2
	    },
		series : series
	});
}

function timeChartSpline(renderTo, title, axisTitle, seriesStr, deathsOverTimeData) {
	var series = eval(seriesStr);

	new Highcharts.StockChart({
		chart : {
			renderTo : renderTo,
			type: 'spline'
		},
		rangeSelector: {
			enabled: false,
		},
		credits : {
			enabled : false
		},
		title : {
			text : title
		},
		xAxis : {
			type : 'datetime',
			dateTimeLabelFormats: {
				second: '%H:%M:%S',
			},
			range: 2 * 60 * 1000,
			plotLines: deathsOverTimeData,
		},
		yAxis : {
			min: 0,
			title : {
				text : axisTitle
			}
		},
		tooltip: {
	    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
	    	valueDecimals: 2
	    },
		series : [{
			id: 'dataseries',
			data: series,
			marker: {
				radius: 1,
			},
			showInLegend: false
		}
		],
	});
}
