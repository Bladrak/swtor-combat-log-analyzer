<?php

namespace LogAnalyzer\Bundle\StatsBundle\Service;

use Doctrine\ORM\EntityManager;

use LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats;

use LogAnalyzer\Bundle\StatsBundle\Entity\Stats;

use LogAnalyzer\Bundle\CombatLogBundle\Service\TranslationMappingService;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight;
use Doctrine\Common\Collections\ArrayCollection;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry;
use Doctrine\ORM\NoResultException;
use LogAnalyzer\Bundle\UserBundle\Entity\User;
use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;
use LogAnalyzer\Bundle\UserBundle\Entity\Guild;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog;
use Doctrine\ORM\EntityRepository;

/**
 * StatsService
 */
class StatsService {

	/**
	 *
	 * @var EntityManager
	 */
	protected $em = null;
	/**
	 * @var TranslationMappingService $tmService
	 */
	protected $tmService = null;

	/**
	 * @return EntityManager
	 */
	public function getEntityManager() { return $this->em; }

	public function __construct(EntityManager $em, TranslationMappingService $tmService)
	{
		$this->em = $em;
		$this->tmService = $tmService;
	}


	/**
	 * Converts a datetime object to milliseconds
	 *
	 * @param \DateTime $dateTime
	 * @return int
	 */
	public static function dateTimeToMilliseconds(\DateTime $dateTime = null)
	{
		if ($dateTime == null)
			$dateTime = new \DateTime();
		return $dateTime->getTimestamp()*1000 + $dateTime->format("u")/1000;
	}

	public function getStatsFor($statsableObject) {

		if ($statsableObject instanceof GroupStats)
		{
			if ($statsableObject->getAggregatedStats() != null && $statsableObject->getAggregatedStats()->getToUpdate() == false)
			{
				return $statsableObject->getAggregatedStats();
			}
			$stats = new Stats();
			$arrayStats = $this->statsArrayForObject($statsableObject);
			$statsableObject->setStats(new ArrayCollection($this->fightStatsForGroupStats($statsableObject)));
			$stats->computeWithStats($arrayStats);
			$statsableObject->setAggregatedStats($stats);
			$this->getEntityManager()->persist($stats);
			$this->getEntityManager()->persist($statsableObject);
			$this->getEntityManager()->flush();
			return $stats;

		}
		if ($statsableObject instanceof CombatLog
				|| $statsableObject instanceof Fight
				|| $statsableObject instanceof Guild
				|| $statsableObject instanceof TorCharacter
				|| $statsableObject instanceof User) {
			// We try to fetch the stats object from the database
			$qb = $this->getEntityManager()->createQueryBuilder();
			$qb->select('partial s.{id,beginTime,endTime,deathsCount,dmgDoneCritRatioValue,dmgDoneCritRatioCapacity,dmgTakenCritRatioValue,dmgTakenCritRatioCapacity,gainedXp,healCritRatioValue,healCritRatioCapacity,maxDamageDealt,maxDamageTaken,maxHealDone,maxHealReceived,overallDps,overallHps,overallTps,totalAbsorbedDamages,totalDamageDone,totalDamageTaken,totalInterrupts,totalHealDone,totalHealReceived,totalDispells,totalThreat,xps,toUpdate,deaths}')
					->from('LogAnalyzer\Bundle\StatsBundle\Entity\Stats', 's');
			if ($statsableObject instanceof CombatLog) {
				$qb->where('s.referencingCombatLog = :object_id');
			} else if ($statsableObject instanceof Guild) {
				$qb->where('s.referencingGuild = :object_id');
			} else if ($statsableObject instanceof User) {
				$qb->where('s.referencingUser = :object_id');
			} else if ($statsableObject instanceof Fight) {
				$qb->where('s.referencingFight = :object_id');
			} else if ($statsableObject instanceof TorCharacter) {
				$qb->where('s.referencingTorCharacter = :object_id');
			}
			$qb->setParameter('object_id', $statsableObject->getId());

			try {
				$retStat = $qb->getQuery()->getSingleResult();
				if ($retStat->getToUpdate() == true && !$statsableObject instanceof Fight) {
					$retStatId = $retStat->getId();
					$this->getEntityManager()->detach($retStat);
					unset($retStat);
					$qb = $this->getEntityManager()->createQueryBuilder();
					$retStat = $qb->select('s')->from('LogAnalyzer\Bundle\StatsBundle\Entity\Stats', 's')->where('s.id = '.$retStatId)->getQuery()->getSingleResult();
					$retStat->computeWithStats($this->statsArrayForObject($statsableObject));
					$this->getEntityManager()->persist($retStat);
					$this->getEntityManager()->flush();
				}
				return $retStat;
			} catch (NoResultException $ex) {
				// The stats hasn't been processed yet... So we will !
				if ($statsableObject instanceof Fight) {
					try {
						return $this->statsForFight($statsableObject);
					} catch (\Exception $ex) {
						return null;
					}
				}
				$stats = new Stats();
				$stats->setReferencingObject($statsableObject);
				$stats->computeWithStats($this->statsArrayForObject($statsableObject));
				$statsableObject->addStats($stats);
				$this->getEntityManager()->persist($stats);
				$this->getEntityManager()->persist($statsableObject);
				$this->getEntityManager()->flush();
				return $stats;
			}
		} else {
			throw new Exception("Class not supported for computing statistics");
		}
	}

	public function fightStatsForGroupStats(GroupStats $gs)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('partial s.{id}')
			->from('LogAnalyzer\Bundle\StatsBundle\Entity\Stats', 's')
			->leftJoin('s.referencingFight', 'f')
			->leftJoin('f.combatLog', 'cl')
			->leftJoin('cl.torCharacter', 'tc')
			->leftJoin('tc.groups', 'sg')
			->where('sg = :groupStats')
			->andWhere('s.beginTime >= sg.fromDate')
			->andWhere('s.endTime <= sg.toDate');
		$qb->setParameter('groupStats', $gs);
		return $qb->getQuery()->getResult();

	}

	public function statsArrayForObject($statsableObject)
	{
		$elemsToFetch = $statsableObject->getStatsFieldsToAggregate();
		$partialStr = "{id,";
		foreach($elemsToFetch as $elem)
		{
			$partialStr .= $elem.",";
		}
		$partialStr = substr($partialStr, 0, -1);
		$partialStr .= "}";
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('partial s.'.$partialStr)
			->from('LogAnalyzer\Bundle\StatsBundle\Entity\Stats', 's');
		if (!$statsableObject instanceof CombatLog) { // CombatLog
			$qb->leftJoin('s.referencingCombatLog', 'cl');
			if (!$statsableObject instanceof TorCharacter) { // TorCharacter
				$qb->leftJoin('cl.torCharacter', 'tc');
				if ($statsableObject instanceof Guild) { // Guild
					$qb->where('tc.guild = :object');
				}
				if ($statsableObject instanceof User) { // User
					$qb->where('tc.user = :object');
				}
				if ($statsableObject instanceof GroupStats)
				{
					$qb->leftJoin('tc.groups', 'sg')
						->where('sg = :object')
						->andWhere('s.beginTime >= sg.fromDate')
						->andWhere('s.endTime <= sg.toDate');
				}
			} else {
				$qb->where('cl.torCharacter = :object');
			}
		} else {
			$qb->leftJoin('s.referencingFight', 'f');
			$qb->where('f.combatLog = :object');
		}
		$qb->setParameter('object', $statsableObject);
		return $qb->getQuery()->getResult();
	}


	private function updateParentStats(Fight $fight)
	{
		$this->getEntityManager()->getRepository("LogAnalyzerStatsBundle:GroupStats")->updateStatsForFight($fight);
		foreach ($fight->getCombatLog()->getLogStats() as $clStat)
		{
			$clStat->setToUpdate(true);
		}
		foreach($fight->getCombatLog()->getTorCharacter()->getLogStats() as $tcStat)
		{
			$tcStat->setToUpdate(true);
		}
		foreach($fight->getCombatLog()->getTorCharacter()->getUser()->getLogStats() as $usrStat)
		{
			$usrStat->setToUpdate(true);
		}
		if ($fight->getCombatLog()->getTorCharacter()->getGuild() != null)
		{
			foreach ($fight->getCombatLog()->getTorCharacter()->getGuild()->getLogStats() as $gStat)
			{
				$gStat->setToUpdate(true);
			}
		}
	}

	public function statsForFight(Fight $statsableObject)
	{
		if ($statsableObject == null || $statsableObject->getCombatLog() == null || $statsableObject->getCombatLog()->getTorCharacter() == null)
			throw new \Exception("Fight object too old to be processed");

		// Querying for string ids related to heal and dps
		$healStringId = $this->tmService->getKey("Heal");
		$dmgStringId = $this->tmService->getKey("Damage");
		$deathStringId = $this->tmService->getKey("Death");
		$gainXpStringId = $this->tmService->getKey("GainXp");

		$charName = $statsableObject->getCombatLog()->getTorCharacter()->getName();

		$computedStats = array();

		$firstTimeStamp = null;
		$dpsSum = $hpsSum = $tpsSum = 0;
		$sourceName = $statsableObject->getCombatLog()->getTorCharacter()->getName();
		$emptyTm = $this->tmService->get("", "");

		$computedStats["endTime"] = new \DateTime("01-01-1970");
		$computedStats["startTime"] = new \DateTime("01-01-2500");

		foreach ($statsableObject->getEntries() as $element) {
			$element->initWithEmptyTm($emptyTm);
			// Deaths
			if ($element->getEffectName() != null && $element->getEffectName()->getStringId() == $deathStringId) {
				if ($element->getTarget()->getStringValue() == $charName) {
					$computedStats["Deaths"]["Self"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $element->getTarget();
				} elseif ($element->getTargetIsPlayer()) {
					$computedStats["Deaths"]["Player"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $element->getTarget();
				} else {
					$computedStats["Deaths"]["Monster"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $element->getTarget();
				}
			}

			//XP
			if ($element->getEffectName() != null && $element->getEffectName()->getStringId() == $gainXpStringId) {
				if (isset($computedStats["XpGained"])) {
					$computedStats["XpGained"] += $element->getValue();
				} else {
					$computedStats["XpGained"] = $element->getValue();
				}
			}

			// Dmg/Healovertime & D/HPSOverTime
			if ($firstTimeStamp == null){
				$firstTimeStamp = StatsService::dateTimeToMilliseconds($element->getLogTimestamp());
			}
			$mSecsElapsed = StatsService::dateTimeToMilliseconds($element->getLogTimestamp()) - $firstTimeStamp;
			$tpsSum += $element->getThreat();
			if ($element->getEffectName() != null && $element->getEffectName()->getStringId() == $dmgStringId
					&& ($sourceName != "" && $element->getSource() == $sourceName)) {

				if ($element->getValueType() != null && $element->getValueType()->getStringValue() != "") {
					$valueType = $element->getValueType()->getStringValue();
				}
				else if ($element->getValueEffectName() != null && $element->getValueEffectName()->getStringValue() != "") {
					$valueType = $element->getValueEffectName()->getStringValue();
				} else {
					$valueType = "Unknown";
				}
				$computedStats["DamageOverTime"][$valueType][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $element->getValue();

				$dpsSum += $element->getValue();
			} else if ($element->getEffectName() != null && $element->getEffectName()->getStringId() == $healStringId
					&& ($sourceName != "" && $element->getSource() == $sourceName)) {
				$computedStats["HealOverTime"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $element->getValue();

				$hpsSum += $element->getValue();
			}
			if ($mSecsElapsed > 0)
			{
				$computedStats["DpsOverTime"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $dpsSum/(max($mSecsElapsed, 1000)/1000);
				$computedStats["HpsOverTime"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $hpsSum/(max($mSecsElapsed, 1000)/1000);
				$computedStats["TpsOverTime"][StatsService::dateTimeToMilliseconds($element->getLogTimestamp())] = $tpsSum/(max($mSecsElapsed, 1000)/1000);
			}

			// Timestamps (starting and ending times)
			if (isset($computedStats["startTime"]) && $computedStats["startTime"] != null) {
				$computedStats["startTime"] = min(
						$element->getLogTimestamp(),
						$computedStats["startTime"]);
			} else {
				$computedStats["startTime"] = $element
				->getLogTimestamp();
			}
			if (isset($computedStats["endTime"]) && $computedStats["endTime"] != null) {
				$computedStats["endTime"] = max(
						$element->getLogTimestamp(),
						$computedStats["endTime"]);
			} else {
				$computedStats["endTime"] = $element
				->getLogTimestamp();
			}

			// Threat
			if (isset($computedStats["Threat"])) {
				$computedStats["Threat"]["total"] += $element
				->getThreat();
			} else {
				$computedStats["Threat"]["total"] = $element
				->getThreat();
			}

			// We determine if the action was done by the user, received,
			// or as well done as received (heals for instance)
			$dest = "";
			if ($element->getSource() == $charName) {
				$dest = "Done";
			}
			if ($element->getTarget() == $charName) {
				$dest .= "Received";
			}
			if ($dest == "") {
				$dest = "Companion";
			}

			// Threat per skill + avg / skill
			if (($tAndC = $element->getThreatAndCause()) != null)
			{
				// If the user has done this action we fill in the array
				if (strpos($dest, "Done") !== false) {
					// We check if the index exists to avoid a PHP notice
					if (isset($computedStats["Threat"]["PerSkill"][$tAndC["cause"]])) {
						$computedStats["Threat"]["PerSkill"][$tAndC["cause"]] += $tAndC["value"];
					} else {
						$computedStats["Threat"]["PerSkill"][$tAndC["cause"]] = $tAndC["value"];
					}
					// Average per capacity
					if (isset($computedStats["Threat"]["AvgPerSkill"][$tAndC["cause"]])) {
						$computedStats["Threat"]["AvgPerSkill"][$tAndC["cause"]]["total"] += $tAndC["value"];
						$computedStats["Threat"]["AvgPerSkill"][$tAndC["cause"]]["count"]++;
					} else {
						$computedStats["Threat"]["AvgPerSkill"][$tAndC["cause"]]["total"] = $tAndC["value"];
						$computedStats["Threat"]["AvgPerSkill"][$tAndC["cause"]]["count"] = 1;
					}
				}
			}


			// We only compute for damages or heal
			if ($element->getEffectName() == null || ($element->getEffectName()->getStringId() != $healStringId
					&& $element->getEffectName()->getStringId() != $dmgStringId))
				continue;

			// We have 2 cases : either the ID is set, or it isn't
			if (isset($computedStats[$element->getEffectName()->getStringId()][$dest])) {
				// Max value
				if ($element->getValue()
						> $computedStats[$element
						->getEffectName()->getStringId()][$dest]["max"]
						->getValue())
					$computedStats[$element->getEffectName()->getStringId()][$dest]["max"] = $element;
				// Total
				$computedStats[$element->getEffectName()->getStringId()][$dest]["total"] += $element
				->getValue();
				// Number of abilities
				$computedStats[$element->getEffectName()->getStringId()][$dest]["count"]++;

				// If we have a critic
				if ($element->getValueCrit()) {
					// Same as before, we check if the id is set.
					if (isset($computedStats[$element
							->getEffectName()->getStringId()][$dest]["crit_count"])
							&& isset($computedStats[$element
									->getEffectName()->getStringId()][$dest]["crit_value"])) {
						// Crit count
						$computedStats[$element
						->getEffectName()->getStringId()][$dest]["crit_count"]++;
						// Crit value
						$computedStats[$element
						->getEffectName()->getStringId()][$dest]["crit_value"] += $element
						->getValue();
					} else {
						// Crit count
						$computedStats[$element
						->getEffectName()->getStringId()][$dest]["crit_count"] = 1;
						// Crit value
						$computedStats[$element
						->getEffectName()->getStringId()][$dest]["crit_value"] = $element
						->getValue();
					}
				}
				// Absorbed damages
				$computedStats[$element->getEffectName()->getStringId()][$dest]["absorbed"] += $element
				->getValueAbsorbed();
			} else {
				// Max
				$computedStats[$element->getEffectName()->getStringId()][$dest]["max"] = $element;
				// Total
				$computedStats[$element->getEffectName()->getStringId()][$dest]["total"] = $element
				->getValue();
				// Count
				$computedStats[$element->getEffectName()->getStringId()][$dest]["count"] = 1;
				// Critic
				if ($element->getValueCrit()) {
					$computedStats[$element->getEffectName()->getStringId()][$dest]["crit_count"] = 1;
					$computedStats[$element->getEffectName()->getStringId()][$dest]["crit_value"] = $element
					->getValue();
				} else {
					$computedStats[$element->getEffectName()->getStringId()][$dest]["crit_count"] = 0;
					$computedStats[$element->getEffectName()->getStringId()][$dest]["crit_value"] = 0;
				}
				// Absorbed value
				$computedStats[$element->getEffectName()->getStringId()][$dest]["absorbed"] = (int) (0
						+ $element->getValueAbsorbed());
			}

			// heals per source / target
			if ($element->getEffectName()->getStringId() == $healStringId) {
				if (strpos($dest, "Done") !== false) {
					if (!isset($computedStats["HealPerTarget"][$element->getTarget()->getStringValue()]))
						$computedStats["HealPerTarget"][$element->getTarget()->getStringValue()] = 0;
					$computedStats["HealPerTarget"][$element->getTarget()->getStringValue()] += $element->getValue();
				}
				if (strpos($dest, "Received") !== false) {
					if (!isset($computedStats["HealPerSource"][$element->getSource()->getStringValue()]))
						$computedStats["HealPerSource"][$element->getSource()->getStringValue()] = 0;
					$computedStats["HealPerSource"][$element->getSource()->getStringValue()] += $element->getValue();
				}
			}
			// Per capacity
			// If the user has done this action we fill in the array
			if (strpos($dest, "Done") !== false) {
				// We check if the index exists to avoid a PHP notice
				if (isset($computedStats[$element
						->getEffectName()->getStringId()]["PerSkill"][$element
						->getAbilityName()->getStringValue()])) {

					$computedStats[$element->getEffectName()->getStringId()]["PerSkill"][$element
					->getAbilityName()->getStringValue()] += $element->getValue();
				} else {
					$computedStats[$element->getEffectName()->getStringId()]["PerSkill"][$element
					->getAbilityName()->getStringValue()] = $element->getValue();
				}
				// Average per capacity
				if (isset($computedStats[$element->getEffectName()->getStringId()]["AvgPerSkill"][$element->getAbilityName()->getStringValue()])) {
					$computedStats[$element->getEffectName()->getStringId()]["AvgPerSkill"][$element->getAbilityName()->getStringValue()]["total"] += $element->getValue();
					$computedStats[$element->getEffectName()->getStringId()]["AvgPerSkill"][$element->getAbilityName()->getStringValue()]["count"]++;
				} else {
					$computedStats[$element->getEffectName()->getStringId()]["AvgPerSkill"][$element->getAbilityName()->getStringValue()]["total"] = $element->getValue();
					$computedStats[$element->getEffectName()->getStringId()]["AvgPerSkill"][$element->getAbilityName()->getStringValue()]["count"] = 1;
				}

			}
		} // End of loop

		$statsableObject->cleanMemory();

		if (($this->dateTimeToMilliseconds($computedStats["endTime"])
										- $this->dateTimeToMilliseconds($computedStats["startTime"])) <0)
			throw new \Exception("Fight object too old to be processed"); // Corrupted Stats

		// Fixing the array... (basically transforming max to string & adding "DoneReceived" to done and received
		foreach ($computedStats as $effectName => $statEffect) {
			if ($effectName != $healStringId && $effectName != $dmgStringId && $effectName != "Threat")
				continue;
			if (isset($computedStats[$effectName]["AvgPerSkill"]))
			{
				$computedAvg = array();
				foreach ($computedStats[$effectName]["AvgPerSkill"] as $skill => $toCompute)
				{
					$computedAvg[$skill] = $toCompute["total"]/$toCompute["count"];
				}
				unset($computedStats[$effectName]["AvgPerSkill"]);
				$computedStats[$effectName]["AvgPerSkill"] = $computedAvg;
			}
			if ($effectName != $healStringId && $effectName != $dmgStringId)
				continue;
			if (isset($computedStats[$effectName]["Companion"]))
				unset($computedStats[$effectName]["Companion"]); // Not managing the companions for now
			if (isset($computedStats[$effectName][""]) && !is_array($computedStats[$effectName][""])) {
				$computedStats[$effectName]["Unspecified"] = $computedStats[$effectName][""];
				unset($computedStats[$effectName][""]);
			}
			if (isset($computedStats[$effectName]["Done"]["max"])
					&& isset(
							$computedStats[$effectName]["DoneReceived"]["max"])
					&& $computedStats[$effectName]["Done"]["max"]
					->getValue()
					< $computedStats[$effectName]["DoneReceived"]["max"]
					->getValue())
				$computedStats[$effectName]["Done"]["max"] = $computedStats[$effectName]["DoneReceived"]["max"];
			if (isset($computedStats[$effectName]["Received"]["max"])
					&& isset($computedStats[$effectName]["DoneReceived"]["max"])
					&& $computedStats[$effectName]["Received"]["max"]->getValue() < $computedStats[$effectName]["DoneReceived"]["max"]->getValue())
				$computedStats[$effectName]["Received"]["max"] = $computedStats[$effectName]["DoneReceived"]["max"];
			if (isset($computedStats[$effectName]["Done"])
					&& isset(
							$computedStats[$effectName]["DoneReceived"])) {
				$computedStats[$effectName]["Done"]["total"] += $computedStats[$effectName]["DoneReceived"]["total"];
				$computedStats[$effectName]["Done"]["crit_count"] += $computedStats[$effectName]["DoneReceived"]["crit_count"];
				$computedStats[$effectName]["Done"]["count"] += $computedStats[$effectName]["DoneReceived"]["count"];
				$computedStats[$effectName]["Done"]["crit_value"] += $computedStats[$effectName]["DoneReceived"]["crit_value"];
				$computedStats[$effectName]["Done"]["absorbed"] += $computedStats[$effectName]["DoneReceived"]["absorbed"];
			} else if (!isset($computedStats[$effectName]["Done"])
					&& isset(
							$computedStats[$effectName]["DoneReceived"])) {
				$computedStats[$effectName]["Done"] = $computedStats[$effectName]["DoneReceived"];
			}
			if (isset($computedStats[$effectName]["Received"])
					&& isset($computedStats[$effectName]["DoneReceived"])) {
				$computedStats[$effectName]["Received"]["total"] += $computedStats[$effectName]["DoneReceived"]["total"];
				$computedStats[$effectName]["Received"]["crit_count"] += $computedStats[$effectName]["DoneReceived"]["crit_count"];
				$computedStats[$effectName]["Received"]["count"] += $computedStats[$effectName]["DoneReceived"]["count"];
				$computedStats[$effectName]["Received"]["crit_value"] += $computedStats[$effectName]["DoneReceived"]["crit_value"];
				$computedStats[$effectName]["Received"]["absorbed"] += $computedStats[$effectName]["DoneReceived"]["absorbed"];
			} else if (!isset($computedStats[$effectName]["Received"])
					&& isset(
							$computedStats[$effectName]["DoneReceived"])) {
				$computedStats[$effectName]["Received"] = $computedStats[$effectName]["DoneReceived"];
			}
			if (isset($computedStats[$effectName]["Done"]["max"]))
				$computedStats[$effectName]["Done"]["max"] = $computedStats[$effectName]["Done"]["max"]
				->readable();
			if (isset($computedStats[$effectName]["Received"]["max"]))
				$computedStats[$effectName]["Received"]["max"] = $computedStats[$effectName]["Received"]["max"]
				->readable();

			// set values to default if not set
			if (!isset($computedStats[$effectName]["Done"]["crit_count"]))
				$computedStats[$effectName]["Done"]["crit_count"] = 0;
			if (!isset($computedStats[$effectName]["Received"]["crit_count"]))
				$computedStats[$effectName]["Received"]["crit_count"] = 0;
			if (!isset($computedStats[$effectName]["Done"]["crit_value"]))
				$computedStats[$effectName]["Done"]["crit_value"] = 0;
			if (!isset($computedStats[$effectName]["Received"]["crit_value"]))
				$computedStats[$effectName]["Received"]["crit_value"] = 0;
			if (!isset($computedStats[$effectName]["Done"]["count"]) || $computedStats[$effectName]["Done"]["count"] == 0)
				$computedStats[$effectName]["Done"]["count"] = 1;
			if (!isset($computedStats[$effectName]["Received"]["count"]) || $computedStats[$effectName]["Received"]["count"] == 0)
				$computedStats[$effectName]["Received"]["count"] = 1;
			if (!isset($computedStats[$effectName]["Done"]["total"]) || $computedStats[$effectName]["Done"]["total"] == 0)
				$computedStats[$effectName]["Done"]["total"] = 1;
			if (!isset($computedStats[$effectName]["Received"]["total"]) || $computedStats[$effectName]["Received"]["total"] == 0)
				$computedStats[$effectName]["Received"]["total"] = 1;
			if (!isset($computedStats[$effectName]["Done"]["max"]))
				$computedStats[$effectName]["Done"]["max"] = 0;
			if (!isset($computedStats[$effectName]["Received"]["max"]))
				$computedStats[$effectName]["Received"]["max"] = 0;
			if (!isset($computedStats[$effectName]["Done"]["absrobed"]))
				$computedStats[$effectName]["Done"]["absorbed"] = 0;
			if (!isset($computedStats[$effectName]["Received"]["absorbed"]))
				$computedStats[$effectName]["Received"]["absorbed"] = 0;


			unset($computedStats[$effectName]["DoneReceived"]);
		}

		$computedStats["msDuration"] = $this->dateTimeToMilliseconds($computedStats["endTime"])
										- $this->dateTimeToMilliseconds($computedStats["startTime"]);



		// We have the computed stats, now we create the object
		$stats = new Stats();
		$stats->setReferencingObject($statsableObject);

		$stats->fillInWithArray($computedStats);
		$statsableObject->addStats($stats);
		$this->getEntityManager()->persist($stats);

		$this->updateParentStats($statsableObject);

		return $stats;

	}
}
