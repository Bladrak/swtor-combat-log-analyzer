<?php

namespace LogAnalyzer\Bundle\StatsBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats;

use LogAnalyzer\Bundle\UserBundle\Entity\Guild;

use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;

use LogAnalyzer\Bundle\UserBundle\Entity\User;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\StatsBundle\Entity\Stats;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;


/**
 * Stats controller.
 *
 * @Route("/stats")
 */
class StatsController extends Controller
{
    /**
     * Finds and displays a Stats entity for a given user.
     *
     * @Route("/{id}/showuser", name="stats_show_user")
     * @Template()
     */
    public function showUserAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$user = $em->getRepository('LogAnalyzerUserBundle:User')->find($id);

        if (!$user) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find User entity.'));
    	}

    	$entity = $this->get('log_analyzer_stats.stats_service')->getStatsFor($user);

    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Stats entity.'));
    	}

//     	$breadcrumbs = $this->get("white_october_breadcrumbs");
//     	$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//     	$breadcrumbs->addItem($this->get('translator')->trans("Stats"), $this->get("router")->generate("stats_show_user", array("id" => $id)));

    	$response = $this->render("LogAnalyzerStatsBundle:Stats:showUser.html.twig", array(
    			'entity'      => $entity,
    	));

    	$response->setPublic();
    	$response->setMaxAge(600);

    	return $response;
    }

    /**
     * Finds and displays a Stats entity for a given combatLog.
     *
     * @Route("/{id}/showcl", name="stats_show_combatlog")
     * @Template("LogAnalyzerStatsBundle:Stats:showCombatlog.html.twig")
     */
    public function showClFromIdAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$combatLog = $em->getRepository("LogAnalyzerCombatLogBundle:CombatLog")->find($id);
    	if (!$combatLog) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find CombatLog entity.'));
    	}
    	return $this->showCombatlogAction($combatLog);
    }


    /**
     * Shows sample statistics
     *
     * @Route("/show_sample", name="stats_show_sample")
     * @Template("LogAnalyzerStatsBundle:Stats:showCombatlog.html.twig")
     */
    public function showSampleAction()
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$combatLog = $em->getRepository("LogAnalyzerCombatLogBundle:CombatLog")->getSample();
    	return $this->showCombatlogAction($combatLog);
    }

    /**
     * Finds and displays a Stats entity for a given combatLog.
     *
     * @Template("LogAnalyzerStatsBundle:Stats:showCombatlog.html.twig")
     */
    public function showCombatlogAction(CombatLog $combatLog)
    {
    	$logger = $this->get('logger');
    	$logger->info('Entering showCombatLogAction');

    	if (!$combatLog) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find CombatLog entity.'));
    	}
    	$logger->info('Getting stats');

    	$entity = $this->get('log_analyzer_stats.stats_service')->getStatsFor($combatLog);

    	$em = $this->getDoctrine()->getEntityManager();

    	$fights = $combatLog->getFights();

    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Stats entity.'));
    	}

//     	$breadcrumbs = $this->get("white_october_breadcrumbs");
//     	$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//     	$breadcrumbs->addItem($combatLog->getTorCharacter()->getName(), $this->get("router")->generate("character_show", array("id" => $combatLog->getTorCharacter()->getId())));
//     	$breadcrumbs->addItem($this->get('translator')->trans("Stats"), $this->get("router")->generate("stats_show_combatlog", array("id" => $combatLog->getId())));

    	return array(
    			'entity'      => $entity,
    			'fights'		=> $fights,
    	);
    }

    /**
     * Finds and displays a Stats entity for a given Guild.
     *
     * @Route("/{id}/showguild_render", name="stats_show_guild_render")
     * @Template()
     */
    public function showGuildRenderAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$guild = $em->getRepository('LogAnalyzerUserBundle:Guild')->find($id);

    	if (!$guild) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Guild entity.'));
    	}

    	$entity = $this->get('log_analyzer_stats.stats_service')->getStatsFor($guild);

    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Stats entity.'));
    	}

    	return array(
    			'entity'      => $entity,
    	);
    }

    /**
     * Finds and displays a Stats entity for a given group.
     *
     * @Route("/{id}/showgroup", name="stats_show_group")
     * @Template()
     */
    public function showGroupAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$group = $em->getRepository('LogAnalyzerStatsBundle:GroupStats')->find($id);

    	if ($group->getAggregatedStats() == null || $group->getAggregatedStats()->getToUpdate() == true)
    	{
    		$group->setAggregatedStats($this->get('log_analyzer_stats.stats_service')->getStatsFor($group));
    	}

    	if (!$group) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Group entity.'));
    	}
//     	$breadcrumbs = $this->get("white_october_breadcrumbs");
//     	$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//     	$breadcrumbs->addItem($this->get('translator')->trans("Group Stats"), $this->get("router")->generate("stats_show_group", array("id" => $id)));

    	return array(
    			'entity'      => $group,
    	);
    }

    /**
     * Finds and displays a Stats entity for a given Fight.
     *
     * @Route("/{id}/showfgt", name="stats_show_fight", options={"expose"=true})
     * @Template()
     */
    public function showFightAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$fight= $em->getRepository('LogAnalyzerCombatLogBundle:Fight')->find($id);

    	if (!$fight) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Fight entity.'));
    	}

    	$entity = $this->get('log_analyzer_stats.stats_service')->getStatsFor($fight);

    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Stats entity.'));
    	}

    	$fights = $fight->getCombatLog()->getFights();

//     	$breadcrumbs = $this->get("white_october_breadcrumbs");
//     	$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//     	$breadcrumbs->addItem($fight->getCombatLog()->getTorCharacter()->getName(), $this->get("router")->generate("character_show", array("id" => $fight->getCombatLog()->getTorCharacter()->getId())));
//     	$breadcrumbs->addItem($this->get('translator')->trans("CombatLog"), $this->get("router")->generate("stats_show_combatlog", array("id" => $fight->getCombatLog()->getId())));
//     	$breadcrumbs->addItem($this->get('translator')->trans("Stats"), $this->get("router")->generate("stats_show_fight", array("id" => $id)));

    	return array(
    			'entity'	=> $entity,
    			'fights'	=> $fights,
    			'currentFightId' => $id,
    	);
    }

    /**
     * Finds and displays a Stats entity for a given torCharacter.
     *
     * @Route("/{id}/showchar", name="stats_show_character")
     * @Template()
     */
    public function showCharacterAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$torCharacter = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($id);

    	if (!$torCharacter) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find TorCharacter entity.'));
    	}

    	$entity = $this->get('log_analyzer_stats.stats_service')->getStatsFor($torCharacter);

    	if (!$entity) {
    		throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Stats entity.'));
    	}

//     	$breadcrumbs = $this->get("white_october_breadcrumbs");
//     	$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//     	$breadcrumbs->addItem($torCharacter->getName()."'s Stats", $this->get("router")->generate("stats_show_character", array("id" => $id)));

    	return array(
    			'entity'      => $entity,
    	);
    }

    private function getTypeForStatsable($entity)
    {
    	if ($entity instanceof CombatLog) {
    		return "combatlog";
    	} else if ($entity instanceof Fight) {
	    	return "fight";
    	} else if ($entity instanceof User) {
    		return "user";
    	} else if ($entity instanceof TorCharacter) {
    		return "character";
    	} else if ($entity instanceof Guild) {
	    	return "guild";
    	} else if ($entity instanceof GroupStats) {
	    	return "group";
    	} else {
    		throw new \Exception("Unknown stats referencing type");
    	}
    }

    /**
     * Display permalink section for stats page
     */
    public function renderPermalinkAction($entity)
    {
    	return $this
			    	->render(
			    			'LogAnalyzerStatsBundle:Stats:renderPermalink.html.twig',
			    			array('type' => $this->getTypeForStatsable($entity),
			    					'id' => $entity->getId()
			    				)
			    			);
    }

    /**
     * Display global stats for entity
     */
    public function renderGlobalStatsAction($entity, $stats)
    {
    	$fight = null;
    	if ($this->getTypeForStatsable($entity) == "fight")
    		$fight = $entity;
    	return $this
			    	->render(
			    			'LogAnalyzerStatsBundle:Stats:renderGlobalStats.html.twig',
			    			array('fight' => $fight,
			    					'stats' => $stats
			    				)
			    			);
    }

    /**
     * Display damages stats for entity
     */
    public function renderDamageStatsAction($entity, $stats)
    {
    	return $this
			    	->render(
			    			'LogAnalyzerStatsBundle:Stats:renderDamageStats.html.twig',
			    			array('entity' => $this->getTypeForStatsable($entity),
			    					'stats' => $stats
			    				)
			    			);
    }

    /**
     * Display heal stats for entity
     */
    public function renderHealStatsAction($entity, $stats)
    {
    	return $this
			    	->render(
			    			'LogAnalyzerStatsBundle:Stats:renderHealStats.html.twig',
			    			array('entity' => $this->getTypeForStatsable($entity),
			    					'stats' => $stats
			    				)
			    			);
    }

    /**
     * Display threat stats for entity
     */
    public function renderThreatStatsAction($entity, $stats)
    {
    	return $this
			    	->render(
			    			'LogAnalyzerStatsBundle:Stats:renderThreatStats.html.twig',
			    			array('entity' => $this->getTypeForStatsable($entity),
			    					'stats' => $stats
			    				)
			    			);
    }

    /**
     * Returns ChartData for stats id
     *
     * @Route("/{id}/{name}/chartdata", name="stats_chartdata")
     */
    public function chartDataAction($id, $name)
    {
    	return $this->chartDataKeysAction($id, $name, false);
    }

    /**
     * Returns ChartData per player for groupstats id
     *
     * @Route("/{id}/{name}/chartdataplayer", name="stats_chartdataplayer")
     */
    public function chartDataPlayerAction($id, $name)
    {
    	$response = new Response($this->getDoctrine()->getEntityManager()->getRepository("LogAnalyzerStatsBundle:Stats")->chartDataPlayer($id, $name));
    	$response->headers->set('Content-Type', 'text/plain');
    	return $response;
    }

    /**
     * Returns RankingData per player for groupstats id
     *
     * @Route("/{id}/{name}/rankingplayer", name="stats_rankingplayer")
     */
    public function rankingPlayerAction($id, $name)
    {
    	$response = new Response($this->getDoctrine()->getEntityManager()->getRepository("LogAnalyzerStatsBundle:Stats")->rankingPlayer($id, $name));
    	$response->headers->set('Content-Type', 'text/plain');
    	return $response;
    }

    /**
     * Returns ChartData for stats id with numericKeys
     *
     * @Route("/{id}/{name}/{numericKeys}/chartdata", name="stats_chartdatakeys")
     */
    public function chartDataKeysAction($id, $name, $numericKeys)
    {
    	$response = new Response($this->getDoctrine()->getEntityManager()
    								->getRepository("LogAnalyzerStatsBundle:Stats")->chartData($id, $name, $numericKeys));
    	$response->headers->set('Content-Type', 'text/plain');
    	return $response;
    }
}
