<?php

namespace LogAnalyzer\Bundle\StatsBundle\Entity;

use LogAnalyzer\Bundle\UserBundle\Entity\Guild;

use Doctrine\Common\Collections\Collection;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats
 *
 * @ORM\Table(indexes={@ORM\index(name="join_stats", columns={"aggregatedstats_id"}),
 * 						@ORM\index(name="join_guild", columns={"guild_id"})})
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\StatsBundle\Entity\GroupStatsRepository")
 */
class GroupStats
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="The title must not be empty.")
     */
    private $title;

    /**
     * @var datetime $fromDate
     *
     * @ORM\Column(name="fromDate", type="datetime")
     * @Assert\NotBlank(message="The begin date must not be empty.")
     * @Assert\DateTime(message="The begin date must have the correct format.")
     */
    private $fromDate;

    /**
     * @var datetime $toDate
     *
     * @ORM\Column(name="toDate", type="datetime")
     * @Assert\NotBlank(message="The end date must not be empty.")
     * @Assert\DateTime(message="The end date must have the correct format.")
     */
    private $toDate;

    /**
     * @var ArrayCollection $members
     *
     * @ORM\ManyToMany(targetEntity="LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter", inversedBy="groups")
     * @ORM\JoinTable(name="chars_groups")
     */
    private $members;

    /**
     * @var Stats $aggregatedStats
     *
     * @ORM\OneToOne(targetEntity="Stats")
     * @ORM\JoinColumn(name="aggregatedstats_id", referencedColumnName="id")
     */
    private $aggregatedStats;

    /**
     * @var ArrayCollection $stats
     *
     * @ORM\ManyToMany(targetEntity="Stats", inversedBy="groups")
     * @ORM\JoinTable(name="group_join_stats")
     */
    private $stats;

    /**
     * @var Guild $guild
     *
     * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\UserBundle\Entity\Guild", inversedBy="groupStats")
     * @ORM\JoinColumn(name="guild_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $guild;

    public function __construct()
    {
    	$this->members = new ArrayCollection();
    	$this->stats = new ArrayCollection();
    }

    public static function getStatsFieldsToAggregate()
    {
    	$keysToAggregate = array("healCritRatioValue",
    			"dmgDoneCritRatioValue", "dmgTakenCritRatioValue",
    			"healCritRatioCapacity", "dmgDoneCritRatioCapacity",
    			"totalHealReceived", "totalHealDone", "totalDamageDone",
    			"totalThreat", "totalAbsorbedDamages", "totalInterrupts",
    			"totalDispells", "dmgTakenCritRatioCapacity", "beginTime",
    			"endTime");
    	$groupKeys = array("maxHealDone", "maxHealReceived",
    			"maxDamageDealt", "maxDamageTaken", "deaths");
    	$keysToAggregate = array_merge($keysToAggregate, $groupKeys);
    	return $keysToAggregate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fromDate
     *
     * @param datetime $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * Get fromDate
     *
     * @return datetime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param datetime $toDate
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;
    }

    /**
     * Get toDate
     *
     * @return datetime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Add members
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $members
     */
    public function addTorCharacter(\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $members)
    {
        $this->members[] = $members;
    }

    /**
     * Get members
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Ser members
     *
     * @param Doctrine\Common\Collections\Collection
     */
    public function setMembers(Collection $members)
    {
    	$this->members = $members;
    }

    /**
     * Set aggregatedStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $aggregatedStats
     */
    public function setAggregatedStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $aggregatedStats)
    {
        $this->aggregatedStats = $aggregatedStats;
    }

    /**
     * Get aggregatedStats
     *
     * @return LogAnalyzer\Bundle\StatsBundle\Entity\Stats
     */
    public function getAggregatedStats()
    {
        return $this->aggregatedStats;
    }

    /**
     * Add stats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $stats
     */
    public function addStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $stats)
    {
        $this->stats[] = $stats;
    }

    /**
     * Get stats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * Set stats
     *
     * @param Doctrine\Common\Collections\ArrayCollection
     */
    public function setStats(ArrayCollection $stats)
    {
		$this->stats = $stats;
    }

    /**
     * Set guild
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\Guild $guild
     */
    public function setGuild(\LogAnalyzer\Bundle\UserBundle\Entity\Guild $guild)
    {
        $this->guild = $guild;
    }

    /**
     * Get guild
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }
}