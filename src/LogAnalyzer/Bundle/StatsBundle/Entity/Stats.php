<?php

namespace LogAnalyzer\Bundle\StatsBundle\Entity;
use LogAnalyzer\Bundle\CombatLogBundle\Service\TranslationMappingService;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight;

use Doctrine\Common\Collections\ArrayCollection;

use LogAnalyzer\Bundle\UserBundle\Entity\Guild;

use Entities\User;

use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAnalyzer\Bundle\StatsBundle\Entity\Stats
 *
 * @ORM\Table(indexes={@ORM\index(name="sort_overallDps", columns={"overall_dps"}),
 * 						@ORM\index(name="sort_overallHps", columns={"overall_hps"}),
 * 						@ORM\index(name="sort_overallTps", columns={"overall_tps"}),
 * 						@ORM\index(name="sort_totalDamageDone", columns={"total_damage_done"}),
 * 						@ORM\index(name="sort_totalHealDone", columns={"total_heal_done"}),
 * 						@ORM\index(name="sort_totalThreat", columns={"total_threat"}),
 * 						@ORM\index(name="sort_dmgDoneCritRatioCapacity", columns={"dmg_done_crit_ratio_capacity"}),
 * 						@ORM\index(name="sort_healCritRatioCapacity", columns={"heal_crit_ratio_capacity"}),
 * 						@ORM\index(name="sort_deathsCount", columns={"deathsCount"}),
 * 						@ORM\index(name="join_combatlog", columns={"cl_id"}),
 * 						@ORM\index(name="join_fight", columns={"fight_id"}),
 * 						@ORM\index(name="join_character", columns={"tor_character_id"}),
 * 						@ORM\index(name="join_user", columns={"user_id"}),
 * 						@ORM\index(name="join_guild", columns={"guild_id"})})
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\StatsBundle\Entity\StatsRepository")
 */
class Stats {
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var boolean $toUpdate
	 *
	 * @ORM\Column(name="toUpdate", type="boolean", nullable=true)
	 */
	private $toUpdate = false;

	/**
	 * @var double $overallHps
	 *
	 * @ORM\Column(name="overall_hps", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $overallHps = 0.0;

	/**
	 * @var double $overallDps
	 *
	 * @ORM\Column(name="overall_dps", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $overallDps = 0.0;

	/**
	 * @var double $overallTps
	 *
	 * @ORM\Column(name="overall_Tps", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $overallTps = 0.0;

	/**
	 * @var string $maxHealDone
	 *
	 * @ORM\Column(name="max_heal_done", type="string", nullable=true)
	 */
	private $maxHealDone = null;

	/**
	 * @var string $maxHealReceived
	 *
	 * @ORM\Column(name="max_heal_received", type="string", nullable=true)
	 */
	private $maxHealReceived = null;

	/**
	 * @var string $maxDamageDealt
	 *
	 * @ORM\Column(name="max_damage_dealt", type="string", nullable=true)
	 */
	private $maxDamageDealt = null;

	/**
	 * @var string $maxDamageTaken
	 *
	 * @ORM\Column(name="max_damage_taken", type="string", nullable=true)
	 */
	private $maxDamageTaken = null;

	/**
	 * @var integer $totalHealDone
	 *
	 * @ORM\Column(name="total_heal_done", type="integer", nullable=true)
	 */
	private $totalHealDone = 0;

	/**
	 * @var integer $totalHealReceived
	 *
	 * @ORM\Column(name="total_heal_received", type="integer", nullable=true)
	 */
	private $totalHealReceived = 0;

	/**
	 * @var integer $totalDamageDone
	 *
	 * @ORM\Column(name="total_damage_done", type="integer", nullable=true)
	 */
	private $totalDamageDone = 0;

	/**
	 * @var integer $totalDamageTaken
	 *
	 * @ORM\Column(name="total_damage_taken", type="integer", nullable=true)
	 */
	private $totalDamageTaken = 0;

	/**
	 * @var integer $totalThreat
	 *
	 * @ORM\Column(name="total_threat", type="integer", nullable=true)
	 */
	private $totalThreat = 0;

	/**
	 * @var integer $totalAbsorbedDamages
	 *
	 * @ORM\Column(name="total_abs_damages", type="integer", nullable=true)
	 */
	private $totalAbsorbedDamages = 0;

	/**
	 * @var integer $totalInterrupts
	 *
	 * @ORM\Column(name="total_interrupts", type="integer", nullable=true)
	 */
	private $totalInterrupts = 0;

	/**
	 * @var integer $totalDispells
	 *
	 * @ORM\Column(name="total_dispells", type="integer", nullable=true)
	 */
	private $totalDispells = 0;

	/**
	 * @var array $healPerCapacity
	 *
	 * @ORM\Column(name="heal_per_capacity", type="array", nullable=true)
	 */
	private $healPerCapacity = array();

	/**
	 * @var array $healAvgPerCapacity
	 *
	 * @ORM\Column(name="heal_avg_per_capacity", type="array", nullable=true)
	 */
	private $healAvgPerCapacity = array();

	/**
	 * @var array $healPerTarget
	 *
	 * @ORM\Column(name="heal_per_target", type="array", nullable=true)
	 */
	private $healPerTarget = array();

	/**
	 * @var array $healPerSource
	 *
	 * @ORM\Column(name="heal_per_source", type="array", nullable=true)
	 */
	private $healPerSource = array();

	/**
	 * @var array $threatPerCapacity
	 *
	 * @ORM\Column(name="threat_per_capacity", type="array", nullable=true)
	 */
	private $threatPerCapacity = array();

	/**
	 * @var array $threatAvgPerCapacity
	 *
	 * @ORM\Column(name="threat_avg_per_capacity", type="array", nullable=true)
	 */
	private $threatAvgPerCapacity = array();

	/**
	 * @var array $damagePerCapacity
	 *
	 * @ORM\Column(name="damage_per_capacity", type="array", nullable=true)
	 */
	private $damagePerCapacity = array();

	/**
	 * @var array $damageAvgPerCapacity
	 *
	 * @ORM\Column(name="damage_avg_per_capacity", type="array", nullable=true)
	 */
	private $damageAvgPerCapacity = array();

	/**
	 * @var array $damageOverTime
	 *
	 * @ORM\Column(name="damage_over_time", type="array", nullable=true)
	 */
	private $damageOverTime = array();

	/**
	 * @var array $healOverTime
	 *
	 * @ORM\Column(name="heal_over_time", type="array", nullable=true)
	 */
	private $healOverTime = array();

	/**
	 * @var array $dpsOverTime
	 *
	 * @ORM\Column(name="dps_over_time", type="array", nullable=true)
	 */
	private $dpsOverTime = array();

	/**
	 * @var array $hpsOverTime
	 *
	 * @ORM\Column(name="hps_over_time", type="array", nullable=true)
	 */
	private $hpsOverTime = array();

	/**
	 * @var array $tpsOverTime
	 *
	 * @ORM\Column(name="tps_over_time", type="array", nullable=true)
	 */
	private $tpsOverTime = array();

	/**
	 * @var double $healCritRatioValue
	 *
	 * @ORM\Column(name="heal_crit_ratio_value", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $healCritRatioValue = 0.0;

	/**
	 * @var double $dmgDoneCritRatioValue
	 *
	 * @ORM\Column(name="dmg_done_crit_ratio_value", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $dmgDoneCritRatioValue = 0.0;

	/**
	 * @var double $dmgTakenCritRatioValue
	 *
	 * @ORM\Column(name="dmg_taken_crit_ratio_value", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $dmgTakenCritRatioValue = 0.0;

	/**
	 * @var double $healCritRatioCapacity
	 *
	 * @ORM\Column(name="heal_crit_ratio_capacity", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $healCritRatioCapacity = 0.0;

	/**
	 * @var double $dmgDoneCritRatioCapacity
	 *
	 * @ORM\Column(name="dmg_done_crit_ratio_capacity", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $dmgDoneCritRatioCapacity = 0.0;

	/**
	 * @var double $dmgTakenCritRatioCapacity
	 *
	 * @ORM\Column(name="dmg_taken_crit_ratio_capacity", type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $dmgTakenCritRatioCapacity = 0.0;

	/**
	 * @var ArrayCollection $groups
	 *
	 * @ORM\ManyToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats", mappedBy="stats")
	 */
	private $groups;

	/**
	 * @var CombatLog $referencingCombatLog
	 *
	 * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog", inversedBy="logStats")
	 * @ORM\JoinColumn(name="cl_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 */
	private $referencingCombatLog;

	/**
	 * @var Fight $referencingFight
	 *
	 * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight", inversedBy="logStats")
	 * @ORM\JoinColumn(name="fight_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 */
	private $referencingFight;

	/**
	 * @var TorCharacter $referencingTorCharacter
	 *
	 * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter", inversedBy="logStats")
	 * @ORM\JoinColumn(name="tor_character_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 */
	private $referencingTorCharacter;

	/**
	 * @var User $referencingUser
	 *
	 * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\UserBundle\Entity\User", inversedBy="logStats")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 */
	private $referencingUser;

	/**
	 * @var Guild $referencingGuild
	 *
	 * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\UserBundle\Entity\Guild", inversedBy="logStats")
	 * @ORM\JoinColumn(name="guild_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 */
	private $referencingGuild;

	/**
	 * @var datetime $beginTime
	 *
	 * @ORM\Column(name="begin_time", type="datetime", nullable=true)
	 */
	private $beginTime;

	/**
	 * @var datetime $endTime
	 *
	 * @ORM\Column(name="endTime", type="datetime", nullable=true)
	 */
	private $endTime;

	/**
	 * @var array $deaths
	 *
	 * @ORM\Column(name="deaths", type="array", nullable=true)
	 */
	private $deaths = array();

	/**
	 * @var integer $deathsCount
	 *
	 * @ORM\Column(name="deathsCount", type="integer", nullable=true)
	 */
	private $deathsCount;

	/**
	 * @var integer $gainedXp
	 *
	 * @ORM\Column(name="gainedXp", type="integer", nullable=true)
	 */
	private $gainedXp;

	/**
	 * @var double $xps
	 *
	 * @ORM\Column(name="xps", type="integer", nullable=true)
	 */
	private $xps;

	/**
	 * Fills in the stats object with informations from the array
	 *
	 * @param array $statsArray
	 */
	public function fillInWithArray($statsArray) {
		$this->setBeginTime($statsArray["startTime"]);
		$this->setEndTime($statsArray["endTime"]);
		if (isset($statsArray[TranslationMappingService::$DAMAGE_STRING_ID])) {
			$this
					->setDmgDoneCritRatioCapacity(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"]["crit_count"]
									/ $statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"]["count"]);
			$this
					->setDmgDoneCritRatioValue(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"]["crit_value"]
									/ $statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"]["total"]);
			$this
					->setDmgTakenCritRatioCapacity(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["crit_count"]
									/ $statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["count"]);
			$this
					->setDmgTakenCritRatioValue(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["crit_value"]
									/ $statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["total"]);

			$this
					->setMaxDamageDealt(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"]["max"]);
			$this
					->setMaxDamageTaken(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["max"]);

			$this
					->setTotalAbsorbedDamages(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["absorbed"]);
			$this
					->setTotalDamageDone(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"]["total"]);
			$this
					->setTotalDamageTaken(
							$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]["total"]);

			unset(
					$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Done"],
					$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["Received"]);
			if (isset(
					$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["PerSkill"]))
				$this
						->setDamagePerCapacity(
								$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["PerSkill"]);
			if (isset(
					$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["AvgPerSkill"]))
				$this
						->setDamageAvgPerCapacity(
								$statsArray[TranslationMappingService::$DAMAGE_STRING_ID]["AvgPerSkill"]);
		}

		if (isset($statsArray[TranslationMappingService::$HEAL_STRING_ID])) {
			$this
					->setHealCritRatioCapacity(
							$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"]["crit_count"]
									/ $statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"]["count"]);
			$this
					->setHealCritRatioValue(
							$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"]["crit_value"]
									/ $statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"]["total"]);

			$this
					->setMaxHealDone(
							$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"]["max"]);
			$this
					->setMaxHealReceived(
							$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Received"]["max"]);

			$this
					->setTotalHealDone(
							$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"]["total"]);
			$this
					->setTotalHealReceived(
							$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Received"]["total"]);

			unset(
					$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Done"],
					$statsArray[TranslationMappingService::$HEAL_STRING_ID]["Received"]);
			if (isset(
					$statsArray[TranslationMappingService::$HEAL_STRING_ID]["PerSkill"]))
				$this
						->setHealPerCapacity(
								$statsArray[TranslationMappingService::$HEAL_STRING_ID]["PerSkill"]);
			if (isset(
					$statsArray[TranslationMappingService::$HEAL_STRING_ID]["AvgPerSkill"]))
				$this
						->setHealAvgPerCapacity(
								$statsArray[TranslationMappingService::$HEAL_STRING_ID]["AvgPerSkill"]);

		}

		if (isset($statsArray["Threat"]["total"]))
			$this->setTotalThreat($statsArray["Threat"]["total"]);
		if (isset($statsArray["Threat"]["PerSkill"]))
			$this->setThreatPerCapacity($statsArray["Threat"]["PerSkill"]);
		if (isset($statsArray["Threat"]["AvgPerSkill"]))
			$this
					->setThreatAvgPerCapacity(
							$statsArray["Threat"]["AvgPerSkill"]);

		if (isset($statsArray["Deaths"]))
			$this->setDeaths($statsArray["Deaths"]);
		if (isset($statsArray["Deaths"]["Self"]))
			$this->setDeathsCount(count($statsArray["Deaths"]["Self"]));
		if (isset($statsArray["XpGained"]))
			$this->setGainedXp($statsArray["XpGained"]);

		$secs = $statsArray["msDuration"] / 1000;
		$this
				->setOverallDps(
						$secs == 0 ? 0 : $this->getTotalDamageDone() / $secs);
		$this
				->setOverallHps(
						$secs == 0 ? 0 : $this->getTotalHealDone() / $secs);
		$this->setOverallTps($secs == 0 ? 0 : $this->getTotalThreat() / $secs);
		$this->setXps($secs == 0 ? 0 : $this->getGainedXp() / $secs);

		if (isset($statsArray["HealPerTarget"]))
			$this->setHealPerTarget($statsArray["HealPerTarget"]);
		if (isset($statsArray["HealPerSource"]))
			$this->setHealPerSource($statsArray["HealPerSource"]);

		if (isset($statsArray["DpsOverTime"]))
			$this->setDpsOverTime($statsArray["DpsOverTime"]);
		if (isset($statsArray["HpsOverTime"]))
			$this->setHpsOverTime($statsArray["HpsOverTime"]);
		if (isset($statsArray["DpsOverTime"]))
			$this->setTpsOverTime($statsArray["TpsOverTime"]);

		if (isset($statsArray["DamageOverTime"]))
			$this->setDamageOverTime($statsArray["DamageOverTime"]);
		if (isset($statsArray["HealOverTime"]))
			$this->setHealOverTime($statsArray["HealOverTime"]);

		unset($statsArray);
	}

	/**
	 * Fills in the stats object based on the aggregate object
	 *
	 * @param $object
	 */
	public function aggregate($object) {
		if ($this->getToUpdate() == false)
			return;
		switch (get_class($object)) {
		case "LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog":
			if ($this->getReferencingCombatLog() == null) {
				$this->setReferencingCombatLog($object);
			}
			$stats = array();
			foreach ($object->getFights() as $fight) {
				$stats = array_merge($stats, $fight->getLogStats()->toArray());
			}
			$this->computeWithStats($stats);
			$this->setToUpdate(false);
			break;
		case "LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter":
			if ($this->getReferencingTorCharacter() == null) {
				$this->setReferencingTorCharacter($object);
			}
			$stats = array();
			foreach ($object->getLogs() as $cl) {
				foreach ($cl->getFights() as $fight) {
					$stats = array_merge($stats,
							$fight->getLogStats()->toArray());
				}
			}
			$this->computeWithStats($stats);
			$this->setToUpdate(false);
			break;
		case "LogAnalyzer\Bundle\UserBundle\Entity\User":
			if ($this->getReferencingUser() == null) {
				$this->setReferencingUser($object);
			}
			$stats = array();
			foreach ($object->getCharacters() as $tc) {
				foreach ($tc->getLogs() as $cl) {
					foreach ($cl->getFights() as $fight) {
						$stats = array_merge($stats,
								$fight->getLogStats()->toArray());
					}
				}
			}
			$this->computeWithStats($stats);
			$this->setToUpdate(false);
			break;
		case "LogAnalyzer\Bundle\UserBundle\Entity\Guild":
			if ($this->getReferencingGuild() == null) {
				$this->setReferencingGuild($object);
			}
			$stats = array();
			foreach ($object->getMembers() as $tc) {
				foreach ($tc->getLogs() as $cl) {
					foreach ($cl->getFights() as $fight) {
						$stats = array_merge($stats,
								$fight->getLogStats()->toArray());
					}
				}
			}
			$this->computeWithStats($stats);
			$this->setToUpdate(false);
			break;
		default:
			break;
		}
	}

	/**
	 * Returns true if stats is not referenced
	 *
	 * @return boolean
	 */
	public function isGroupedStats() {
		return $this->getStatsType() == "group";
	}

	public function getStatsType()
	{
		if ($this->getReferencingCombatLog() != null)
			return "combatlog";
		if ($this->getReferencingFight() != null)
			return "fight";
		if ($this->getReferencingGuild() != null)
			return "guild";
		if ($this->getReferencingTorCharacter() != null)
			return "character";
		if ($this->getReferencingUser() != null)
			return "user";
		return "group";
	}

	public function getReferencingObject()
	{
		if ($this->getReferencingCombatLog() != null)
			return $this->getReferencingCombatLog();
		if ($this->getReferencingFight() != null)
			return $this->getReferencingFight();
		if ($this->getReferencingGuild() != null)
			return $this->getReferencingGuild();
		if ($this->getReferencingTorCharacter() != null)
			return $this->getReferencingTorCharacter();
		if ($this->getReferencingUser() != null)
			return $this->getReferencingUser();
		return null;
	}

	/**
	 * Fills in the stats object based on several statistics
	 *
	 * @param array $stats
	 */
	public function computeWithStats($stats) {
		if (count($stats) == 0)
			return;
		if (count($stats) == 1)
			return $this->copyStats($stats[0]);
		if ($this->getStatsType() == "group") {
			$keysToAggregate = GroupStats::getStatsFieldsToAggregate();
		} else {
			$keysToAggregate = $this->getReferencingObject()->getStatsFieldsToAggregate();
		}
		foreach ($keysToAggregate as $key) {
			if (strpos($key, "Per") !== false || strpos($key, "ps") !== false) {
				foreach ($stats as $st) {
					$arrayPer = $this->$key;
					if (isset($st->$key) && $st->$key != null) {
						foreach ($st->$key as $cap => $capVal) {
							if (!isset($arrayPer[$cap])) {
								$arrayPer[$cap] = 0;
							}
							$arrayPer[$cap] += (int) $capVal;
						}
					}
					$this->$key = $arrayPer;
				}
			} else if ($key == "deaths") {
				foreach ($stats as $st) {
					$arrayPer = $this->$key;
					if (isset($st->$key) && $st->$key != null) {
						$arrayPer = array_merge($arrayPer, $st->$key);
					}
					$this->$key = $arrayPer;
				}
			} else if ($key == "damageOverTime") {
				if (!isset($this->$key) || $this->$key == null)
					$this->$key = array();
				$dotArray = $this->$key;
				foreach ($stats as $st) {
					foreach ($st->$key as $type => $arrayVals) {
						foreach ($arrayVals as $timestamp => $amount) {
							$dotArray[$type][$timestamp] = $amount;
						}
					}
				}
				$this->$key = $dotArray;
			} else if (strpos($key, "max") !== false) {
				foreach ($stats as $st) {
					$elems = explode("value: ", $this->$key);
					if (count($elems) > 0 && isset($elems[1])) {
						$elems = explode(" ability:", $elems[1]);
						$thisMaxVal = $elems[0];
					} else {
						$thisMaxVal = 0;
					}
					$elems = explode("value: ", $st->$key);
					if (count($elems) > 0 && isset($elems[1])) {
						$elems = explode(" ability:", $elems[1]);
						$stMaxVal = $elems[0];
					} else {
						$stMaxVal = 0;
					}
					if ($stMaxVal > $thisMaxVal)
						$this->$key = $st->$key;
				}
			} else if (strpos($key, "total") !== false
					|| $key == "deathsCount" || $key == "gainedXp") {
				foreach ($stats as $st) {
					$this->$key += $st->$key;
				}
			} else if (strpos($key, "Crit") !== false) {
				// We do the mean, even though it's not really accurate
				foreach ($stats as $st) {
					$this->$key += $st->$key;
				}
				$this->$key /= count($stats);
			}
		}
		if ($this->getBeginTime() == null)
			$this->setBeginTime(new \DateTime());
		if ($this->getEndTime() == null)
			$this->setEndTime(new \DateTime("1970-01-01"));
		foreach ($stats as $st) {
			if ($this->getBeginTime() >= $st->getBeginTime())
				$this->setBeginTime($st->getBeginTime());
			if ($this->getEndTime() <= $st->getEndTime())
				$this->setEndTime($st->getEndTime());
		}
		$secs = $this->getEndTime()->getTimestamp()
				- $this->getBeginTime()->getTimestamp();
		$this
				->setOverallDps(
						$secs == 0 ? 0 : $this->getTotalDamageDone() / $secs);
		$this
				->setOverallHps(
						$secs == 0 ? 0 : $this->getTotalHealDone() / $secs);
		$this->setOverallTps($secs == 0 ? 0 : $this->getTotalThreat() / $secs);
		$this->setXps($secs == 0 ? 0 : $this->getGainedXp() / $secs);
		$this->setToUpdate(false);
	}

	public function copyStats(Stats $st) {
		foreach ($this as $key => $value) {
			if ($key != "id" && strpos($key, "referencing") === false) {
				$this->$key = $st->$key;
			}
		}
		$this->setToUpdate(false);
	}

	/**
	 * Returns the player's name for those stats
	 *
	 * @return String
	 */
	public function getPlayerName() {
		if ($this->referencingCombatLog == null
				&& $this->referencingFight == null
				&& $this->referencingTorCharacter == null)
			return "";
		if ($this->referencingCombatLog)
			return $this->referencingCombatLog->getTorCharacter()->getName();
		if ($this->referencingTorCharacter)
			return $this->referencingTorCharacter->getName();
		if ($this->referencingFight)
			return $this->referencingFight->getCombatLog()->getTorCharacter()
					->getName();
	}

	/**
	 * Returns heal per capacity datas in JSON
	 *
	 * @return string
	 */
	public function getHealChartData() {
		return $this->encodeHighChart($this->getHealPerCapacity());
	}

	/**
	 * Returns heal per target datas in JSON
	 *
	 * @return string
	 */
	public function getHealPerTargetData() {
		return $this->encodeHighChart($this->getHealPerTarget());
	}

	/**
	 * Returns heal per source datas in JSON
	 *
	 * @return string
	 */
	public function getHealPerSourceData() {
		return $this->encodeHighChart($this->getHealPerSource());
	}

	/**
	 * Returns damages per capacity datas in JSON
	 *
	 * @return string
	 */
	public function getDamagesChartData() {
		return $this->encodeHighChart($this->getDamagePerCapacity());
	}

	private function encodeHighChart($arrayToEncode, $numericKeys = false) {
		if (count($arrayToEncode) == 0) {
			return "[[\"Nothing to display\", 100]]";
		}
		$str = "[";
		foreach ($arrayToEncode as $key => $value) {
			$str .= "[";
			if (!$numericKeys)
				$str .= "\"";
			$str .= $key;
			if (!$numericKeys)
				$str .= "\"";
			$str .= ", " . $value . "],";
		}
		$str = substr($str, 0, -1); // Removing the last comma
		$str .= "]";
		return $str;
	}

	/**
	 * Returns damages over time datas in JSON
	 *
	 * @return string
	 */
	public function getDamagesOverTimeData() {
		$str = "[";
		foreach ($this->damageOverTime as $valueType => $array) {
			$str .= "{name:\"" . $valueType . "\", data:";
			$str .= $this->encodeHighChart($array, true);
			$str .= "}, ";
		}
		$str = substr($str, 0, -2);
		$str .= "]";
		return ($str != "]" ? $str : "[]");
	}

	/**
	 * Returns heals over time datas in JSON
	 *
	 * @return string
	 */
	public function getHealsOverTimeData() {
		return $this->encodeHighChart($this->getHealOverTime(), true);
	}

	/**
	 * Returns hps over time datas in JSON
	 *
	 * @return string
	 */
	public function getHpsOverTimeData() {
		return $this->encodeHighChart($this->getHpsOverTime(), true);
	}

	/**
	 * Returns dps over time datas in JSON
	 *
	 * @return string
	 */
	public function getDpsOverTimeData() {
		return $this->encodeHighChart($this->getDpsOverTime(), true);
	}

	/**
	 * Returns dps over time datas in JSON
	 *
	 * @return string
	 */
	public function getTpsOverTimeData() {
		return $this->encodeHighChart($this->getTpsOverTime(), true);
	}

	/**
	 * Get logEntries
	 *
	 * @return Collection
	 */
	public function getLogs() {
		$ret = array();
		if ($this->getReferencingGuild() != null) {
			foreach ($this->getReferencingGuild()->getMembers() as $member) {
				foreach ($member->getCombatLogs() as $combatLog) {
					$ret = array_merge($ret,
							$combatLog->getEntries()->toArray());
				}
			}
		} else if ($this->getReferencingUser() != null) {
			foreach ($this->getReferencingUser()->getTorCharacters() as $member) {
				foreach ($member->getCombatLogs() as $combatLog) {
					$ret = array_merge($ret,
							$combatLog->getEntries()->toArray());
				}
			}
		} else if ($this->getReferencingTorCharacter() != null) {
			foreach ($this->getReferencingTorCharacter() as $char) {
				$ret = array_merge($ret, $char->getEntries()->toArray());
			}
		} else if ($this->getReferencingFight() != null) {
			$ret = $this->getReferencingFight()->getEntries();
		} else if ($this->getReferencingCombatLog() != null) {
			$ret = $this->getReferencingCombatLog()->getEntries()->toArray();
		}
		return new ArrayCollection($ret);
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set overallHps
	 *
	 * @param decimal $overallHps
	 */
	public function setOverallHps($overallHps) {
		$this->overallHps = $overallHps;
	}

	/**
	 * Get overallHps
	 *
	 * @return decimal
	 */
	public function getOverallHps() {
		return $this->overallHps;
	}

	/**
	 * Set overallDps
	 *
	 * @param decimal $overallDps
	 */
	public function setOverallDps($overallDps) {
		$this->overallDps = $overallDps;
	}

	/**
	 * Get overallDps
	 *
	 * @return decimal
	 */
	public function getOverallDps() {
		return $this->overallDps;
	}

	/**
	 * Set overallTps
	 *
	 * @param decimal $overallTps
	 */
	public function setOverallTps($overallTps) {
		$this->overallTps = $overallTps;
	}

	/**
	 * Get overallTps
	 *
	 * @return decimal
	 */
	public function getOverallTps() {
		return $this->overallTps;
	}

	/**
	 * Set maxHealDone
	 *
	 * @param integer $maxHealDone
	 */
	public function setMaxHealDone($maxHealDone) {
		$this->maxHealDone = $maxHealDone;
	}

	/**
	 * Get maxHealDone
	 *
	 * @return integer
	 */
	public function getMaxHealDone() {
		return $this->maxHealDone;
	}

	/**
	 * Set maxHealReceived
	 *
	 * @param string $maxHealReceived
	 */
	public function setMaxHealReceived($maxHealReceived) {
		$this->maxHealReceived = $maxHealReceived;
	}

	/**
	 * Get maxHealReceived
	 *
	 * @return string
	 */
	public function getMaxHealReceived() {
		return $this->maxHealReceived;
	}

	/**
	 * Set maxDamageDealt
	 *
	 * @param integer $maxDamageDealt
	 */
	public function setMaxDamageDealt($maxDamageDealt) {
		$this->maxDamageDealt = $maxDamageDealt;
	}

	/**
	 * Get maxDamageDealt
	 *
	 * @return integer
	 */
	public function getMaxDamageDealt() {
		return $this->maxDamageDealt;
	}

	/**
	 * Set maxDamageTaken
	 *
	 * @param integer $maxDamageTaken
	 */
	public function setMaxDamageTaken($maxDamageTaken) {
		$this->maxDamageTaken = $maxDamageTaken;
	}

	/**
	 * Get maxDamageTaken
	 *
	 * @return integer
	 */
	public function getMaxDamageTaken() {
		return $this->maxDamageTaken;
	}

	/**
	 * Set totalHealDone
	 *
	 * @param integer $totalHealDone
	 */
	public function setTotalHealDone($totalHealDone) {
		$this->totalHealDone = $totalHealDone;
	}

	/**
	 * Get totalHealDone
	 *
	 * @return integer
	 */
	public function getTotalHealDone() {
		return $this->totalHealDone;
	}

	/**
	 * Set totalHealReceived
	 *
	 * @param integer $totalHealReceived
	 */
	public function setTotalHealReceived($totalHealReceived) {
		$this->totalHealReceived = $totalHealReceived;
	}

	/**
	 * Get totalHealReceived
	 *
	 * @return integer
	 */
	public function getTotalHealReceived() {
		return $this->totalHealReceived;
	}

	/**
	 * Set totalDamageDone
	 *
	 * @param integer $totalDamageDone
	 */
	public function setTotalDamageDone($totalDamageDone) {
		$this->totalDamageDone = $totalDamageDone;
	}

	/**
	 * Get totalDamageDone
	 *
	 * @return integer
	 */
	public function getTotalDamageDone() {
		return $this->totalDamageDone;
	}

	/**
	 * Set totalDamageTaken
	 *
	 * @param integer $totalDamageTaken
	 */
	public function setTotalDamageTaken($totalDamageTaken) {
		$this->totalDamageTaken = $totalDamageTaken;
	}

	/**
	 * Get totalDamageTaken
	 *
	 * @return integer
	 */
	public function getTotalDamageTaken() {
		return $this->totalDamageTaken;
	}

	/**
	 * Set totalThreat
	 *
	 * @param integer $totalThreat
	 */
	public function setTotalThreat($totalThreat) {
		$this->totalThreat = $totalThreat;
	}

	/**
	 * Get totalThreat
	 *
	 * @return integer
	 */
	public function getTotalThreat() {
		return $this->totalThreat;
	}

	/**
	 * Set totalAbsorbedDamages
	 *
	 * @param integer $totalAbsorbedDamages
	 */
	public function setTotalAbsorbedDamages($totalAbsorbedDamages) {
		$this->totalAbsorbedDamages = $totalAbsorbedDamages;
	}

	/**
	 * Get totalAbsorbedDamages
	 *
	 * @return integer
	 */
	public function getTotalAbsorbedDamages() {
		return $this->totalAbsorbedDamages;
	}

	/**
	 * Set totalInterrupts
	 *
	 * @param integer $totalInterrupts
	 */
	public function setTotalInterrupts($totalInterrupts) {
		$this->totalInterrupts = $totalInterrupts;
	}

	/**
	 * Get totalInterrupts
	 *
	 * @return integer
	 */
	public function getTotalInterrupts() {
		return $this->totalInterrupts;
	}

	/**
	 * Set totalDispells
	 *
	 * @param integer $totalDispells
	 */
	public function setTotalDispells($totalDispells) {
		$this->totalDispells = $totalDispells;
	}

	/**
	 * Get totalDispells
	 *
	 * @return integer
	 */
	public function getTotalDispells() {
		return $this->totalDispells;
	}

	/**
	 * Set healPerCapacity
	 *
	 * @param array $healPerCapacity
	 */
	public function setHealPerCapacity($healPerCapacity) {
		$this->healPerCapacity = $healPerCapacity;
	}

	/**
	 * Get healPerCapacity
	 *
	 * @return array
	 */
	public function getHealPerCapacity() {
		return $this->healPerCapacity;
	}

	/**
	 * Set damagePerCapacity
	 *
	 * @param array $damagePerCapacity
	 */
	public function setDamagePerCapacity($damagePerCapacity) {
		$this->damagePerCapacity = $damagePerCapacity;
	}

	/**
	 * Get damagePerCapacity
	 *
	 * @return array
	 */
	public function getDamagePerCapacity() {
		return $this->damagePerCapacity;
	}

	/**
	 * Set healCritRatioValue
	 *
	 * @param decimal $healCritRatioValue
	 */
	public function setHealCritRatioValue($healCritRatioValue) {
		$this->healCritRatioValue = $healCritRatioValue;
	}

	/**
	 * Get healCritRatioValue
	 *
	 * @return decimal
	 */
	public function getHealCritRatioValue() {
		return $this->healCritRatioValue;
	}

	/**
	 * Set dmgDoneCritRatioValue
	 *
	 * @param decimal $dmgDoneCritRatioValue
	 */
	public function setDmgDoneCritRatioValue($dmgDoneCritRatioValue) {
		$this->dmgDoneCritRatioValue = $dmgDoneCritRatioValue;
	}

	/**
	 * Get dmgDoneCritRatioValue
	 *
	 * @return decimal
	 */
	public function getDmgDoneCritRatioValue() {
		return $this->dmgDoneCritRatioValue;
	}

	/**
	 * Set dmgTakenCritRatioValue
	 *
	 * @param decimal $dmgTakenCritRatioValue
	 */
	public function setDmgTakenCritRatioValue($dmgTakenCritRatioValue) {
		$this->dmgTakenCritRatioValue = $dmgTakenCritRatioValue;
	}

	/**
	 * Get dmgTakenCritRatioValue
	 *
	 * @return decimal
	 */
	public function getDmgTakenCritRatioValue() {
		return $this->dmgTakenCritRatioValue;
	}

	/**
	 * Set healCritRatioCapacity
	 *
	 * @param decimal $healCritRatioCapacity
	 */
	public function setHealCritRatioCapacity($healCritRatioCapacity) {
		$this->healCritRatioCapacity = $healCritRatioCapacity;
	}

	/**
	 * Get healCritRatioCapacity
	 *
	 * @return decimal
	 */
	public function getHealCritRatioCapacity() {
		return $this->healCritRatioCapacity;
	}

	/**
	 * Set dmgDoneCritRatioCapacity
	 *
	 * @param decimal $dmgDoneCritRatioCapacity
	 */
	public function setDmgDoneCritRatioCapacity($dmgDoneCritRatioCapacity) {
		$this->dmgDoneCritRatioCapacity = $dmgDoneCritRatioCapacity;
	}

	/**
	 * Get dmgDoneCritRatioCapacity
	 *
	 * @return decimal
	 */
	public function getDmgDoneCritRatioCapacity() {
		return $this->dmgDoneCritRatioCapacity;
	}

	/**
	 * Set dmgTakenCritRatioCapacity
	 *
	 * @param decimal $dmgTakenCritRatioCapacity
	 */
	public function setDmgTakenCritRatioCapacity($dmgTakenCritRatioCapacity) {
		$this->dmgTakenCritRatioCapacity = $dmgTakenCritRatioCapacity;
	}

	/**
	 * Get dmgTakenCritRatioCapacity
	 *
	 * @return decimal
	 */
	public function getDmgTakenCritRatioCapacity() {
		return $this->dmgTakenCritRatioCapacity;
	}

	/**
	 * Set beginTime
	 *
	 * @param datetime $beginTime
	 */
	public function setBeginTime($beginTime) {
		$this->beginTime = $beginTime;
	}

	/**
	 * Get beginTime
	 *
	 * @return datetime
	 */
	public function getBeginTime() {
		return $this->beginTime;
	}

	/**
	 * Set endTime
	 *
	 * @param datetime $endTime
	 */
	public function setEndTime($endTime) {
		$this->endTime = $endTime;
	}

	/**
	 * Get endTime
	 *
	 * @return datetime
	 */
	public function getEndTime() {
		return $this->endTime;
	}

	/**
	 * Set referencingCombatLog
	 *
	 * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $referencingCombatLog
	 */
	public function setReferencingCombatLog(
			\LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $referencingCombatLog) {
		$this->referencingCombatLog = $referencingCombatLog;
	}

	public function setReferencingObject($object) {
		if ($object instanceof CombatLog) {
			$this->setReferencingCombatLog($object);
		} else if ($object instanceof \LogAnalyzer\Bundle\UserBundle\Entity\User) {
			$this->setReferencingUser($object);
		} else if ($object instanceof TorCharacter) {
			$this->setReferencingTorCharacter($object);
		} else if ($object instanceof Guild) {
			$this->setReferencingGuild($object);
		} else if ($object instanceof Fight) {
			$this->setReferencingFight($object);
		} else {
			throw new \Exception("Class not supported");
		}
	}

	/**
	 * Get referencingCombatLog
	 *
	 * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog
	 */
	public function getReferencingCombatLog() {
		return $this->referencingCombatLog;
	}

	/**
	 * Set referencingTorCharacter
	 *
	 * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $referencingTorCharacter
	 */
	public function setReferencingTorCharacter(
			\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $referencingTorCharacter) {
		$this->referencingTorCharacter = $referencingTorCharacter;
	}

	/**
	 * Get referencingTorCharacter
	 *
	 * @return LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter
	 */
	public function getReferencingTorCharacter() {
		return $this->referencingTorCharacter;
	}

	/**
	 * Set referencingUser
	 *
	 * @param LogAnalyzer\Bundle\UserBundle\Entity\User $referencingUser
	 */
	public function setReferencingUser(
			\LogAnalyzer\Bundle\UserBundle\Entity\User $referencingUser) {
		$this->referencingUser = $referencingUser;
	}

	/**
	 * Get referencingUser
	 *
	 * @return LogAnalyzer\Bundle\UserBundle\Entity\User
	 */
	public function getReferencingUser() {
		return $this->referencingUser;
	}

	/**
	 * Set referencingGuild
	 *
	 * @param LogAnalyzer\Bundle\UserBundle\Entity\Guild $referencingGuild
	 */
	public function setReferencingGuild(
			\LogAnalyzer\Bundle\UserBundle\Entity\Guild $referencingGuild) {
		$this->referencingGuild = $referencingGuild;
	}

	/**
	 * Get referencingGuild
	 *
	 * @return LogAnalyzer\Bundle\UserBundle\Entity\Guild
	 */
	public function getReferencingGuild() {
		return $this->referencingGuild;
	}

	/**
	 * Set source
	 *
	 * @param string $source
	 */
	public function setSource($source) {
		$this->source = $source;
	}

	/**
	 * Get source
	 *
	 * @return string
	 */
	public function getSource() {
		return $this->source;
	}

	/**
	 * Set target
	 *
	 * @param string $target
	 */
	public function setTarget($target) {
		$this->target = $target;
	}

	/**
	 * Get target
	 *
	 * @return string
	 */
	public function getTarget() {
		return $this->target;
	}

	/**
	 * Set damageOverTime
	 *
	 * @param array $damageOverTime
	 */
	public function setDamageOverTime($damageOverTime) {
		$this->damageOverTime = $damageOverTime;
	}

	/**
	 * Get damageOverTime
	 *
	 * @return array
	 */
	public function getDamageOverTime() {
		return $this->damageOverTime;
	}

	/**
	 * Set healPerTarget
	 *
	 * @param array $healPerTarget
	 */
	public function setHealPerTarget($healPerTarget) {
		$this->healPerTarget = $healPerTarget;
	}

	/**
	 * Get healPerTarget
	 *
	 * @return array
	 */
	public function getHealPerTarget() {
		return $this->healPerTarget;
	}

	/**
	 * Set healPerSource
	 *
	 * @param array $healPerSource
	 */
	public function setHealPerSource($healPerSource) {
		$this->healPerSource = $healPerSource;
	}

	/**
	 * Get healPerSource
	 *
	 * @return array
	 */
	public function getHealPerSource() {
		return $this->healPerSource;
	}

	/**
	 * Set referencingFight
	 *
	 * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight $referencingFight
	 */
	public function setReferencingFight(
			\LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight $referencingFight) {
		$this->referencingFight = $referencingFight;
	}

	/**
	 * Get referencingFight
	 *
	 * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight
	 */
	public function getReferencingFight() {
		return $this->referencingFight;
	}

	/**
	 * Set healOverTime
	 *
	 * @param array $healOverTime
	 */
	public function setHealOverTime($healOverTime) {
		$this->healOverTime = $healOverTime;
	}

	/**
	 * Get healOverTime
	 *
	 * @return array
	 */
	public function getHealOverTime() {
		return $this->healOverTime;
	}

	/**
	 * Set toUpdate
	 *
	 * @param boolean $toUpdate
	 */
	public function setToUpdate($toUpdate) {
		if ($toUpdate && count($this->groups) > 0) {
			foreach ($this->groups as $group) {
				$group->getAggregatedStats()->setToUpdate(true);
			}
		}
		$this->toUpdate = $toUpdate;
	}

	/**
	 * Get toUpdate
	 *
	 * @return boolean
	 */
	public function getToUpdate() {
		return $this->toUpdate;
	}

	/**
	 * Set dpsOverTime
	 *
	 * @param array $dpsOverTime
	 */
	public function setDpsOverTime($dpsOverTime) {
		$this->dpsOverTime = $dpsOverTime;
	}

	/**
	 * Get dpsOverTime
	 *
	 * @return array
	 */
	public function getDpsOverTime() {
		return $this->dpsOverTime;
	}

	/**
	 * Set hpsOverTime
	 *
	 * @param array $hpsOverTime
	 */
	public function setHpsOverTime($hpsOverTime) {
		$this->hpsOverTime = $hpsOverTime;
	}

	/**
	 * Get hpsOverTime
	 *
	 * @return array
	 */
	public function getHpsOverTime() {
		return $this->hpsOverTime;
	}

	/**
	 * Set tpsOverTime
	 *
	 * @param array $tpsOverTime
	 */
	public function setTpsOverTime($tpsOverTime) {
		$this->tpsOverTime = $tpsOverTime;
	}

	/**
	 * Get tpsOverTime
	 *
	 * @return array
	 */
	public function getTpsOverTime() {
		return $this->tpsOverTime;
	}

	/**
	 * Set deaths
	 *
	 * @param array $deaths
	 */
	public function setDeaths($deaths) {
		$this->deaths = $deaths;
	}

	/**
	 * Get deaths
	 *
	 * @return array
	 */
	public function getDeaths() {
		return $this->deaths;
	}

	/**
	 * Returns death over time formatted datas for Highcharts
	 *
	 * @return string
	 */
	public function getDeathsOverTimeData($displayMobDeaths = false) {
		$str = "[";
		foreach ($this->getDeaths() as $deathTypeName => $deathTypeArray) {
			if ($deathTypeName != "Monster" || $displayMobDeaths) {
				if ($deathTypeName == "Monster")
					$color = "blue";
				if ($deathTypeName == "Player")
					$color = "green";
				if ($deathTypeName == "Self")
					$color = "red";
				foreach ($deathTypeArray as $time => $deadPerson) {
					$str .= "{value: " . $time . ", color:'" . $color
							. "', dashStyle: 'dash', width: 2, label: { text: '"
							. $deadPerson->getStringValue()
							. " died.', rotation: 0, verticalAlign: 'bottom', textAlign: 'right', y: -10, x: -5}},";
				}
			}
		}
		if (strlen($str) > 1) {
			$str = substr($str, 0, -1);
		}
		$str .= "]";
		return $str;
	}

	/**
	 * Set deathsCount
	 *
	 * @param integer $deathsCount
	 */
	public function setDeathsCount($deathsCount) {
		$this->deathsCount = $deathsCount;
	}

	/**
	 * Get deathsCount
	 *
	 * @return integer
	 */
	public function getDeathsCount() {
		return $this->deathsCount;
	}

	/**
	 * Set gainedXp
	 *
	 * @param integer $gainedXp
	 */
	public function setGainedXp($gainedXp) {
		$this->gainedXp = $gainedXp;
	}

	/**
	 * Get gainedXp
	 *
	 * @return int
	 */
	public function getGainedXp() {
		return $this->gainedXp;
	}

	/**
	 * Set xps
	 *
	 * @param integer $xps
	 */
	public function setXps($xps) {
		$this->xps = $xps;
	}

	/**
	 * Get xps
	 *
	 * @return integer
	 */
	public function getXps() {
		return $this->xps;
	}
	public function __construct() {
		$this->groups = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add groups
	 *
	 * @param LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats $groups
	 */
	public function addGroupStats(
			\LogAnalyzer\Bundle\StatsBundle\Entity\GroupStats $groups) {
		$this->groups[] = $groups;
	}

	/**
	 * Get groups
	 *
	 * @return Doctrine\Common\Collections\Collection
	 */
	public function getGroups() {
		return $this->groups;
	}

	/**
	 * Set threatPerCapacity
	 *
	 * @param array $threatPerCapacity
	 */
	public function setThreatPerCapacity($threatPerCapacity) {
		$this->threatPerCapacity = $threatPerCapacity;
	}

	/**
	 * Returns threat per capacity datas in JSON
	 *
	 * @return string
	 */
	public function getThreatChartData() {
		return $this->encodeHighChart($this->getThreatPerCapacity());
	}

	/**
	 * Get threatPerCapacity
	 *
	 * @return array
	 */
	public function getThreatPerCapacity() {
		return $this->threatPerCapacity;
	}

	/**
	 * Set healAvgPerCapacity
	 *
	 * @param array $healAvgPerCapacity
	 */
	public function setHealAvgPerCapacity($healAvgPerCapacity) {
		$this->healAvgPerCapacity = $healAvgPerCapacity;
	}

	/**
	 * Get healAvgPerCapacity
	 *
	 * @return array
	 */
	public function getHealAvgPerCapacity() {
		return $this->healAvgPerCapacity;
	}

	/**
	 * Get healAvgPerCapacityData
	 *
	 * @return string
	 */
	public function getHealAvgPerCapacityData() {
		return $this->encodeHighChart($this->healAvgPerCapacity);
	}

	/**
	 * Set threatAvgPerCapacity
	 *
	 * @param array $threatAvgPerCapacity
	 */
	public function setThreatAvgPerCapacity($threatAvgPerCapacity) {
		$this->threatAvgPerCapacity = $threatAvgPerCapacity;
	}

	/**
	 * Get threatAvgPerCapacity
	 *
	 * @return array
	 */
	public function getThreatAvgPerCapacity() {
		return $this->threatAvgPerCapacity;
	}

	/**
	 * Get threatAvgPerCapacityData
	 *
	 * @return string
	 */
	public function getThreatAvgPerCapacityData() {
		return $this->encodeHighChart($this->threatAvgPerCapacity);
	}

	/**
	 * Set damageAvgPerCapacity
	 *
	 * @param array $damageAvgPerCapacity
	 */
	public function setDamageAvgPerCapacity($damageAvgPerCapacity) {
		$this->damageAvgPerCapacity = $damageAvgPerCapacity;
	}

	/**
	 * Get damageAvgPerCapacity
	 *
	 * @return array
	 */
	public function getDamageAvgPerCapacity() {
		return $this->damageAvgPerCapacity;
	}

	/**
	 * Get damageAvgPerCapacityData
	 *
	 * @return string
	 */
	public function getDamageAvgPerCapacityData() {
		return $this->encodeHighChart($this->damageAvgPerCapacity);
	}

}
