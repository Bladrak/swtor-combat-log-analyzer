<?php

namespace LogAnalyzer\Bundle\StatsBundle\Form;

use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;

use Admingenerator\GeneratorBundle\Form\Type\DoctrineDoubleListType;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GroupStatsType extends AbstractType
{
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$groupStats = $options["data"];
    	$guild = $server = null;
    	if ($groupStats->getGuild() != null) {
    		$guild = $groupStats->getGuild();
    	} else {
	    	$server = is_object($groupStats->getMembers()->first()) ? $groupStats->getMembers()->first()->getServer() : null;
    	}
    	
    	$builder->setAttribute('show_legend', false);
        $builder
            ->add('title')
            ->add('fromDate', 'datetime', array('widget' => 'single_text',
            									'label' => "From:",
            									'attr' => array('placeholder' => 'Click me!')))
            ->add('toDate', 'datetime', array('widget' => 'single_text',
            									'label' => "To:",
            									'attr' => array('placeholder' => 'Click me!')))
            ->add('description')
            ->add('guild', null, array('attr' => array('hidden' => true)))
            ->add('members', 'double_list', array(
            								'class' => 'LogAnalyzerUserBundle:TorCharacter',
            								'query_builder' => function(EntityRepository $er) use ($server, $guild) {
            														$qb = $er->createQueryBuilder('t')
            																	->orderBy('t.name', 'ASC');
            														if ($guild != null) {
            															$qb->where('t.guild = :guild')
            																->setParameter('guild', $guild);
            														}
            														if ($server != null) {
            															$qb->where('t.server = :server')
	            															->setParameter('server', $server);
            														}
            														return $qb;
									        },
									        'property' => 'nameAndClass',
            								))
            ;
    }

    public function getName()
    {
        return 'loganalyzer_bundle_statsbundle_groupstatstype';
    }
}
