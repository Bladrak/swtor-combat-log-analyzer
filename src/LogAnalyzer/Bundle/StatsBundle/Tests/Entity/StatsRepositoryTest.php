<?php

namespace LogAnalyzer\Bundle\StatsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StatsRepositoryTest extends WebTestCase {
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $_em;

	private $conn;

	protected function setUp() {
		$kernel = static::createKernel();
		$kernel->boot();
		$this->_em = $kernel->getContainer()
				->get('doctrine.orm.entity_manager');
		$this->conn = $kernel->getContainer()->get('database_connection');
	}

	public function testGetStatsFor() {
		$fight = $this->_em->getRepository('LogAnalyzerCombatLogBundle:Fight')->findOneBy(array('id' => 165));
//		$combatLog = $this->_em->getRepository('LogAnalyzerCombatLogBundle:CombatLog')->findOneBy(array('id' => 1));

		$retStRepo = $this->_em->getRepository('LogAnalyzerStatsBundle:GroupStats')->updateStatsForFight($fight);
	}
}
