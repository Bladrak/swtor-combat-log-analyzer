<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LogEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('log_timestamp')
            ->add('source')
            ->add('target')
            ->add('ability_name')
            ->add('ability_id')
            ->add('action_name')
            ->add('action_string_id')
            ->add('effect_name')
            ->add('effect_string_id')
            ->add('value')
            ->add('value_crit')
            ->add('threat')
            ->add('source_is_player')
            ->add('target_is_player')
            ->add('is_event')
            ->add('event_type')
            ->add('event_type_id')
            ->add('combat_log')
        ;
    }

    public function getName()
    {
        return 'loganalyzer_bundle_combatlogbundle_logentrytype';
    }
}
