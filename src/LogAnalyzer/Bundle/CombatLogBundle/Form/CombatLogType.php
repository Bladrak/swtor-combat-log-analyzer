<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CombatLogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("imported_file", "file", array('label' => "Import new combat log", 'required' => true))
            ->add("tor_character", "entity", array(
        			'class' => 'LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter',
        			'attr' => array('style' => 'display:none'),
            		))
        ;
    }

    public function getName()
    {
        return 'loganalyzer_bundle_combatlogbundle_combatlogtype';
    }
}
