<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Entity;

/**
 * LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry
 *
 */
use LogAnalyzer\Bundle\CombatLogBundle\Service\TranslationMappingService;

class LogEntry {
	/**
	 * @var \DateTime $log_timestamp
	 *
	 */
	private $log_timestamp;

	/**
	 * @var TranslationMapping $source
	 *
	 */
	private $source;

	/**
	 * @var TranslationMapping $target
	 *
	 */
	private $target;

	/**
	 * @var TranslationMapping $sourceCompanion
	 *
	 */
	private $sourceCompanion;

	/**
	 * @var TranslationMapping $targetCompanion
	 *
	 */
	private $targetCompanion;

	/**
	 * @var TranslationMapping $ability_name
	 *
	 */
	private $ability_name;

	/**
	 * @var TranslationMapping $resource_event
	 *
	 */
	private $resource_event;

	/**
	 * @var TranslationMapping $effect_name
	 */
	private $effect_name;

	/**
	 * @var integer $value
	 *
	 */
	private $value;

	/**
	 * @var boolean $value_crit
	 *
	 */
	private $value_crit;

	/**
	 * @var TranslationMapping $value_type
	 *
	 */
	private $value_type;

	/**
	 * @var string $value_absorbed
	 *
	 */
	private $value_absorbed;

	/**
	 * @var TranslationMapping $value_absorbed_name
	 *
	 */
	private $value_absorbed_name;

	/**
	 * @var TranslationMapping $value_effect_name
	 *
	 */
	private $value_effect_name;

	/**
	 * @var integer $threat
	 *
	 */
	private $threat;

	/**
	 * @var boolean $source_is_player
	 *
	 */
	private $source_is_player;

	/**
	 * @var boolean $target_is_player
	 *
	 */
	private $target_is_player;

	/**
	 * @var boolean $is_effect
	 *
	 */
	private $is_effect;

	/**
	 * @var CombatLog $combatLog
	 *
	 */
	private $combatLog;

	public function __toString() {
		return $this->readable();
	}

	/**
	 * Returns threat value with associated cause. If no threat, returns null.
	 *
	 * @return array
	 */
	public function getThreatAndCause()
	{
		if ($this->getThreat() == 0)
			return null;
		$ret["value"] = $this->getThreat();
		$ret["cause"] = ($this->getAbilityName() != null && $this->getAbilityName()->getStringValue() != "") ? $this->getAbilityName()->getStringValue() : $this->getEffectName()->getStringValue();
		return $ret;
	}

	public function isEndOfFight($username)
	{
		return $this->getEffectName()->getStringId() == TranslationMappingService::$EXIT_COMBAT_STRING_ID
				|| ($this->getEffectName()->getStringId() == TranslationMappingService::$DEATH_STRING_ID
						&& $this->getTarget()->getStringValue() == $username);
// 				 || $this->getAbilityName()->getStringId() == TranslationMappingService::$SECURE_CONNECT_STRING_ID
// 				 || $this->getEffectName()->getStringId() == TranslationMappingService::$RANIMATE_STRING_ID;
	}

	/**
	 * @return string
	 */
	public function readable() {
		$retstr = "From: " . $this->source->getStringValue() . " to: "
				. $this->target->getStringValue() . " effect: "
				. $this->effect_name->getStringValue() . " value: "
				. $this->value;
		if ($this->getValueAbsorbed() != null && $this->getValueAbsorbed() > 0)
			$retstr .= " absorbed: " . $this->value_absorbed;
		$retstr .= " ability: " . $this->ability_name->getStringValue();
		return $retstr;
	}


	public function initWithEmptyTm(TranslationMapping $tm)
	{
		$tmVars = array("ability_name", "effect_name", "resource_event",
						"source", "sourceCompanion", "target", "targetCompanion",
						"value_absorbed_name", "value_effect_name", "value_type");
		foreach (get_object_vars($this) as $key => $val)
		{
			if (in_array($key, $tmVars)){
				if ($this->$key == null)
					$this->$key = $tm;
			}
		}
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set log_timestamp
     *
     * @param datetime $logTimestamp
     */
    public function setLogTimestamp($logTimestamp)
    {
        $this->log_timestamp = $logTimestamp;
    }

    /**
     * Get log_timestamp
     *
     * @return datetime
     */
    public function getLogTimestamp()
    {
        return $this->log_timestamp;
    }

    /**
     * Set value
     *
     * @param integer $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value_crit
     *
     * @param boolean $valueCrit
     */
    public function setValueCrit($valueCrit)
    {
        $this->value_crit = $valueCrit;
    }

    /**
     * Get value_crit
     *
     * @return boolean
     */
    public function getValueCrit()
    {
        return $this->value_crit;
    }

    /**
     * Set value_absorbed
     *
     * @param string $valueAbsorbed
     */
    public function setValueAbsorbed($valueAbsorbed)
    {
        $this->value_absorbed = $valueAbsorbed;
    }

    /**
     * Get value_absorbed
     *
     * @return string
     */
    public function getValueAbsorbed()
    {
        return $this->value_absorbed;
    }

    /**
     * Set threat
     *
     * @param integer $threat
     */
    public function setThreat($threat)
    {
        $this->threat = $threat;
    }

    /**
     * Get threat
     *
     * @return integer
     */
    public function getThreat()
    {
        return $this->threat;
    }

    /**
     * Set source_is_player
     *
     * @param boolean $sourceIsPlayer
     */
    public function setSourceIsPlayer($sourceIsPlayer)
    {
        $this->source_is_player = $sourceIsPlayer;
    }

    /**
     * Get source_is_player
     *
     * @return boolean
     */
    public function getSourceIsPlayer()
    {
        return $this->source_is_player;
    }

    /**
     * Set target_is_player
     *
     * @param boolean $targetIsPlayer
     */
    public function setTargetIsPlayer($targetIsPlayer)
    {
        $this->target_is_player = $targetIsPlayer;
    }

    /**
     * Get target_is_player
     *
     * @return boolean
     */
    public function getTargetIsPlayer()
    {
        return $this->target_is_player;
    }

    /**
     * Set is_effect
     *
     * @param boolean $isEffect
     */
    public function setIsEffect($isEffect)
    {
        $this->is_effect = $isEffect;
    }

    /**
     * Get is_effect
     *
     * @return boolean
     */
    public function getIsEffect()
    {
        return $this->is_effect;
    }

    /**
     * Set source
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $source
     */
    public function setSource(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $source)
    {
        $this->source = $source;
    }

    /**
     * Get source
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set target
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $target
     */
    public function setTarget(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $target)
    {
        $this->target = $target;
    }

    /**
     * Get target
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set sourceCompanion
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $sourceCompanion
     */
    public function setSourceCompanion(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $sourceCompanion)
    {
        $this->sourceCompanion = $sourceCompanion;
    }

    /**
     * Get sourceCompanion
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getSourceCompanion()
    {
        return $this->sourceCompanion;
    }

    /**
     * Set targetCompanion
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $targetCompanion
     */
    public function setTargetCompanion(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $targetCompanion)
    {
        $this->targetCompanion = $targetCompanion;
    }

    /**
     * Get targetCompanion
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getTargetCompanion()
    {
        return $this->targetCompanion;
    }

    /**
     * Set ability_name
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $abilityName
     */
    public function setAbilityName(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $abilityName)
    {
        $this->ability_name = $abilityName;
    }

    /**
     * Get ability_name
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getAbilityName()
    {
        return $this->ability_name;
    }

    /**
     * Set resource_event
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $resourceEvent
     */
    public function setResourceEvent(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $resourceEvent)
    {
        $this->resource_event = $resourceEvent;
    }

    /**
     * Get resource_event
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getResourceEvent()
    {
        return $this->resource_event;
    }

    /**
     * Set effect_name
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $effectName
     */
    public function setEffectName(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $effectName)
    {
        $this->effect_name = $effectName;
    }

    /**
     * Get effect_name
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getEffectName()
    {
        return $this->effect_name;
    }

    /**
     * Set value_type
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $valueType
     */
    public function setValueType(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $valueType)
    {
        $this->value_type = $valueType;
    }

    /**
     * Get value_type
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getValueType()
    {
        return $this->value_type;
    }

    /**
     * Set value_absorbed_name
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $valueAbsorbedName
     */
    public function setValueAbsorbedName(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $valueAbsorbedName)
    {
        $this->value_absorbed_name = $valueAbsorbedName;
    }

    /**
     * Get value_absorbed_name
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getValueAbsorbedName()
    {
        return $this->value_absorbed_name;
    }

    /**
     * Set value_effect_name
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $valueEffectName
     */
    public function setValueEffectName(\LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping $valueEffectName)
    {
        $this->value_effect_name = $valueEffectName;
    }

    /**
     * Get value_effect_name
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
     */
    public function getValueEffectName()
    {
        return $this->value_effect_name;
    }

    /**
     * Set combatLog
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $combatLog
     */
    public function setCombatLog(\LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $combatLog)
    {
        $this->combatLog = $combatLog;
    }

    /**
     * Get combatLog
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog
     */
    public function getCombatLog()
    {
        return $this->combatLog;
    }
}