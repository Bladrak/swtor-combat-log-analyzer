<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Entity;

use LogAnalyzer\Bundle\StatsBundle\Entity\Stats;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;


/**
 * LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog
 *
 * @ORM\Table(indexes={@ORM\index(name="sort_importTime", columns={"cl_importTime"}),
 * 						@ORM\index(name="join_char", columns={"char_id"})})
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLogRepository")
 */
class CombatLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $cl_importTime
     *
     * @ORM\Column(name="cl_importTime", type="datetime")
     */
    private $cl_importTime;

    /**
	 * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter", inversedBy="logs")
	 * @ORM\JoinColumn(name="char_id", referencedColumnName="id", onDelete="CASCADE")
	 *
	 * @var LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $torCharacter
     */
    private $torCharacter;

    /**
     *
     * @var ArrayCollection
     */
    private $entries;

    /**
     * @ORM\OneToMany(targetEntity="Fight", mappedBy="combatLog")
     */
    private $fights;

    /**
     * @var file $imported_file
     *
     * @Assert\File(maxSize="8M", maxSizeMessage="The file is too large ({{ size }}). Maximum size is {{ limit }}", mimeTypes = { "text/plain", "text/x-c" }, mimeTypesMessage = "The file must be a txt file.")
     */
    private $imported_file;

    /* -----------------------------------------
     * 				 STATISTICS
     * -----------------------------------------
     */

    /**
     * @var ArrayCollection $logStats
     *
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\Stats", mappedBy="referencingCombatLog")
     */
    private $logStats;


    public function __construct()
    {
    	$this->entries = new ArrayCollection();
    	$this->cl_importTime = new \DateTime('now');
    	$this->logStats = new ArrayCollection();
    }
    
    public function getStatsFieldsToAggregate()
    {
    	$keysToAggregate = array("healCritRatioValue",
    			"dmgDoneCritRatioValue", "dmgTakenCritRatioValue",
    			"healCritRatioCapacity", "dmgDoneCritRatioCapacity",
    			"totalHealReceived", "totalHealDone", "totalDamageDone",
    			"totalThreat", "totalAbsorbedDamages", "totalInterrupts",
    			"totalDispells", "dmgTakenCritRatioCapacity", "beginTime",
    			"endTime");
		$fightKeys = array("maxHealDone", "maxHealReceived",
				"maxDamageDealt", "maxDamageTaken", "healPerCapacity",
				"healAvgPerCapacity", "healPerTarget", "healPerSource",
				"threatPerCapacity", "threatAvgPerCapacity",
				"damagePerCapacity", "damageAvgPerCapacity",
				"damageOverTime", "deaths", "deathsCount",
				"gainedXp");
    	$keysToAggregate = array_merge($keysToAggregate, $fightKeys);
    	return $keysToAggregate;
    }
    

    /**
     * Get the starting date of the combat log
     *
     * @return \DateTime
     */
/*    public function getStartingDate()
    {
    	$starting_date = new \DateTime("now");
    	foreach ($this->getLogStats()->toArray() as $entry)
    	{
    		if ($entry->getBeginTime() < $starting_date)
    			$starting_date = $entry->getBeginTime();
    	}
    	return $starting_date;
    }
*/
    /**
     * Get the ending date of the combat log
     *
     * @return \DateTime
     */
/*    public function getEndingDate()
    {
    	$ending_date = new \DateTime("01/01/1970 00:00:00");
    	foreach ($this->getLogStats()->toArray() as $entry)
    	{
    		if ($entry->getEndTime() > $ending_date)
    			$ending_date = $entry->getEndTime();
    	}
    	return $ending_date;
    }
*/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imported_file
     *
     * @param string $importedFile
     */
    public function setImportedFile($importedFile)
    {
        $this->imported_file = $importedFile;
    }

    /**
     * Get imported_file
     *
     * @return File
     */
    public function getImportedFile()
    {
        return $this->imported_file;
    }

    /**
     * Set cl_importTime
     *
     * @param datetime $clImportTime
     */
    public function setClImportTime($clImportTime)
    {
        $this->cl_importTime = $clImportTime;
    }

    /**
     * Get cl_importTime
     *
     * @return datetime
     */
    public function getClImportTime()
    {
        return $this->cl_importTime;
    }

    /**
     * Add entries
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry $entries
     */
    public function addLogEntry(\LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry $entries)
    {
        $this->entries[] = $entries;
    }

    /**
     * Get entries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Set torCharacter
     *
     * @param LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $torCharacter
     */
    public function setTorCharacter(\LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter $torCharacter)
    {
        $this->torCharacter = $torCharacter;
    }

    /**
     * Get torCharacter
     *
     * @return LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter
     */
    public function getTorCharacter()
    {
        return $this->torCharacter;
    }

    /**
     * Set logStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats
     */
    public function setLogStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats)
    {
        $this->logStats = $logStats;
    }

    /**
     * Get logStats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLogStats()
    {
        return $this->logStats;
    }

    /**
     * Add fights
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight $fights
     */
    public function addFight(\LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight $fights)
    {
        $this->fights[] = $fights;
    }

    /**
     * Get fights
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getFights()
    {
        return $this->fights;
    }

    /**
     * Add logStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats
     */
    public function addStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats)
    {
        $this->logStats[] = $logStats;
    }
}