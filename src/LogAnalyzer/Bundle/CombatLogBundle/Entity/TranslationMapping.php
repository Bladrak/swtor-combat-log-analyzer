<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping
 *
 * @ORM\Table(name="TranslationMapping")
 * @ORM\Entity
 */
class TranslationMapping
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $stringId
     *
     * @ORM\Column(name="stringId", type="string", length=255)
     */
    private $stringId;

    /**
     * @var string $stringValue
     *
     * @ORM\Column(name="stringValue", type="string", length=255)
     */
    private $stringValue;

	public function __construct()
	{
		$this->stringId = "";
		$this->stringValue = "";
	}


    public function __toString()
    {
    	return $this->stringValue;
    }


    public function setValueKey($value, $key = null)
    {
    	$this->stringValue = $value;
    	$this->stringId = $key;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stringId
     *
     * @param string $stringId
     */
    public function setStringId($stringId)
    {
        $this->stringId = $stringId;
    }

    /**
     * Get stringId
     *
     * @return string
     */
    public function getStringId()
    {
    	if ($this->stringId == null || $this->stringId == "") {
    		$this->stringId = "laGen_".microtime(true);
    	}
        return $this->stringId;
    }

    /**
     * Set stringValue
     *
     * @param string $stringValue
     */
    public function setStringValue($stringValue)
    {
        $this->stringValue = $stringValue;
    }

    /**
     * Get stringValue
     *
     * @return string
     */
    public function getStringValue()
    {
        return $this->stringValue;
    }
}