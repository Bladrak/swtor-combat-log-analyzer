<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Entity;

use LogAnalyzer\Bundle\CombatLogBundle\Service\TranslationMappingService;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight
 *
 * @ORM\Table(indexes={@ORM\index(name="sort_startTime", columns={"startTime"}),
 * 						@ORM\index(name="join_combatlog", columns={"cl_id"})})
 * @ORM\Entity(repositoryClass="LogAnalyzer\Bundle\CombatLogBundle\Entity\FightRepository")
 */
class Fight
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $startTime
     *
     * @ORM\Column(name="startTime", type="datetime")
     */
    private $startTime;

    /**
     * @var datetime $endTime
     *
     * @ORM\Column(name="endTime", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var array $versusTargets
     *
     * @ORM\Column(name="versusTargets", type="array", nullable=true)
     */
    private $versusTargets = array();

    /**
     * @ORM\ManyToOne(targetEntity="LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog", inversedBy="fights")
     * @ORM\JoinColumn(name="cl_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $combatLog
     */
    private $combatLog;

    /**
     * @var ArrayCollection $logStats
     *
     * @ORM\OneToMany(targetEntity="LogAnalyzer\Bundle\StatsBundle\Entity\Stats", mappedBy="referencingFight")
     */
    private $logStats;

    /**
     * @var ArrayCollection $entries
     */
    private $entries;



    public function __construct()
    {
    	$this->logStats = new ArrayCollection();
    	$this->entries = new ArrayCollection();
    }
    
    public function getStatsFieldsToAggregate()
    {
		$keysToAggregate = array("healCritRatioValue",
				"dmgDoneCritRatioValue", "dmgTakenCritRatioValue",
				"healCritRatioCapacity", "dmgDoneCritRatioCapacity",
				"totalHealReceived", "totalHealDone", "totalDamageDone",
				"totalThreat", "totalAbsorbedDamages", "totalInterrupts",
				"totalDispells", "dmgTakenCritRatioCapacity", "beginTime",
				"endTime");
    	$fightKeys = array("maxHealDone", "maxHealReceived",
    			"maxDamageDealt", "maxDamageTaken", "healPerCapacity",
    			"healAvgPerCapacity", "healPerTarget", "healPerSource",
    			"threatPerCapacity", "threatAvgPerCapacity",
    			"damagePerCapacity", "damageAvgPerCapacity",
    			"damageOverTime", "healOverTime", "dpsOverTime",
    			"hpsOverTime", "tpsOverTime", "deaths", "deathsCount",
    			"gainedXp");
    	$keysToAggregate = array_merge($keysToAggregate, $fightKeys);
    	return $keysToAggregate; 
    }

    public function cleanMemory()
    {
    	unset($this->entries);
    	$this->entries = new ArrayCollection();
    }

    public function getDuration()
    {
    	$diff = $this->getStartTime()->diff($this->getEndTime(), true);
    	return $diff->format('%Hh %Im %Ss');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param datetime $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * Get startTime
     *
     * @return datetime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param datetime $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * Get endTime
     *
     * @return datetime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    public function addEntry(LogEntry $entry, $username)
    {
    	if ($entry->getEffectName()->getStringId() == TranslationMappingService::$DAMAGE_STRING_ID)
    	{
	    	if ($entry->getTarget() != $username
	    			&& $entry->getTarget() != ""
	    			&& !in_array($entry->getTarget(), $this->getVersusTargets())) {
	    		$this->addVersusTarget($entry->getTarget());
	    	}
	    	if ($entry->getSource() != $username
	    			&& $entry->getSource() != ""
	    			&& !in_array($entry->getSource(), $this->getVersusTargets())) {
	    		$this->addVersusTarget($entry->getSource());
	    	}
    	}

    	if ($this->startTime == null)
    		$this->startTime = $entry->getLogTimestamp();
    	if ($this->endTime == null)
    		$this->endTime = $entry->getLogTimestamp();
    	if ($this->endTime < $entry->getLogTimestamp())
    		$this->endTime = $entry->getLogTimestamp();
    	if ($this->startTime > $entry->getLogTimestamp())
    		$this->startTime = $entry->getLogTimestamp();
    	$this->entries->add($entry);
    }

    /**
     * Get LogEntries related to this fight
     *
     * @return ArrayCollection
     */
    public function getEntries()
    {
    	if (count($this->entries) > 0)
    		return $this->entries;
    	$ret = array();
    	if ($this->getCombatLog() != null 
    			&& $this->getCombatLog()->getEntries() != null 
    			&& count($this->getCombatLog()->getEntries()) > 0)
    	{
	    	foreach ($this->getCombatLog()->getEntries()->toArray() as $logEntry)
	    	{
	    		if ($logEntry->getLogTimestamp() >= $this->getStartTime()
	    				&& $logEntry->getLogTimestamp() <= $this->getEndTime()) {
	    			if (is_array($logEntry)) {
	    				var_dump($logEntry);
	    			}
	    			$ret[] = $logEntry;
	    		}
	    	}
    	}
    	return $ret;
    }

    /**
     * Set combatLog
     *
     * @param LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $combatLog
     */
    public function setCombatLog(\LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog $combatLog)
    {
        $this->combatLog = $combatLog;
    }

    /**
     * Get combatLog
     *
     * @return LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog
     */
    public function getCombatLog()
    {
        return $this->combatLog;
    }

    /**
     * Add logStats
     *
     * @param LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats
     */
    public function addStats(\LogAnalyzer\Bundle\StatsBundle\Entity\Stats $logStats)
    {
        $this->logStats[] = $logStats;
    }

    /**
     * Get logStats
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLogStats()
    {
        return $this->logStats;
    }

    /**
     * Add opponent to array
     *
     * @param string $target
     */
    public function addVersusTarget($target)
    {
    	$this->versusTargets[] = $target;
    }

    /**
     * Set versusTargets
     *
     * @param array $versusTargets
     */
    public function setVersusTargets($versusTargets)
    {
        $this->versusTargets = $versusTargets;
    }

    /**
     * Get string representation of opponents
     *
     * @return string
     */
    public function getVersusTarget()
    {
    	$ret = "";
    	foreach ($this->versusTargets as $target)
    	{
    		$ret .= $target." ; ";
    	}
    	return strlen($ret) > 3 ? substr($ret, 0, -3) : "noone";
    }

    /**
     * Get versusTargets
     *
     * @return array
     */
    public function getVersusTargets()
    {
        return $this->versusTargets;
    }
}