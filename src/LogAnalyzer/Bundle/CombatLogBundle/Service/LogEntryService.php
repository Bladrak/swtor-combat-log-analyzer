<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Service;

use Doctrine\ORM\Query\ResultSetMapping;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Console\Output\OutputInterface;

use LogAnalyzer\Bundle\CombatLogBundle\Service\TranslationMappingService;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping;

class LogEntryService
{
	protected $em = null;
	/**
	 * @var TranslationMappingService $tmService
	 */
	protected $tmService = null;

	/**
	 * @return EntityManager
	 */
	public function getEntityManager() { return $this->em; }

	public function __construct($em, TranslationMappingService $tmService)
	{
		$this->em = $em;
		$this->tmService = $tmService;
	}

	/**
	 * Selects the log entries from combat logs imported at least one day ago
	 * and deletes them. Those entries are misimported logs.
	 *
	 * Outputs the results to $output
	 *
	 * @param OutputInterface $output
	 */
	public function cleanDb(OutputInterface $output)
	{
		$now = new \DateTime();
		$output->writeln("<info>".date("H:i:s", time())." : Cleaning the database, started on ".$now->format("Y-m-d H:i:s")."</info>");
		$limitDate = $now->sub(new \DateInterval("P1D"));
		$sql = "DELETE le FROM loganalyzer_LogEntry le
					INNER JOIN loganalyzer_CombatLog cl
					WHERE le.cl_id = cl.id
					AND cl.cl_importTime < '".$limitDate->format("Y-m-d H:i:s")."';";

		$sql .= "DELETE cl FROM loganalyzer_CombatLog cl
					INNER JOIN loganalyzer_Stats s
					WHERE s.cl_id = cl.id
					AND s.endTime IS NULL;";

		$sql .= "DELETE s FROM loganalyzer_Stats s
					WHERE (s.endTime IS NULL
					OR s.begin_time IS NULL)
					AND s.id NOT IN (SELECT aggregatedstats_id FROM loganalyzer_GroupStats);";

		$output->writeln("<info>".date("H:i:s", time())." : About to execute query : </info>");
		$output->writeln("<comment>".$sql."</comment>");

		//$numDeleted = $this->getEntityManager()->getConnection()->executeUpdate($sql);

		$output->writeln("<info>".date("H:i:s", time())." : Query executed, ".$numDeleted." entries deleted</info>");
	}

	/**
	 * Constructs a log entry
	 * @param string $str_entry
	 * @param DateTime $clDate
	 * @param DateTime $firstEntryTime
	 * @return LogEntry
	 * @throws \Exception
	 */
	public function newLogEntry($str_entry, \DateTime $clDate = null, \DateTime $firstEntryTime = null)
	{
		if ($str_entry=="") return;
		$encodingList = array("UTF-8", "ISO-8859-1");
		$enc = mb_detect_encoding($str_entry, "UTF-8", true);
		if ($enc == false) {
			$str_entry = mb_convert_encoding($str_entry, "UTF-8", mb_detect_encoding($str_entry, $encodingList));
		}
		$datePattern = "([0-9]{2}/[0-9]{2}/[0-9]{4} )?[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{3})?";

		$stringIdPattern = "[0-9]*";

		$valuePattern = "(?P<value>[0-9*]*)( -)?( (-)?(?P<value_type>[^{]*) \{(?P<value_type_id>"
		. $stringIdPattern
		. ")\})?( \((?P<value_absorbed>[0-9]*) (?P<value_absorbed_name>.*) \{(?P<value_absorbed_name_id>"
		. $stringIdPattern . ")\}\))?((?P<value_effect_name>.*) \{(?P<value_effect_name_id>".$stringIdPattern."\}))?";

		$pattern = "#\[(?P<date>" . $datePattern . ")\] "
		. "\[(?P<source>[^{]*)( \{(?P<source_id>".$stringIdPattern.")\})?(:(?P<source_uid>".$stringIdPattern."))?\] "
		. "\[(?P<target>[^{]*)( \{(?P<target_id>".$stringIdPattern.")\})?(:(?P<target_uid>".$stringIdPattern."))?\] "
		. "\[((?P<ability_name>.*) \{(?P<ability_name_id>". $stringIdPattern. ")\})?\] "
		. "\[(?P<resource_event>.*) \{(?P<resource_event_id>"
		. $stringIdPattern
		. ")\}: (?P<effect_name>.*) \{(?P<effect_name_id>"
		. $stringIdPattern . ")\}\] \(" . $valuePattern
		. "\)( <(?P<threat>[0-9]+)>)?#";
		preg_match_all($pattern, $str_entry, $matches);
		if (!isset($matches["date"][0])) {
			throw new \Exception("42: Unmatched format for log entry");
		}

		// We generate the unexisting translationmappings entities
		$this->tmService->fillWithArray($matches);

		if ($clDate != null) {
			$format = "Y/m/d H:i:s.u";
			$leDate = \DateTime::createFromFormat($format, $clDate->format("Y/m/d")." ".$matches["date"][0]);
			if ($firstEntryTime != null) {
				$firstHours = \DateTime::createFromFormat($format, $clDate->format("Y/m/d")." ".$firstEntryTime->format("H:i:s.u"));
				if ($firstHours->diff($leDate)->h > 10) {
					$leDate->add(new \DateInterval("P1D")); // We add one day as we past midnight
				}
			}
		} else {
			$leDate = new \DateTime($matches["date"][0]); // In this case there is no milliseconds in the timestamp
		}
		$logEntry = new LogEntry();
		$logEntry->setLogTimestamp($leDate);
		$logEntry->initWithEmptyTm($this->tmService->get("", ""));

		// Source / Target
		$logEntry->setSourceIsPlayer(strpos($matches["source"][0], "@") !== false);
		if ($logEntry->getSourceIsPlayer()) {
			$source = str_replace("@", "", $matches["source"][0]);
			if (strpos($source, ":") !== false) { // Companion management
				$srcArray = explode(":", $source);
				$source = $srcArray[0];
				$companion_name = $srcArray[1];
				$companion_id = $matches["source_id"][0];
				$logEntry->setSourceCompanion($this->tmService->get($companion_name, $companion_id));
			}
			$source = $this->tmService->get($source, null);
		} else {
			$sourceValue = $matches["source"][0];
			$sourceKey = $matches["source_id"][0];
			$source = $this->tmService->get($sourceValue, $sourceKey);
		}
		$logEntry->setSource($source);

		$logEntry->setTargetIsPlayer(strpos($matches["target"][0], "@") !== false);
		if ($logEntry->getTargetIsPlayer()) {
			$target = str_replace("@", "", $matches["target"][0]);
			if (strpos($target, ":") !== false) { // Companion management
				$tgtArray = explode(":", $target);
				$target = $tgtArray[0];
				$companion_name = $tgtArray[1];
				$companion_id = $matches["target_id"][0];
				$logEntry->setSourceCompanion($this->tmService->get($companion_name, $companion_id));
			}
			$target = $this->tmService->get($target, null);
		} else {
			$targetValue = $matches["target"][0];
			$targetKey = $matches["target_id"][0];
			$target = $this->tmService->get($targetValue, $targetKey);
		}
		$logEntry->setTarget($target);

		if (isset($matches["ability_name"][0]) && $matches["ability_name"][0] != "") {
			$logEntry->setAbilityName($this->tmService->get($matches["ability_name"][0], $matches["ability_name_id"][0]));
		}
		if (isset($matches["resource_event"][0]) && $matches["resource_event"][0] != "") {
			$logEntry->setResourceEvent($this->tmService->get($matches["resource_event"][0], $matches["resource_event_id"][0]));
		}
		if (isset($matches["effect_name"][0]) && $matches["effect_name"][0] != "") {
			$logEntry->setEffectName($this->tmService->get($matches["effect_name"][0], $matches["effect_name_id"][0]));
		}

		$logEntry->setValueCrit(strpos($matches["value"][0], "*") !== false);
		$logEntry->setValue(str_replace("*", "", $matches["value"][0]));
		if (isset($matches["value_type"][0])) {
			$logEntry->setValueType($this->tmService->get($matches["value_type"][0], $matches["value_type_id"][0]));
		}
		if (isset($matches["value_absorbed"][0])) {
			$logEntry->setValueAbsorbed($matches["value_absorbed"][0]);
			$logEntry->setValueAbsorbedName($this->tmService->get($matches["value_absorbed_name"][0], $matches["value_absorbed_name_id"][0]));
			$logEntry->setValueEffectName($this->tmService->get($matches["value_effect_name"][0], $matches["value_effect_name_id"][0]));
		}
		$logEntry->setThreat($matches["threat"][0]);

		$logEntry->setIsEffect($logEntry->getAbilityName() == null);

		if (strpos("}", $logEntry->getEffectName()->getStringId()) !== false) {
			var_dump($str_entry); exit;
		}

		return $logEntry;
	}
}