<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Service;
use LogAnalyzer\Bundle\StatsBundle\Entity\Stats;

use Monolog\Logger;

use Doctrine\ORM\ORMException;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\TranslationMapping;

/**
 * TranslationMappingService
 *
 */
use Doctrine\ORM\EntityManager;

class TranslationMappingService {
	private $translations = array();
	private $translationsKeys = array();

	public static $HEAL_STRING_ID;
	public static $DAMAGE_STRING_ID;
	public static $ENTER_COMBAT_STRING_ID;
	public static $EXIT_COMBAT_STRING_ID;
// 	public static $SECURE_CONNECT_STRING_ID;
// 	public static $RANIMATE_STRING_ID;
	public static $DEATH_STRING_ID;
	public static $GAIN_XP_STRING_ID;

	/**
	 *
	 * @var EntityManager $em
	 */
	private $em;

	/**
	 *
	 * @var Logger $logger
	 */
	private $logger;
	
	private $loaded = false;

	public function __construct(EntityManager $em, Logger $logger) {
		$this->em = $em;
		$this->logger = $logger;
		//$this->logger->info("Constructing TranslationMappingService");
		$tMaps = $this->em
				->createQuery("SELECT tm FROM LogAnalyzerCombatLogBundle:TranslationMapping tm
								WHERE tm.stringValue IN ('Heal', 'Damage', 'EnterCombat', 'ExitCombat', 'Death', 'GainXp')")
				->getResult()
		;
		foreach ($tMaps as $tMap) {
			$this->translations[$tMap->getStringId()][$tMap->getStringValue()] = $tMap;
			$this->translationsKeys[$tMap->getStringValue()] = $tMap->getStringId();
		}
		self::$HEAL_STRING_ID = $this->getKey("Heal");
		self::$DAMAGE_STRING_ID = $this->getKey("Damage");
		self::$ENTER_COMBAT_STRING_ID = $this->getKey("EnterCombat");
		self::$EXIT_COMBAT_STRING_ID = $this->getKey("ExitCombat");
// 		self::$SECURE_CONNECT_STRING_ID = $this->getKey("Safe Login");
// 		self::$RANIMATE_STRING_ID = $this->getKey("Revived");
		self::$DEATH_STRING_ID = $this->getKey("Death");
		self::$GAIN_XP_STRING_ID = $this->getKey("GainXp");
	}

	public function refresh()
	{
		//$this->logger->info("Refreshing TranslationMappingService");
		$tMaps = $this->em
		->getRepository('LogAnalyzerCombatLogBundle:TranslationMapping')
		->findAll();
		foreach ($tMaps as $tMap) {
			$this->translations[$tMap->getStringId()][$tMap->getStringValue()] = $tMap;
			$this->translationsKeys[$tMap->getStringValue()] = $tMap->getStringId();
		}
		$this->loaded = true;
	}

	/**
	 * Returns translation mapping object based on its value
	 * If not set, creates it
	 * @param string $value
	 * @param string $key
	 * @return TranslationMapping
	 */
	public function get($value, $key) {
		if (!$this->loaded)
			$this->refresh();
		if ($key == null) // Username
		{
			$key = isset($this->translationsKeys[$value]) ? $this->translationsKeys[$value] : null;
		}
		if (!isset($this->translations[$key][$value])) {
			$trans = new TranslationMapping();
			$this->logger->info("New translation mapping : ".$key." - ".$value);
			$trans->setStringValue($value);
			$trans->setStringId($key);
			$this->em->persist($trans);
			$key = $trans->getStringId();
			$this->translations[$trans->getStringId()][$trans->getStringValue()] = $trans;
			$this->translationsKeys[$trans->getStringValue()] = $trans->getStringId();
		}
		return $this->translations[$key][$value];
	}

	/**
	 * Returns the stringId associated to a stringValue
	 *
	 * @param string $value
	 * @return string
	 */
	public function getKey($value) {
		if (isset($this->translationsKeys[$value]))
			return $this->translationsKeys[$value];
		return null;
	}

	/**
	 * Generates unexising entities to avoid multiple connections to the database
	 *
	 * @param array $values
	 */
	public function fillWithArray($values)
	{
		if (!$this->loaded)
			$this->refresh();
		foreach ($values as $key => $value)
		{
			if ($key != null && $value[0] != "" && strpos($key, "_id") !== false)
			{
				if (isset($this->translations[$value[0]][$values[substr($key, 0, -3)][0]]))
					continue;
				$trans = new TranslationMapping();
				$trans->setStringId($value[0]);
				$trans->setStringValue($values[substr($key, 0, -3)][0]);
				$this->logger->info("New translation mapping : ".$trans->getStringId()." - ".$trans->getStringValue());
				$this->em->persist($trans);
				$this->translations[$trans->getStringId()][$trans->getStringValue()] = $trans;
				$this->translationsKeys[$trans->getStringValue()] = $trans->getStringId();
			}
		}
	}

	public function getValueForKey($key)
	{
		if (!$this->loaded)
			$this->refresh();
		$values = $this->translations[$key];
		if (count($values) > 0) {
			return current($values)->getStringValue();
		}
		return "";
	}

	public function isKey($key)
	{
		if (!$this->loaded)
			$this->refresh();
		return array_key_exists($key, $this->translations);
	}

	/**
	 *
	 * @return Stats
	 * @param Stats $stats
	 */
	public function cleanUpStats(Stats $stats)
	{
		if (!$this->loaded)
			$this->refresh();
		$hps = $stats->getHealPerSource();
		foreach ($hps as $key => $value)
		{
			if ($this->isKey($key))
				$hps[$this->getValueForKey($key)] = $value;
		}
		$stats->setHealPerSource($hps);
		$hps = $stats->getHealPerTarget();
		foreach ($hps as $key => $value)
		{
			if ($this->isKey($key))
				$hps[$this->getValueForKey($key)] = $value;
		}
		$stats->setHealPerTarget($hps);
		return $stats;
	}

}
