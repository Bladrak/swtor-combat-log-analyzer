<?php


namespace LogAnalyzer\Bundle\CombatLogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;





class CleanDbCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName("logentries:clean-db")
			->setDescription("Cleans the DB of ghost log entries")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->getContainer()->get('log_analyzer_combat_log.logentry_service')->cleanDb($output);
	}
}