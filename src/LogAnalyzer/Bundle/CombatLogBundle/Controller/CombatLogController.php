<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Controller;
use FOS\UserBundle\Model\UserInterface;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use LogAnalyzer\Bundle\StatsBundle\Service\StatsService;

use LogAnalyzer\Bundle\CombatLogBundle\Service\TranslationMappingService;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight;

use LogAnalyzer\Bundle\StatsBundle\Entity\Stats;

use Symfony\Component\Form\FormError;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\CombatLog;
use LogAnalyzer\Bundle\CombatLogBundle\Form\CombatLogType;
use LogAnalyzer\Bundle\UserBundle\Entity\TorCharacter;

/**
 * CombatLog controller.
 *
 * @Route("/combatlog")
 */
class CombatLogController extends Controller {
	/**
	 * Lists all CombatLog entities.
	 *
	 * @Route("/", name="combatlog")
	 * @Template()
	 */
	public function indexAction() {
		$em = $this->getDoctrine()->getEntityManager();

		$entities = $em->getRepository('LogAnalyzerCombatLogBundle:CombatLog')
				->findAll();

		return array('entities' => $entities);
	}

	public function listLogsAction($character_id) {
		$logs = $this->getDoctrine()->getEntityManager()
				->getRepository('LogAnalyzerCombatLogBundle:CombatLog')
				->listByTorCharacter($character_id);

		$entity = $this->getDoctrine()->getEntityManager()
				->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($character_id);

		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			$access = $user->hasCharacter($entity);
		}

		return $this
				->render(
						'LogAnalyzerCombatLogBundle:CombatLog:listLogs.html.twig',
						array('entities' => $logs,
								'character_id' => $character_id,
								'access' => $access));
	}

// 	/**
// 	 * Finds and displays a CombatLog entity.
// 	 *
// 	 * @Route("/{id}/show", name="combatlog_show")
// 	 * @Template()
// 	 */
// 	public function showAction($id) {
// 		$em = $this->getDoctrine()->getEntityManager();

// 		$entity = $em->getRepository('LogAnalyzerCombatLogBundle:CombatLog')
// 				->find($id);

// 		if (!$entity) {
// 			throw $this
// 					->createNotFoundException(
// 							'Unable to find CombatLog entity.');
// 		}

// 		$deleteForm = $this->createDeleteForm($id);

// 		$breadcrumbs = $this->get("white_october_breadcrumbs");
// 		$breadcrumbs
// 				->addItem("Home", $this->get("router")->generate("_welcome"));
// 		$breadcrumbs
// 				->addItem($entity->getTorCharacter()->getName(),
// 						$this->get("router")
// 								->generate("character_show",
// 										array(
// 												"id" => $entity
// 														->getTorCharacter()
// 														->getId())));
// 		$breadcrumbs
// 				->addItem(
// 						$entity->getClImportTime()
// 								->format("F jS, Y \\a\\t H:ia"),
// 						$this->get("router")
// 								->generate("combatlog_show",
// 										array("id" => $entity->getId())));

// 		return array('entity' => $entity,
// 				'delete_form' => $deleteForm->createView(),);
// 	}

	/**
	 * Displays a form to create a new CombatLog entity.
	 *
	 * @Route("/new", name="combatlog_new")
	 * @Template()
	 */
	public function newAction($character_id) {
		$t_char = $this->getDoctrine()->getEntityManager()
				->getRepository('LogAnalyzerUserBundle:TorCharacter')
				->find($character_id);
		$entity = new CombatLog();
		$entity->setTorCharacter($t_char);
		$form = $this->createForm(new CombatLogType(), $entity);

		return array('entity' => $entity, 'form' => $form->createView());
	}

	/**
	 * Creates a new CombatLog entity.
	 *
	 * @Route("/create", name="combatlog_create")
	 * @Method("post")
	 * @Template("LogAnalyzerCombatLogBundle:CombatLog:new.html.twig")
	 */
	public function createAction() {

		$entity = new CombatLog();
		$request = $this->getRequest();
		$form = $this->createForm(new CombatLogType(), $entity);
		$form->bindRequest($request);

		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			if ($entity->getTorCharacter() == null)
			{
				$access = false;
			} else {
				$access = $user->hasCharacter($entity->getTorCharacter());
			}
		}
		if ($access === false)
			throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


		$username = $entity->getTorCharacter()->getName();
		$is_valid = false;

		$logger = $this->get('logger');

		if ($form->isValid() && $form["imported_file"]->getData() != null) {

			$em = $this->getDoctrine()->getEntityManager();
			$fileName = $form["imported_file"]->getData()
					->getClientOriginalName();
			$fileName = explode("_", $fileName);
			$fileDate = new \DateTime($fileName[1]);
			$firstEntryDate = null;
			$currentFight = null;
			$logEntry = null;
			$file = $form["imported_file"]->getData()->openFile();
			$this->get('log_analyzer_combat_log.translationmapping_service');


			while (!$file->eof()) {
				$line = $file->fgets();
				if ($line != "") {
					try {
						$logEntry = $this
								->get('log_analyzer_combat_log.logentry_service')
								->newLogEntry($line, $fileDate, $firstEntryDate);
					} catch (\Exception $ex) {
						try {
							$logEntry = $this
									->get('log_analyzer_combat_log.logentry_service')
									->newLogEntry($line);
						} catch (\Exception $ex2) {
							if (strpos($ex2->getMessage(), "42:") !== false)
								continue;
							throw $ex2;
						}
					}
					if ($firstEntryDate == null)
						$firstEntryDate = $logEntry->getLogTimestamp();
					if ($logEntry->getEffectName()->getStringId()
									== TranslationMappingService::$ENTER_COMBAT_STRING_ID) {
// 						$logger->info('New fight found !, insertObjectsCount : '.$insertObjectsCount);
						if ($currentFight != null) {
							$currentFight->addEntry($logEntry, $username);
							if (count($currentFight->getVersusTargets()) > 0)
							{
								$currentFight->setCombatLog($entity);
								$entity->addFight($currentFight);
								$em->persist($currentFight);
							} else {
								$currentFight->cleanMemory();
								unset($currentFight);
							}
							$currentFight = null;
						}
						$currentFight = new Fight();
					}
					if ($currentFight != null
							&& ($logEntry->getTarget() != ""
									|| $logEntry->getSource() != "")
							&& $logEntry->getEffectName()->getStringId() == TranslationMappingService::$DAMAGE_STRING_ID) {

					}
					if ($logEntry->isEndOfFight($username)
							&& $currentFight != null) {
// 						$logger->info('End of fight, cause: '.$logEntry->getEffectName()->getStringValue()."  ".$logEntry->getAbilityName()->getStringValue()." time : ".$logEntry->getLogTimestamp()->format('Y-m-d H:i:s'));
						if (count($currentFight->getVersusTargets()) > 0)
						{
							$currentFight->addEntry($logEntry, $username);
							$currentFight->setCombatLog($entity);
							$entity->addFight($currentFight);
							$em->persist($currentFight);
						} else {
							$currentFight->cleanMemory();
							unset($currentFight);
						}
						$currentFight = null;
					}
					$is_valid = $is_valid
							|| (strcasecmp($logEntry->getSource()->getStringValue(),
									$username) === 0)
							|| (strcasecmp($logEntry->getTarget()->getStringValue(),
									$username) === 0);
					if ($currentFight != null)
						$currentFight->addEntry($logEntry, $username);
					else unset($logEntry);
				}
			}
			$logger->info('Ended parsing file');
			if ($currentFight != null && $logEntry != null
					&& $logEntry->getLogTimestamp() != null) {
				if (count($currentFight->getVersusTargets()) > 0)
				{
					$currentFight->setCombatLog($entity);
					$entity->addFight($currentFight);
					$em->persist($currentFight);
				} else {
					$currentFight->cleanMemory();
					unset($currentFight);
				}
				$currentFight = null;
			}
			$logger->info('Checking validity');
			$repIsValid = $this->getDoctrine()->getRepository('LogAnalyzerCombatLogBundle:CombatLog')->checkIsValid($entity);
			$logger->info('isValid : '.$is_valid." repIsValid : ".$repIsValid);
			$is_valid = $is_valid && $repIsValid;
			if (!$is_valid) {
				$em->remove($entity);
				$em->flush();
				$this->get('session')
						->setFlash('error',
								$this->get('translator')->trans("Your character name doesn't match the combat log, 
																	you don't have any fight in your combat log, 
																	or the combat log format is invalid."));
				return $this
						->redirect(
								$this
										->generateUrl('character_show',
												array(
														'id' => $entity
																->getTorCharacter()
																->getId())));
			}

			foreach ($entity->getFights() as $fight)
			{
				$this->get('log_analyzer_stats.stats_service')->statsForFight($fight);
			}

			$em->persist($entity);
			$em->flush();
			$clid = $entity->getid();
			$em->clear();
			unset($entity);



			return $this->redirect($this->generateUrl("stats_show_combatlog", array("id" => $clid)));
		}

		foreach($form->getErrors() as $error)
		{
			$this->get('session')
			->setFlash('error', $error->getMessageTemplate(), $error->getMessageParameters());
		}
		foreach($form->getChildren() as $child)
		{
			if (!$child->isValid()) {
				foreach ($child->getErrors() as $error)
				{
					$this->get('session')
					->setFlash('error', $error->getMessageTemplate(), $error->getMessageParameters());
				}
			}
		}
		if ($form["imported_file"]->getData() == null)
		{
			$this->get('session')
			->setFlash('error', $this->get('translator')->trans("File is empty"));
		}

		return $this
				->redirect(
						$this
								->generateUrl('character_show',
										array(
												'id' => $entity
														->getTorCharacter()
														->getId())));
	}

	/**
	 * Deletes a CombatLog entity.
	 *
	 * @Route("/delete/{id}", name="combatlog_delete_noform")
	 */
	public function deleteNoFormAction($id) {
		$em = $this->getDoctrine()->getEntityManager();
		$entity = $em
					->getRepository('LogAnalyzerCombatLogBundle:CombatLog')
					->find($id);

		if (!$entity) {
			throw $this
					->createNotFoundException(
							$this->get('translator')->trans('Unable to find CombatLog entity.'));
		}

		$user = $this->container->get('security.context')->getToken()->getUser();
		$access = false;
		if (is_object($user) && $user instanceof UserInterface) {
			$access = $user->hasCharacter($entity->getTorCharacter());
		}
		if ($access === false)
			throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


		$char_id = $entity->getTorCharacter()->getId();
		$em->remove($entity);
		$em->flush();

		$this->get('session')->setFlash('success', $this->get('translator')->trans("The combat log has been deleted."));

		return $this
					->redirect(
							$this
							->generateUrl('character_show',
									array('id' => $char_id)));
					;
	}

	private function createDeleteForm($id) {
		return $this->createFormBuilder(array('id' => $id))
				->add('id', 'hidden')->getForm();
	}
}
