<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry;
use LogAnalyzer\Bundle\CombatLogBundle\Form\LogEntryType;

/**
 * LogEntry controller.
 *
 * @Route("/logentry")
 */
class LogEntryController extends Controller
{
    /**
     * Lists all LogEntry entities.
     *
     * @Route("/", name="logentry")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Returns the stats for the logs of one combat log
     *
     * @param integer $cl_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listLogsAction($cl_id)
    {
    	$logs = $this->getDoctrine()->getEntityManager()->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->findByCombatLog($cl_id);
    	$stats = $this->processLogs($cl_id);
    	var_dump($stats); 
    	return $this->render('LogAnalyzerCombatLogBundle:LogEntry:listLogs.html.twig', array('stats' => $stats, 'cl_id' => $cl_id));
    }


    /**
     * Finds and displays a LogEntry entity.
     *
     * @Route("/{id}/show", name="logentry_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new LogEntry entity.
     *
     * @Route("/new", name="logentry_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LogEntry();
        $form   = $this->createForm(new LogEntryType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new LogEntry entity.
     *
     * @Route("/create", name="logentry_create")
     * @Method("post")
     * @Template("LogAnalyzerCombatLogBundle:LogEntry:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new LogEntry();
        $request = $this->getRequest();
        $form    = $this->createForm(new LogEntryType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('logentry_show', array('id' => $entity->getId())));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing LogEntry entity.
     *
     * @Route("/{id}/edit", name="logentry_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        $editForm = $this->createForm(new LogEntryType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing LogEntry entity.
     *
     * @Route("/{id}/update", name="logentry_update")
     * @Method("post")
     * @Template("LogAnalyzerCombatLogBundle:LogEntry:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        $editForm   = $this->createForm(new LogEntryType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('logentry_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a LogEntry entity.
     *
     * @Route("/{id}/delete", name="logentry_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LogEntry entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('logentry'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
