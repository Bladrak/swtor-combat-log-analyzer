<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Controller;

use FOS\UserBundle\Model\UserInterface;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LogAnalyzer\Bundle\CombatLogBundle\Entity\Fight;

/**
 * Fight controller.
 *
 * @Route("/fight")
 */
class FightController extends Controller
{
    /**
     * Lists all Fight entities.
     *
     * @Route("/{char_id}", name="fight")
     * @Template()
     */
    public function indexAction($char_id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $character = $em->getRepository('LogAnalyzerUserBundle:TorCharacter')->find($char_id);

        if (!$character)
        	throw $this->createNotFoundException($this->get('translator')->trans("Unable to find Character."));

    	$user = $this->container->get('security.context')->getToken()->getUser();
    	$access = false;
    	if (is_object($user) && $user instanceof UserInterface) {
    		$access = $user->hasCharacterId($char_id);
    	}
    	if ($access === false)
    		throw new AccessDeniedException($this->get('translator')->trans("You do not have the privileges to do that."));


//     	$breadcrumbs = $this->get("white_october_breadcrumbs");
//     	$breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("_welcome"));
//     	$breadcrumbs->addItem($character->getName(), $this->get("router")->generate("character_show", array("id" => $char_id)));
//     	$breadcrumbs->addItem($this->get('translator')->trans("Fights list"), $this->get("router")->generate("fight", array("char_id" => $char_id)));


        $entities = $em->getRepository('LogAnalyzerCombatLogBundle:Fight')->forCharacterId($char_id);

        return array(
        		'entities' => $entities,
        		'character' => $character,
        		);
    }

}
