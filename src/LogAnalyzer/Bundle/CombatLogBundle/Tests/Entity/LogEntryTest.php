<?php

namespace LogAnalyzer\Bundle\CombatLogBundle\Tests\Entity;

use LogAnalyzer\Bundle\CombatLogBundle\Entity\LogEntry;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class LogEntryTest extends WebTestCase
{

	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $_em;

	private $conn;

	protected function setUp() {
		$kernel = static::createKernel();
		$kernel->boot();
		$this->_em = $kernel->getContainer()
		->get('doctrine.orm.entity_manager');
		$this->conn = $kernel->getContainer()->get('database_connection');
	}

	public function testCreateEntry()
	{
		$str_entry = "[03/25/2012 18:50:45] [@Carboncopy:Aric Jorgan {493337123487744}] [@Carboncopy:Aric Jorgan {493337123487744}] [Burst {2163125918892032}] [Event {836045448945472}: AbilityActivate {836045448945479}] ()";

		$entry = $this->_em->getRepository('LogAnalyzerCombatLogBundle:LogEntry')->logEntryFromString($str_entry);
		$this->_em->persist($entry);
		try {
			$this->_em->flush();
		} catch (\PDOException $ex) {
			if ($ex->getCode() == '23000' && strpos($ex->getMessage(), "idValue")) {
				echo "Intercepted ex, but it's ok\n".$ex->getMessage();
			} else {
				throw $ex;
			}

		}
		//var_dump($entry);
		var_dump($entry->readable());
	}
}